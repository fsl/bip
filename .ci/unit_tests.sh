#!/usr/bin/env bash

thisdir=$(cd $(dirname $0) && pwd)

source activate /dev.env
export FSLDIR=$CONDA_PREFIX
source $FSLDIR/etc/fslconf/fsl.sh

pip install --no-deps -e .

pytest
