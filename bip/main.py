#!/usr/bin/env python
#
# main.py - Main BIP entry.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W1201,R0912
#

import os
import os.path as op
import sys
import logging
import argparse
import json
from file_tree import FileTree
from fsl_pipe import Pipeline, Ref
import bip
from bip.utils.config import Config
from bip.utils.log_utils import setup_logging
from bip.pipelines import struct_T1, struct_T2_FLAIR, struct_FS
from bip.pipelines import struct_swMRI, struct_asl, IDPs_gen
from bip.pipelines import dMRI_fieldmap, dMRI_diff
from bip.pipelines import fMRI_task, fMRI_rest#, fMRI_surf

log = logging.getLogger('bip.main')

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write(f'error: {message}\n')
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

class Context(Config):
    def __init__(self, subject, cfgdir=None):

        # Paths which may be referenced
        # in configuration files.
        env = {'BIPDIR' : bip.BIPDIR,
               'FSLDIR' : os.environ['FSLDIR']}

        self.subject = subject
        self.BIPDIR  = env['BIPDIR']
        self.FSLDIR  = env['FSLDIR']

        if not cfgdir:
            cfgdir = self.get_data('config/config.toml')

        Config.__init__(self, cfgdir=cfgdir, env=env)

        with open(self.get_data('dMRI/autoptx/struct_list.json'), 'r',
                  encoding="utf-8") as f:
            self.tract_struct = json.load(f)

    @property
    def MNI(self):
        return self.get_standard('MNI152_T1_1mm')

    @property
    def MNI_brain_mask(self):
        return self.get_standard('MNI152_T1_1mm_brain_mask')

    def get_standard(self, file_name):
        file_name = file_name.replace('/', os.sep)
        basedir = self.FSLDIR + os.sep + op.join('data', 'standard') + os.sep
        return basedir + file_name

    def get_atlas(self, file_name):
        file_name = file_name.replace('/', os.sep)
        basedir = self.FSLDIR + os.sep + op.join('data', 'atlases') + os.sep
        return basedir + file_name

    @staticmethod
    def get_data(file_name):
        file_name = file_name.replace('/', os.sep)
        return bip.utils.get_data(file_name)


def parseArguments():

    #########################
    # ARGUMENT PARSING CODE #
    #########################
    parser = MyParser(description='BioBank Pipeline Manager V. 2.0')
    parser.add_argument("subject", help='Subject. This can be either a ' +
                                        'folder with the subject data or a ' +
                                        'txt file with the list of folders ' +
                                        'of the subjects to proces',
                        action="store")
    parser.add_argument("-c", "--configDir",
                        help='Configuration folder: ' +
                             'A config.toml file must exist.',
                        action="store", default=bip.utils.get_data('config'))

    return parser.parse_args()

def main():

    args = parseArguments()

    # Parse subject argument
    subj = args.subject
    subj = subj.strip()
    if subj.endswith('/'):
        subj = subj[0:len(subj)-1]

    if not op.isdir(subj):
        if op.isfile(subj):
            with open(subj, 'r', encoding="utf-8") as f:
                subjects = [x.strip() for x in f.readlines()]
        else:
            sys.exit("\nError: The folder for the subject does not exist.\n")

    else:
        subjects = [subj]

    configDir = args.configDir

    for subject in subjects:
        ctx = Context(subject, cfgdir=configDir)

        setup_logging(op.join(ctx.subject, 'logs', 'main.log'))

        tree = FileTree.read(ctx.get_data('FileTree'),
                             subject=ctx.subject,
                             autoptx_tract=list(ctx.tract_struct.keys()),
                             netmats_dim=["25", "100"])

        # add submit parameters here that should
        # be applied to all jobs in the pipeline
        pipe = Pipeline(default_submit={"logdir": Ref('logs_dir'),
                                        "export_vars": list(os.environ.keys())})

        # pipe = Pipeline(default_submit=dict(logdir=Ref("logs_dir")))
        # ctx.save_context(tree.get('config_file'))

        # This list will be filled with all desired outputs ... Later
        # targets = []

        struct_T1.add_to_pipeline      (ctx, pipe, tree)
        struct_T2_FLAIR.add_to_pipeline(ctx, pipe, tree)
        struct_swMRI.add_to_pipeline   (ctx, pipe, tree)
        struct_asl.add_to_pipeline     (ctx, pipe, tree)
        dMRI_fieldmap.add_to_pipeline  (ctx, pipe, tree)
        fMRI_task.add_to_pipeline      (ctx, pipe, tree)
        fMRI_rest.add_to_pipeline      (ctx, pipe, tree)
        dMRI_diff.add_to_pipeline      (ctx, pipe, tree)
        IDPs_gen.add_to_pipeline       (ctx, pipe, tree)
        struct_FS.add_to_pipeline      (ctx, pipe, tree)
        #fMRI_surf.add_to_pipeline      (ctx, pipe, tree)


        # The skip-missing flag deals with cases where the subject is missing
        # the data of some modalities. For more details, check:
        # https://open.win.ox.ac.uk/pages/ndcn0236/pipe-tree/tutorial.html#dealing-with-missing-data
        # jobs = pipe.generate_jobs(tree).filter(None, skip_missing=True)

        jobs = pipe.generate_jobs(tree).split_pipeline()[args.stage][2].filter(None, skip_missing=True)

        jobs.report()

        jobs.run("submit")
        # jobs.run("local")

if __name__ == "__main__":
    main()
