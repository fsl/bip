#!/usr/bin/env python

from unittest import mock

import bip.main as main


def test_Context():
    with mock.patch.dict('os.environ', FSLDIR='.'):
        ctx = main.Context('subjectId')
