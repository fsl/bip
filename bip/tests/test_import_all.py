#!/usr/bin/env python



import pkgutil
import importlib
import bip


def test_importall():

    def recurse(module):

        path    = module.__path__
        name    = module.__name__
        submods = list(pkgutil.iter_modules(path, f'{name}.'))

        for i, (spath, smodname, ispkg) in enumerate(submods):

            submod = importlib.import_module(smodname)

            if ispkg:
                recurse(submod)

    recurse(bip)
