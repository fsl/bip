#!/usr/bin/env python
#
# Utility functions available for use by tests.
#


import            contextlib
import            os
import os.path as op
import            tempfile


def touch(fpath):
    """Create a dummy file at fpath."""
    with open(fpath, 'wt') as f:
        f.write(fpath)


@contextlib.contextmanager
def tempdir():
    """Create and change into a temp directory, deleting it afterwards."""
    prevdir = os.getcwd()
    with tempfile.TemporaryDirectory() as td:
        os.chdir(td)
        try:
            yield td
        finally:
            os.chdir(prevdir)


@contextlib.contextmanager
def mock_directory(contents):
    """Create a temp directory with dummy contents."""
    with tempdir() as td:
        for c in contents:
            touch(c)
        yield td


def dicts_equal(da, db):
    """Compare two dicts, ignoring order."""
    da = {k : da[k] for k in sorted(da.keys())}
    db = {k : db[k] for k in sorted(db.keys())}
    return da == db
