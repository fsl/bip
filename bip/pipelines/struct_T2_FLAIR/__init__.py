#!/usr/bin/env python
#
# struct_T2_FLAIR.py - Pipeline with the T2 FLAIR processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1735
#

import logging
from bip.utils.log_utils           import job_name
from bip.pipelines.struct_T2_FLAIR import T2_FLAIR_gdc, T2_FLAIR_brain_extract
from bip.pipelines.struct_T2_FLAIR import T2_FLAIR_defacing, T2_FLAIR_apply_bfc
from bip.pipelines.struct_T2_FLAIR import T2_FLAIR_bianca

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree):

    subj = ctx.subject

    pipe(T2_FLAIR_gdc.run,
         submit=dict(jobtime=200, name=job_name(T2_FLAIR_gdc.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T2_FLAIR_brain_extract.run,
         submit=dict(jobtime=200, name=job_name(T2_FLAIR_brain_extract.run,
                                                subj)),
         kwargs={'ctx' : ctx})
    pipe(T2_FLAIR_defacing.run,
         submit=dict(jobtime=200, name=job_name(T2_FLAIR_defacing.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T2_FLAIR_apply_bfc.run,
         submit=dict(jobtime=200, name=job_name(T2_FLAIR_apply_bfc.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T2_FLAIR_bianca.run,
         submit=dict(jobtime=200, name=job_name(T2_FLAIR_bianca.run, subj)),
         kwargs={'ctx' : ctx})
