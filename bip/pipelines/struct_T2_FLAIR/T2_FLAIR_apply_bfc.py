#!/usr/bin/env python
#
# T2_FLAIR_apply_bfc.py - Sub-pipeline applying the Bias Field Correction
#                         to T2 FLAIR
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T2_FLAIR:                In,
        T2_FLAIR_brain:          In,
        T1_fast_brain_bias:      In,
        logs_dir:                Ref,
        T2_FLAIR_unbiased:       Out,
        T2_FLAIR_unbiased_brain: Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        # Apply bias field correction to T2_FLAIR warped
        if op.isfile(T1_fast_brain_bias):
            wrappers.fslmaths(T2_FLAIR).div(T1_fast_brain_bias).\
                run(T2_FLAIR_unbiased)
            wrappers.fslmaths(T2_FLAIR_brain).div(T1_fast_brain_bias).\
                run(T2_FLAIR_unbiased_brain)
        else:
            print("WARNING: There was no bias field estimation. " +
                  "Bias field correction cannot be applied to T2.")
