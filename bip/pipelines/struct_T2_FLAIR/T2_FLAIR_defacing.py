#!/usr/bin/env python
#
# T2_FLAIR_defacing.py -  Sub-pipeline with defacing of the of T2 FLAIR.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1704
#

import os.path as op
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T2_FLAIR_orig:                    In,
        T2_FLAIR_brain_mask:              In,
        T2_FLAIR_to_MNI_linear_mat:       In,
        T2_FLAIR_orig_ud_to_T2_FLAIR_mat: In,
        T2_FLAIR_orig_defaced:            Out,
        T2_FLAIR_defacing_mask:           Out,
        T2_FLAIR_orig_defacing_mask:      Out,
        T2_FLAIR:                         Out,
        logs_dir:                         Ref,
        tmp_dir:                          Ref):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        T2_FLAIR_tmp_1     = op.join(tmp_dir, 'T2_FLAIR_tmp_1nii.gz')
        T2_FLAIR_tmp_3_mat = op.join(tmp_dir, 'T2_FLAIR_tmp_3.mat')

        BigFoV_mask_mat = 'MNI/MNI_to_MNI_BigFoV_facemask.mat'
        BigFoV_mask     = 'MNI/MNI152_T1_1mm_BigFoV_facemask'

        # TODO: Replace this part with proper call to fsl_deface
        # Defacing T2_FLAIR_orig
        wrappers.concatxfm(atob=T2_FLAIR_orig_ud_to_T2_FLAIR_mat,
                           btoc=T2_FLAIR_to_MNI_linear_mat,
                           atoc=T2_FLAIR_tmp_3_mat)
        wrappers.concatxfm(atob=T2_FLAIR_tmp_3_mat,
                           btoc=ctx.get_data(BigFoV_mask_mat),
                           atoc=T2_FLAIR_tmp_3_mat)
        wrappers.invxfm(inmat=T2_FLAIR_tmp_3_mat, omat=T2_FLAIR_tmp_3_mat)
        wrappers.applyxfm(src=ctx.get_data(BigFoV_mask),
                          ref=T2_FLAIR_orig, mat=T2_FLAIR_tmp_3_mat,
                          out=T2_FLAIR_orig_defacing_mask,
                          interp="trilinear")
        wrappers.fslmaths(T2_FLAIR_orig_defacing_mask).binv().\
            mul(T2_FLAIR_orig).run(T2_FLAIR_orig_defaced)

        # Defacing T2_FLAIR
        copyfile(src=T2_FLAIR, dst=T2_FLAIR_tmp_1)
        wrappers.concatxfm(atob=T2_FLAIR_to_MNI_linear_mat,
                           btoc=ctx.get_data(BigFoV_mask_mat),
                           atoc=T2_FLAIR_tmp_3_mat)
        wrappers.invxfm(inmat=T2_FLAIR_tmp_3_mat, omat=T2_FLAIR_tmp_3_mat)
        wrappers.applyxfm(src=ctx.get_data(BigFoV_mask),
                          ref=T2_FLAIR, mat=T2_FLAIR_tmp_3_mat,
                          out=T2_FLAIR_defacing_mask, interp="trilinear")
        wrappers.fslmaths(T2_FLAIR_defacing_mask).binv().mul(T2_FLAIR).\
            run(T2_FLAIR)
