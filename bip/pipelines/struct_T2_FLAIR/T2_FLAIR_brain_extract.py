#!/usr/bin/env python
#
# T2_FLAIR_brain_extract.py - Sub-pipeline with the brain extraction of T2 FLAIR
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=R1704
#

import os.path as op
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_brain:                           In,
        T1_brain_mask:                      In,
        T1_orig_ud:                         In,
        T1_orig_ud_to_T1_mat:               In,
        T1_to_MNI_linear_mat:               In,
        T1_to_MNI_warp:                     In,
        T2_FLAIR_orig:                      In,
        T2_FLAIR_orig_ud:                   In,
        T2_FLAIR_orig_ud_warp:              In,
        T2_FLAIR_brain:                     Out,
        T2_FLAIR_brain_mask:                Out,
        T2_FLAIR_brain_to_MNI:              Out,
        T2_FLAIR_to_MNI_linear_mat:         Out,
        T2_FLAIR_orig_ud_to_T2_FLAIR_mat:   Out,
        T2_FLAIR_orig_ud_to_MNI_linear_mat: Out,
        T2_FLAIR_orig_to_MNI_warp:          Out,
        T2_FLAIR:                           Ref,
        logs_dir:                           Ref,
        tmp_dir:                            Ref):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        T2_FLAIR_tmp_1_mat = op.join(tmp_dir, 'T2_FLAIR_tmp_1.mat')
        T2_FLAIR_tmp_2_mat = op.join(tmp_dir, 'T2_FLAIR_tmp_2.mat')

        # Take T2 to T1 and also the brain mask
        wrappers.flirt(src=T2_FLAIR_orig_ud, ref=T1_orig_ud,
                       omat=T2_FLAIR_tmp_1_mat, dof=6)
        wrappers.concatxfm(atob=T2_FLAIR_tmp_1_mat, btoc=T1_orig_ud_to_T1_mat,
                           atoc=T2_FLAIR_tmp_2_mat)

        # The T2_FLAIR.nii.gz in the output of this
        # flirt will be overwritten in case of GDC
        wrappers.flirt(src=T2_FLAIR_orig_ud, ref=T1_brain,
                       refweight=T1_brain_mask, nosearch=True,
                       init=T2_FLAIR_tmp_2_mat, out=T2_FLAIR,
                       omat=T2_FLAIR_orig_ud_to_T2_FLAIR_mat, dof=6)

        if ctx.gdc != '' :
            wrappers.applywarp(src=T2_FLAIR_orig, ref=T1_brain, out=T2_FLAIR,
                               w=T2_FLAIR_orig_ud_warp,
                               postmat=T2_FLAIR_orig_ud_to_T2_FLAIR_mat,
                               rel=True, interp='spline')

        copyfile(src=T1_brain_mask, dst=T2_FLAIR_brain_mask)
        wrappers.fslmaths(T2_FLAIR).mul(T2_FLAIR_brain_mask).run(T2_FLAIR_brain)

        # enerate the linear matrix from T2 to MNI (Needed for defacing)
        wrappers.concatxfm(atob=T2_FLAIR_orig_ud_to_T2_FLAIR_mat,
                           btoc=T2_FLAIR_tmp_1_mat,
                           atoc=T2_FLAIR_orig_ud_to_MNI_linear_mat)
        copyfile(src=T1_to_MNI_linear_mat, dst=T2_FLAIR_to_MNI_linear_mat)

        # Generate the non-linearly warped T2 in MNI
        # (Needed for post-freesurfer processing)
        if ctx.gdc != '' :
            wrappers.convertwarp(ref=ctx.MNI, warp1=T2_FLAIR_orig_ud_warp,
                                 midmat=T2_FLAIR_orig_ud_to_T2_FLAIR_mat,
                                 warp2=T1_to_MNI_warp,
                                 out=T2_FLAIR_orig_to_MNI_warp)
        else:
            wrappers.convertwarp(ref=ctx.MNI,
                                 premat=T2_FLAIR_orig_ud_to_T2_FLAIR_mat,
                                 warp1=T1_to_MNI_warp,
                                 out=T2_FLAIR_orig_to_MNI_warp)
        wrappers.applywarp(src=T2_FLAIR_orig, ref=ctx.MNI,
                           out=T2_FLAIR_brain_to_MNI,
                           w=T2_FLAIR_orig_to_MNI_warp,
                           rel=True, interp='spline')
        wrappers.fslmaths(T2_FLAIR_brain_to_MNI).mul(ctx.MNI_brain_mask).\
            run(T2_FLAIR_brain_to_MNI)
