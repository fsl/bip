#!/usr/bin/env python
#
# T2_FLAIR_bianca.py -  Sub-pipeline with FSL's BIANCA processing of T2 FLAIR.
#
# Author: Gaurav Bhalerao <gaurav.bhalerao@psych.ox.ac.uk>
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,C0301
#

import os.path as op
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_unbiased:                           In,
        T2_FLAIR_unbiased:                     In,
        T1_to_MNI_warp_coef_inv:               In,
        T1_to_MNI_linear_mat:                  In,
        T1_fast_pve_0:                         In,
        T1_unbiased_brain:                     In,
        T1_brain_mask:                         In,
        logs_dir:                              Ref,
        T1_dir:                                Ref,
        T2_FLAIR_dir:                          Ref,
        T2_FLAIR_bianca_lesion_dir:            Ref,
        T1_transforms_dir:                     Ref,
        T1_unbiased_brain_mask:                Out,
        T1_unbiased_bianca_mask:               Out,
        T1_unbiased_ventmask:                  Out,
        bianca_T1_unbiased_brain_mask:         Out,
        bianca_T1_unbiased_bianca_mask:        Out,
        bianca_T1_unbiased_ventmask:           Out,
        T2_FLAIR_bianca_conf_file:             Out,
        T2_FLAIR_bianca_mask:                  Out,
        T2_FLAIR_bianca_final_mask:            Out,
        T2_FLAIR_bianca_deepwm_map:            Out,
        T2_FLAIR_bianca_perivent_map:          Out,
        T2_FLAIR_bianca_tot_pvent_deep_volume: Out,
        T2_FLAIR_bianca_volume:                Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        # TODO: Figure out how to find WMHs if there's no trained BIANCA
        #       Alternatively: Think about what to produce (empty mask?)
        #       if there is no trained BIANCA Model and no alternative
        bianca_class_data = ctx.get('bianca_class_data', '')
        if op.exists(bianca_class_data):
            # Create bianca mask
            wrappers.make_bianca_mask(T1_unbiased, T1_fast_pve_0,
                                      T1_to_MNI_warp_coef_inv, keep_files=False)

            copyfile(src=T1_unbiased_brain_mask,
                     dst=bianca_T1_unbiased_brain_mask)
            copyfile(src=T1_unbiased_bianca_mask,
                     dst=bianca_T1_unbiased_bianca_mask)
            copyfile(src=T1_unbiased_ventmask,
                     dst=bianca_T1_unbiased_ventmask)

            # Create masterfile for bianca
            filenames = [T1_unbiased_brain, T2_FLAIR_unbiased,
                         T1_to_MNI_linear_mat]
            with open(T2_FLAIR_bianca_conf_file, 'w', encoding="utf-8") as f:
                for j in filenames:
                    f.write(j+" ")

            # Run bianca
            bianca_class_data = ctx.get_data("bianca/bianca_class_data")
            wrappers.bianca(singlefile=T2_FLAIR_bianca_conf_file,
                            querysubjectnum=1, brainmaskfeaturenum=1,
                            loadclassifierdata=bianca_class_data,
                            matfeaturenum=3, featuresubset="1,2",
                            o=T2_FLAIR_bianca_mask)

            # Multiply the lesions mask (bianca_mask)
            # by the "unbiased_bianca_mask"
            wrappers.fslmaths(T2_FLAIR_bianca_mask).\
                mul(bianca_T1_unbiased_bianca_mask).\
                thr(0.8).bin().run(T2_FLAIR_bianca_final_mask)

            # Calculate total WMH (volume of the final mask),
            # periventricular WMH & deep WMH - 10mm Criteria
            wrappers.bianca_perivent_deep(wmh_map=T2_FLAIR_bianca_final_mask,
                                          vent_mask=bianca_T1_unbiased_ventmask,
                                          minclustersize=0,
                                          outputdir=T2_FLAIR_bianca_lesion_dir,
                                          do_stats=2)

            vals = wrappers.fslstats(T2_FLAIR_bianca_final_mask).V.run()[0]
            with open(T2_FLAIR_bianca_volume, 'wt', encoding="utf-8") as f:
                f.write(str(vals) + "\n")
