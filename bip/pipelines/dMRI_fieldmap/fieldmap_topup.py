#!/usr/bin/env python
#
# fieldmap_topup.py - Sub-pipeline with the fieldmap generation with topup
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        B0_AP_PA:            In,
        acqparams:           In,
        logs_dir:            Ref,
        fieldmap_out_prefix: Ref,
        fieldmap_jacout:     Ref,
        fieldmap_fout:       Out):

    with redirect_logging(job_name(run), outdir=logs_dir):
        wrappers.topup(imain=B0_AP_PA,
                       datain=acqparams,
                       config="b02b0.cnf",
                       out=fieldmap_out_prefix,
                       fout=fieldmap_fout,
                       jacout=fieldmap_jacout,
                       verbose=True)
