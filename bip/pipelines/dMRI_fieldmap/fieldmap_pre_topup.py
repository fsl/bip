#!/usr/bin/env python
#
# fieldmap_pre_topup.py - Sub-pipeline with the fieldmap processing before topup
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R0912,C0123,R1704
#

import os.path as op
import logging
import nibabel as nib
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils       import redirect_logging, tempdir, job_name
from bip.pipelines.dMRI_fieldmap.fieldmap_pre_topup_fnc import choose_best_B0
from bip.pipelines.dMRI_fieldmap.fieldmap_pre_topup_fnc import generate_acqparams

log = logging.getLogger(__name__)

def run(ctx,
        AP:               In,
        PA:               In,
        AP_bval:          In,
        PA_bval:          In,
        AP_json:          In,
        PA_json:          In,
        AP_indices:       Out,
        PA_indices:       Out,
        total_B0_AP:      Out,
        total_B0_PA:      Out,
        B0_AP:            Out,
        B0_PA:            Out,
        AP_best_index:    Out,
        PA_best_index:    Out,
        B0_AP_PA:         Out,
        acqparams:        Out,
        AP_fieldmap_flag: Ref,
        PA_fieldmap_flag: Ref,
        logs_dir:         Ref,
        tmp_dir:          Ref):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        AP_tmp = op.join(tmp_dir, 'AP_tmp_')
        PA_tmp = op.join(tmp_dir, 'PA_tmp_')

        choose_best_B0(ctx, AP, AP_bval, total_B0_AP, AP_indices, AP_tmp,
                       AP_best_index, B0_AP, AP_fieldmap_flag, tmp_dir)
        choose_best_B0(ctx, PA, PA_bval, total_B0_PA, PA_indices, PA_tmp,
                       PA_best_index, B0_PA, PA_fieldmap_flag, tmp_dir)

        wrappers.fslmerge("t", B0_AP_PA, B0_AP, B0_PA)

        generate_acqparams(AP, PA, AP_json, PA_json, 1, 1, acqparams)

        # Improvement by Kevin Anderson on 20/12/2017
        # This guarantees that the number of slices is even
        im_AP_PA = nib.load(B0_AP_PA)
        Zslices  = im_AP_PA.header['dim'][3]

        if (Zslices % 2) != 0:
            wrappers.fslroi(B0_AP_PA, B0_AP_PA, 0, -1, 0, -1, 0, Zslices - 1)
        # TODO: Discuss this with Jesper.
