#!/usr/bin/env python
#
# dMRI_fieldmap.py - Pipeline with the fieldmap processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1735
#

import logging
from bip.utils.log_utils         import job_name
from bip.pipelines.dMRI_fieldmap import fieldmap_pre_topup, fieldmap_topup
from bip.pipelines.dMRI_fieldmap import fieldmap_post_topup

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree):

    subj = ctx.subject

    pipe(fieldmap_pre_topup.run,
         submit=dict(jobtime=200, name=job_name(fieldmap_pre_topup.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(fieldmap_topup.run,
         submit=dict(jobtime=200, name=job_name(fieldmap_topup.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(fieldmap_post_topup.run,
         submit=dict(jobtime=200, name=job_name(fieldmap_post_topup.run, subj)),
         kwargs={'ctx' : ctx})
