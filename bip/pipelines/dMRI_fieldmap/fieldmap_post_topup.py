#!/usr/bin/env python
#
# fieldmap_post_topup.py - Sub-pipeline with the fieldmap processing after topup
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=R1704
#

import os.path as op
import logging
from shutil import copyfile
import numpy as np
import nibabel as nib
from fsl_pipe import In, Out, Ref
from gradunwarp.core.gradient_unwarp_apply import gradient_unwarp_apply
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1:                             In,
        T1_brain:                       In,
        T1_brain_mask:                  In,
        T1_fast_WM_mask:                In,
        B0_AP:                          In,
        B0_PA:                          In,
        acqparams:                      In,
        fieldmap_fout:                  In,
        logs_dir:                       Ref,
        tmp_dir:                        Ref,
        fieldmap_out_prefix:            Ref,
        fieldmap_GDC:                   Ref,
        T1_brain_mask_dil:              Out,
        T1_to_fieldmap_iout_mat:        Out,
        fieldmap_iout:                  Out,
        fieldmap_iout_mean:             Out,
        fieldmap_iout_mean_ud:          Out,
        fieldmap_iout_mean_ud_warp:     Out,
        fieldmap_iout_to_T1:            Out,
        fieldmap_fout_to_T1:            Out,
        fieldmap_fout_to_T1_brain:      Out,
        fieldmap_fout_to_T1_brain_rad:  Out,
        fieldmap_iout_to_T1_mat:        Out,
        fieldmap_mask:                  Out,
        fieldmap_mask_ud:               Out,
        fieldmap_iout_mean_ud_inv_warp: Out):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        B0_AP_corr_tmp  = op.join(tmp_dir, 'B0_AP_corr_tmp.nii.gz')
        B0_PA_corr_tmp  = op.join(tmp_dir, 'B0_PA_corr_tmp.nii.gz')
        B0_AP_fixed_tmp = op.join(tmp_dir, 'B0_AP_fixed_tmp.nii.gz')
        B0_PA_fixed_tmp = op.join(tmp_dir, 'B0_PA_fixed_tmp.nii.gz')
        fieldmap_tmp    = op.join(tmp_dir, 'fieldmap_tmp.nii.gz')

        # Get corrected B=0 for AP and PA and average them avoiding zero values
        wrappers.applytopup(imain=B0_AP, datain=acqparams, index=1,
                            topup=fieldmap_out_prefix,
                            out=B0_AP_corr_tmp, method="jac")
        wrappers.applytopup(imain=B0_PA, datain=acqparams, index=2,
                            topup=fieldmap_out_prefix,
                            out=B0_PA_corr_tmp, method="jac")

        B0_AP_corr_tmp_img = nib.load(B0_AP_corr_tmp)
        B0_PA_corr_tmp_img = nib.load(B0_PA_corr_tmp)

        B0_AP_corr_tmp_imgf = B0_AP_corr_tmp_img.get_fdata()
        B0_PA_corr_tmp_imgf = B0_PA_corr_tmp_img.get_fdata()

        # Get a list of indices the zero-valued voxels for AP and PA
        x1, y1, z1 = np.where(B0_AP_corr_tmp_imgf == 0)
        x2, y2, z2 = np.where(B0_PA_corr_tmp_imgf == 0)

        # For AP voxels with 0 value, get the values in PA  (And viceversa)
        B0_AP_fixed_tmp_imgf = B0_AP_corr_tmp_imgf
        B0_PA_fixed_tmp_imgf = B0_PA_corr_tmp_imgf
        B0_AP_fixed_tmp_imgf[x1, y1, z1] = B0_PA_corr_tmp_imgf[x1, y1, z1]
        B0_PA_fixed_tmp_imgf[x2, y2, z2] = B0_AP_corr_tmp_imgf[x2, y2, z2]

        B0_AP_fixed_tmp_img = nib.Nifti1Image(B0_AP_fixed_tmp_imgf,
                                              B0_AP_corr_tmp_img.affine,
                                              B0_AP_corr_tmp_img.header)

        B0_PA_fixed_tmp_img = nib.Nifti1Image(B0_PA_fixed_tmp_imgf,
                                              B0_PA_corr_tmp_img.affine,
                                              B0_PA_corr_tmp_img.header)

        nib.save(B0_AP_fixed_tmp_img, B0_AP_fixed_tmp)
        nib.save(B0_PA_fixed_tmp_img, B0_PA_fixed_tmp)

        # Merge and average them
        wrappers.fslmerge("t", fieldmap_iout, B0_AP_fixed_tmp, B0_PA_fixed_tmp)
        wrappers.fslmaths(fieldmap_iout).Tmean().run(fieldmap_iout_mean)

        gdc = ctx.get('gdc', None)

        if gdc not in (None, "", "none"):
            # Calculate and apply the Gradient Distortion Unwarp
            # TODO: Review the "half=True" in next version
            gradient_unwarp_apply(WD=fieldmap_GDC, infile=fieldmap_iout_mean,
                                  outfile=fieldmap_iout_mean_ud,
                                  owarp=fieldmap_iout_mean_ud_warp,
                                  gradcoeff=gdc,
                                  vendor='siemens', nojac=True, half=True)
        else:
            copyfile(src=fieldmap_iout_mean, dst=fieldmap_iout_mean_ud)

        wrappers.fslcpgeom(B0_AP, fieldmap_fout)

        # Get the topup iout (magnitude) to struct space and
        # apply the transformation to fout (fieldmap)
        wrappers.epi_reg(epi=fieldmap_iout_mean_ud, t1=T1, t1brain=T1_brain,
                         out=fieldmap_iout_to_T1, wmseg=T1_fast_WM_mask)
        wrappers.applywarp(src=fieldmap_fout, ref=T1,
                           out=fieldmap_fout_to_T1,
                           w=fieldmap_iout_mean_ud_warp,
                           postmat=fieldmap_iout_to_T1_mat,
                           rel=True, interp='spline')

        # Mask the warped fout (fieldmap) using T1 brain mask
        wrappers.fslmaths(fieldmap_fout_to_T1).mul(T1_brain_mask).\
            run(fieldmap_fout_to_T1_brain)

        # Multiply the warped & masked fout (fieldmap)
        # by 2*Pi to have it in radians
        wrappers.fslmaths(fieldmap_fout_to_T1_brain).mul(2*np.pi).\
            run(fieldmap_fout_to_T1_brain_rad)

        # Generate a mask for topup output by inverting the
        # previous registration and applying it to T1 brain mask
        wrappers.invxfm(inmat=fieldmap_iout_to_T1_mat,
                        omat=T1_to_fieldmap_iout_mat)
        wrappers.fslmaths(T1_brain_mask).thr(0.1).run(fieldmap_tmp)
        wrappers.fslmaths(fieldmap_tmp).kernel("sphere", 1.1).dilF().\
            run(fieldmap_tmp)
        wrappers.fslmaths(fieldmap_tmp).bin().fillh().run(T1_brain_mask_dil)
        wrappers.applyxfm(src=T1_brain_mask_dil, ref=fieldmap_iout_mean,
                          mat=T1_to_fieldmap_iout_mat, out=fieldmap_mask_ud,
                          interp="trilinear")
        wrappers.fslmaths(fieldmap_mask_ud).thr(0.25).bin().\
            run(fieldmap_mask_ud)

        # Warp the dilated T1 brain mask to the
        # Gradient Distorted space by inverting
        wrappers.invwarp(ref=fieldmap_iout_mean,
                         warp=fieldmap_iout_mean_ud_warp,
                         out=fieldmap_iout_mean_ud_inv_warp)
        wrappers.applywarp(src=T1_brain_mask_dil, ref=fieldmap_iout_mean,
                           out=fieldmap_mask,
                           premat=T1_to_fieldmap_iout_mat,
                           w=fieldmap_iout_mean_ud_inv_warp,
                           rel=True, interp='trilinear')
        wrappers.fslmaths(fieldmap_mask).thr(0.25).bin().fillh().\
            run(fieldmap_mask)
