#!/usr/bin/env python
#
# fieldmap_pre_topup.py - Sub-pipeline with the fieldmap processing before topup
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R0912,W0612,C0123,C0301
#

import os.path as op
import glob
import logging
from shutil import copyfile
import numpy as np
import nibabel as nib
from fsl import wrappers
from bip.utils.get_b0s         import get_b0s
from bip.utils.get_dwell_time  import get_dwell_time
from bip.utils.read_json_field import read_json_field

log = logging.getLogger(__name__)

def choose_best_B0(ctx, img, bval, total_B0, indices, tmp, best_index, B0,
                   fieldmap_flag, tmp_dir):

    pre_topup     = op.join(tmp_dir, 'pre_topup.nii.gz')
    pre_topup_mat = op.join(tmp_dir, 'pre_topup.mat')

    b0_threshold = ctx.get('dMRI_b0_threshold', 100)

    bvals = np.loadtxt(bval)

    if type(bvals) is not np.ndarray:
        num_vols = 1
    else:
        num_vols = len(np.where(bvals <= b0_threshold)[0])

    get_b0s(img, bval, total_B0, indices, num_vols,  b0_threshold)
    ind_values = [int(x) for x in np.loadtxt(indices)]

    wrappers.fslsplit(total_B0, tmp)

    files = glob.glob(tmp + '*')
    files.sort()

    N = len(files)

    if N == 1:
        with open(best_index, 'wt', encoding="utf-8") as f:
            f.write('0\n')
        copyfile(src=total_B0, dst=B0)
    else:
        scores = np.zeros([N, N])
        for i in range(N):
            for j in range(N):
                if j > i:
                    wrappers.flirt(src=files[i], ref=files[j], nosearch=True,
                                   dof=6, out=pre_topup, omat=pre_topup_mat)
                    im1  = nib.load(pre_topup)
                    im2  = nib.load(files[j])
                    im1f = im1.get_fdata().flatten()
                    im2f = im2.get_fdata().flatten()
                    corr = np.corrcoef(im1f, im2f)[0, 1]
                    scores[i, j] = corr
                    scores[j, i] = corr

        final_scores = np.sum(scores, axis=1) / (N - 1)

        best_ind = final_scores.argmax()
        best_score = final_scores[best_ind]

        # If the correlation of the first B0 is very good
        # (e.g. greater than 0.98), we simply keepy it,
        # even if it is not the best correlation.
        if final_scores[0] > ctx.get('dMRI_upper_threshold_best_b0', 0.98):
            best_ind = 0
            best_score = final_scores[best_ind]

        # If the best correlation of the first B0 is low
        # (e.g. lower than 0.98), there is probably a problem
        if best_score < ctx.get('dMRI_lower_threshold_best_b0', 0.95):
            with open(fieldmap_flag, 'wt', encoding="utf-8") as f:
                f.write(f'The best score was {best_score}.' +
                        'The subject will probably have problems ' +
                        'with this DWI data\n')
        with open(best_index, 'wt', encoding="utf-8") as f:
            f.write(f'{ind_values[best_ind]}\n')
            copyfile(src=files[best_ind], dst=B0)

def get_acqparams_row(phase_encode, im, im_json):
    """
    Creates a row for the acqparams file from topup based

    Args:
        phase_encode: phase encode direction (string with two elements, first
                      one should be i, j, or k, second one should be + or -)
        im:           image file to get the number of lines to calculate the
                      readout time
        im_json:      json file corresponding to the previous image
    """

    dimensions = ('i', 'j', 'k')
    elements = ['0', '0', '0']
    if phase_encode in dimensions:
        dimension = dimensions.index(phase_encode)
        elements[dimension] = '1'
    elif len(phase_encode) == 2:
        orient, sign = phase_encode
        if sign in dimensions:
            orient, sign = sign, orient
        if orient not in dimensions or sign not in '+-':
            raise ValueError('Unrecognised phase encode direction ' +
                             f'{phase_encode}; should by like i- or j+')
        dimension = dimensions.index(orient)
        elements[dimension] = '1' if sign == '+' else '-1'
    else:
        raise ValueError(f'Unrecognised phase encode direction {phase_encode}')

    imAP = nib.load(im)
    numlines = imAP.header['dim'][dimension]
    dtiDwell = get_dwell_time(im, im_json)
    topupValue = (dtiDwell * (numlines - 1)) / 1000.0

    return ' '.join(elements) + ' ' + str(topupValue) + '\n'


def generate_acqparams(AP, PA, AP_json, PA_json, numAP, numPA, acqparams):

    enc_dir_AP = read_json_field(fileName=AP_json,
                                 fieldName="PhaseEncodingDirection")
    enc_dir_PA = read_json_field(fileName=PA_json,
                                 fieldName="PhaseEncodingDirection")

    # TODO: Write a way to evaluate that they are indeed opposite directions
    values_AP = get_acqparams_row(enc_dir_AP, AP, AP_json)
    values_PA = get_acqparams_row(enc_dir_PA, PA, PA_json)

    with open(acqparams, 'wt', encoding="utf-8") as f:
        for i in range(numAP):
            f.write(values_AP)
        for i in range(numPA):
            f.write(values_PA)
