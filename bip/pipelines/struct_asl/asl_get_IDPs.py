#!/usr/bin/env python
#
# asl_get_IDPs.py - Sub-pipeline generating the IDPs of the ASL pipeline.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1718
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        region_analysis_gm_csv:  In,
        logs_dir:                Ref,
        ASL_region_analysis_dir: Ref,
        ASL_IDPs:                Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        result = ctx.subject

        asl_format_file = ctx.get_data("asl/asl_format.txt")

        with open(asl_format_file, 'rt', encoding="utf-8") as f:
            asl_format = [x.strip().split() for x in f.readlines()]

        regions = set([x[0] for x in asl_format])

        for region in regions:
            file_name_1 = op.join(ASL_region_analysis_dir, region + ".csv")
            file_name_2 = op.join(ASL_region_analysis_dir, region + ".txt")

            with open(file_name_1, 'rt', encoding="utf-8") as f:
                file_data = f.read()

            file_data = file_data.replace(", ", "_-_")
            file_data = file_data.replace('"', "")
            file_data = file_data.replace(" ", "_")

            with open(file_name_2, 'wt', encoding="utf-8") as f:
                f.write(file_data)

        for fil, row, column in asl_format:

            file_name = op.join(ASL_region_analysis_dir, fil + ".txt")

            with open(file_name, 'rt', encoding="utf-8") as f:
                file_data = [x.strip().split(",") for x in f.readlines()]

            for line in file_data:
                if line[0] == row:
                    # The -1 is needed because Python starts with 0
                    val = line[int(column)-1]
            if val == "":
                val = "NaN"

            result += " " + val

        with open(ASL_IDPs, 'wt', encoding="utf-8") as f:
            f.write(result + "\n")
           