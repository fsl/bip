#!/usr/bin/env python
#
# asl_proc.py - Sub-pipeline with the ASL main processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os
import os.path as op
import glob
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from gradunwarp.core.gradient_unwarp_apply import gradient_unwarp_apply
from fsl import wrappers
from bip.utils.log_utils       import redirect_logging, job_name
from bip.utils.read_json_field import read_json_field

log = logging.getLogger(__name__)

def run(ctx,
        T1:                            In,
        T1_brain:                      In,
        T1_to_MNI_warp:                In,
        fieldmap_fout_to_T1_brain_rad: In,
        ASL_M0:                        In,
        ASL_M0_json:                   In,
        logs_dir:                      Ref,
        ASL_GDC:                       Ref,
        OXASL_ra_dir:                  Ref,
        T1_fast_prefix:                Ref,
        ASL_PLD_prefix:                Ref,
        ASL_raw_dir:                   Ref,
        ASL_M0_ud:                     Out,
        ASL_M0_ud_warp:                Out,
        ASL_DATA_wrongorder:           Out,
        ASL_DATA:                      Out,
        ASL_DATA_diff:                 Out,
        ASL_DATA_diff_mean:            Out,
        ASL_control:                   Out,
        ASL_label:                     Out,
        CALIB:                         Out,
        CALIB_json:                    Out,
        ASL_qc_modelfit:               Out,
        ASL_mni_right_amygdala:        Out,
        ASL_masks_right_amygdala:      Out,
        ASL_native_right_amygdala:     Out,
        ASL_struct_90_wm:              Out,
        ASL_calib_M0:                  Out,
        ASL_struct_ACBV_calib:         Out,
        ASL_std_ACBV_calib:            Out,
        region_analysis_gm_csv:        Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        BBASL_ROI_DIR = ctx.get_data("asl/ukb_rois/")

        ###############################################
        # Initial check for Phase Encoding Directions #
        ###############################################
        list_PEDs = []

        for fil in glob.glob(op.join(ASL_raw_dir, "*.json")):
            list_PEDs.append(read_json_field(fileName=fil,
                                             fieldName="PhaseEncodingDirection"))

        if len(list_PEDs) != 12:
            error_msg =  "ERROR: Wrong phase-encoding-direction in some of the "
            error_msg += "files (or wrong number of files). "
            error_msg += "ASL will not be processed."
            print(error_msg)
            # TODO: Make unusable this modality
            # bb_make_unusable ASL $src "2 Wrong_acquisition"

        ##################
        # Pre-processing #
        ##################
        list_fils_control = glob.glob(ASL_PLD_prefix + "*_control.nii.gz")
        list_fils_control.sort()
        list_fils_label = glob.glob(ASL_PLD_prefix + "*_label.nii.gz")
        list_fils_label.sort()

        wrappers.fslmerge("t", ASL_control, *list_fils_control)
        wrappers.fslmerge("t", ASL_label, *list_fils_label)
        wrappers.fslmerge("t", ASL_DATA_wrongorder, ASL_label, ASL_control)

        rel_path = op.relpath(ASL_M0, op.dirname(CALIB))
        os.symlink(rel_path, CALIB)

        rel_path = op.relpath(ASL_M0_json, op.dirname(CALIB_json))
        os.symlink(rel_path, CALIB_json)

        TE = read_json_field(fileName=CALIB_json, fieldName="EchoTime",
                             rounding=3, multFactor=1000)

        # Gradient distortion correction applied to the M0
        gdc = ctx.get('gdc', None)

        if gdc not in (None, "", "none"):
            # Calculate and apply the Gradient Distortion Unwarp
            # TODO: Review the "half=True" in next version
            gradient_unwarp_apply(WD=ASL_GDC, infile=CALIB, outfile=ASL_M0_ud,
                                  owarp=ASL_M0_ud_warp, gradcoeff=gdc,
                                  vendor='siemens', nojac=True, half=False)
        else:
            copyfile(src=CALIB, dst=ASL_M0_ud)

        ##############
        # Processing #
        ##############
        # Use asl_file to get into alternating tag-control
        wrappers.asl_file(data=ASL_DATA_wrongorder, out=ASL_DATA, ntis=5,
                          iaf="tcb")

        # PWI generation - this is more of a sanity check than anything else
        # now use ASL file to get difference images
        wrappers.asl_file(data=ASL_DATA, out=ASL_DATA_diff,
                          mean=ASL_DATA_diff_mean, ntis=5, iaf="tc", diff=True)

        #######################
        # PERFUSION ANALSYSIS #
        #######################

        raa_list = [BBASL_ROI_DIR + "MNI_seg_max_prob_masked_RandL.nii.gz",
                    BBASL_ROI_DIR + "HO_L_Cerebral_WM_thr80.nii.gz",
                    BBASL_ROI_DIR + "HO_R_Cerebral_WM_thr80.nii.gz",
                    BBASL_ROI_DIR + "VascularTerritories_ero.nii.gz"]
        raa_labels_list = [BBASL_ROI_DIR + "MNI_seg_max_prob_masked_RandL.txt",
                           BBASL_ROI_DIR + "HO_L_Cerebral_WM_thr80.txt",
                           BBASL_ROI_DIR + "HO_R_Cerebral_WM_thr80.txt",
                           BBASL_ROI_DIR + "VascularTerritories_ero.txt"]

        wrappers.oxford_asl(data=ASL_DATA,
                            out=OXASL_ra_dir,
                            c=CALIB,
                            s=T1,
                            regfrom_method="pwi",
                            reg_init_bbr=True,
                            sbrain=T1_brain,
                            fastsrc=T1_fast_prefix,
                            warp=T1_to_MNI_warp,
                            fmap=fieldmap_fout_to_T1_brain_rad,
                            fmapmag=T1,
                            fmapmagbrain=T1_brain,
                            gdcwarp=ASL_M0_ud_warp,
                            iaf="tc",
                            ibf="rpt",
                            mc="on",
                            tis=[2.2, 2.6, 3.0, 3.4, 3.8],
                            casl=True,
                            bolus=1.8,
                            fixbolus=True,
                            cgain=10,
                            spatial=True,
                            tr=5,
                            te=TE,
                            echospacing=0.0005,
                            pedir="x",
                            gm_thresh=0.7,
                            cmethod="voxel",
                            nofmapreg=True,
                            region_analysis=True,
                            region_analysis_atlas=raa_list,
                            region_analysis_atlas_labels=raa_labels_list,
                            region_analysis_save_rois=True,
                            qc_output=True)
