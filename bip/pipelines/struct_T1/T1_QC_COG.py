#!/usr/bin/env python
#
# T1_QC_COG.py - Sub-pipeline with the QC (Calculate Center of Gravity) of T1w.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=R1704,W1203
#

import os.path as op
import logging
import numpy as np
import nibabel as nib
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1:            In,
        T1_brain_mask: In,
        T1_QC_COG:     Out,
        logs_dir:      Ref,
        tmp_dir:       Ref):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        T1_tmp_5 = op.join(tmp_dir, 'T1_tmp_5.nii.gz')
        T1_tmp_6 = op.join(tmp_dir, 'T1_tmp_6.nii.gz')

        refSubj = ctx.get_data('MNI/MNI152_T1_1mm_BigFoV_facemask.nii.gz')

        COG_coord = wrappers.fslstats(T1_brain_mask).C.run()
        log.info(f'COG from {T1_brain_mask} : {COG_coord}')
        T1_img = nib.load(T1)

        qform = T1_img.get_qform()
        log.info(f'qform from {T1} : {qform}')

        new_coord = qform.dot(np.append(COG_coord, 1))
        new_x = new_coord[0]
        new_z = new_coord[2]

        wrappers.fslmaths(T1_brain_mask).thr(0.5).bin().run(T1_tmp_5)
        wrappers.flirt(src=T1_tmp_5, ref=refSubj, out=T1_tmp_6, applyxfm=True,
                       usesqform=True, interp='nearestneighbour')

        new_y = wrappers.fslstats(T1_tmp_6).w.run()[2]

        log.info(f'CoG coords: {[new_x, new_y, new_z]}')

        np.savetxt(T1_QC_COG, [new_x, new_y, new_z], fmt='%.6f', newline=' ')
