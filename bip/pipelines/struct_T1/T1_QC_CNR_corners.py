#!/usr/bin/env python
#
# T1_QC_CNR_corners.py - Sub-pipeline with the QC (CNR in the corners) of T1w.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1704
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1:                      In,
        T1_orig:                 In,
        T1_notNorm:              In,
        T1_fast_GM_mask:         In,
        T1_fast_WM_mask:         In,
        T1_orig_QC_CNR_lower:    Out,
        T1_orig_QC_CNR_upper:    Out,
        T1_notNorm_QC_CNR_lower: Out,
        T1_notNorm_QC_CNR_upper: Out,
        logs_dir:                Ref,
        tmp_dir:                 Ref):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        roi_1_1     = op.join(tmp_dir, 'roi_1_1.nii.gz')
        roi_1_245   = op.join(tmp_dir, 'roi_1_245.nii.gz')
        roi_197_1   = op.join(tmp_dir, 'roi_197_1.nii.gz')
        roi_197_245 = op.join(tmp_dir, 'roi_197_245.nii.gz')
        roi_x_1     = op.join(tmp_dir, 'roi_x_1.nii.gz')
        roi_x_245   = op.join(tmp_dir, 'roi_x_245.nii.gz')
        roi_lower   = op.join(tmp_dir, 'roi_lower.nii.gz')
        roi_upper   = op.join(tmp_dir, 'roi_upper.nii.gz')

        in_files   = [T1_orig, T1_notNorm]
        out_files_lo = [T1_orig_QC_CNR_lower, T1_notNorm_QC_CNR_lower]
        out_files_up = [T1_orig_QC_CNR_upper, T1_notNorm_QC_CNR_upper]

        for i in [0, 1]:
            # Lower corners
            wrappers.fslroi(in_files[i], roi_1_1,       1, 10, 1,   10, 0, 10)
            wrappers.fslroi(in_files[i], roi_1_245,     1, 10, 245, 10, 0, 10)
            wrappers.fslroi(in_files[i], roi_197_1,   197, 10, 1,   10, 0, 10)
            wrappers.fslroi(in_files[i], roi_197_245, 197, 10, 245, 10, 0, 10)

            wrappers.fslmerge("x", roi_x_1,   roi_1_1,   roi_197_1)
            wrappers.fslmerge("x", roi_x_245, roi_1_245, roi_197_245)
            wrappers.fslmerge("y", roi_lower, roi_x_1,   roi_x_245)

            # Upper corners
            wrappers.fslroi(in_files[i], roi_1_1,       1, 10, 1,   10, 245, 10)
            wrappers.fslroi(in_files[i], roi_1_245,     1, 10, 245, 10, 245, 10)
            wrappers.fslroi(in_files[i], roi_197_1,   197, 10, 1,   10, 245, 10)
            wrappers.fslroi(in_files[i], roi_197_245, 197, 10, 245, 10, 245, 10)

            wrappers.fslmerge("x", roi_x_1,   roi_1_1,   roi_197_1)
            wrappers.fslmerge("x", roi_x_245, roi_1_245, roi_197_245)
            wrappers.fslmerge("y", roi_upper, roi_x_1,   roi_x_245)

            grey   = wrappers.fslstats(T1).k(T1_fast_GM_mask).m.run()
            white  = wrappers.fslstats(T1).k(T1_fast_WM_mask).m.run()
            threshold = wrappers.fslstats(T1).k(T1_fast_GM_mask).P(1).run() / 2

            wrappers.fslmaths(roi_lower).uthr(threshold).run(roi_lower)
            wrappers.fslmaths(roi_upper).uthr(threshold).run(roi_upper)

            noise_lower = wrappers.fslstats(roi_lower).S.run()
            noise_upper = wrappers.fslstats(roi_upper).S.run()

            brain = (grey + white) / 2

            result_lower = noise_lower / brain
            result_upper = noise_upper / brain

            with open(out_files_lo[i], 'wt', encoding="utf-8") as f:
                f.write(f'{result_lower}\n')
            with open(out_files_up[i], 'wt', encoding="utf-8") as f:
                f.write(f'{result_upper}\n')
