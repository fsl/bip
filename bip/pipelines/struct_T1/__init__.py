#!/usr/bin/env python
#
# struct_T1.py - Pipeline with the T1w processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1735
#

import logging
from bip.utils.log_utils     import job_name
from bip.pipelines.struct_T1 import T1_gdc, T1_brain_extract, T1_defacing
from bip.pipelines.struct_T1 import T1_fast, T1_first, T1_sienax, T1_QC_COG
from bip.pipelines.struct_T1 import T1_QC_CNR_corners, T1_QC_CNR_eyes

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree):

    subj = ctx.subject

    pipe(T1_gdc.run,
         submit=dict(jobtime=200, name=job_name(T1_gdc.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T1_brain_extract.run,
         submit=dict(jobtime=200, name=job_name(T1_brain_extract.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T1_defacing.run,
         submit=dict(jobtime=200, name=job_name(T1_defacing.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T1_fast.run,
         submit=dict(jobtime=200, name=job_name(T1_fast.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T1_first.run,
         submit=dict(jobtime=200, name=job_name(T1_first.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T1_sienax.run,
         submit=dict(jobtime=200, name=job_name(T1_sienax.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T1_QC_COG.run,
         submit=dict(jobtime=200, name=job_name(T1_QC_COG.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T1_QC_CNR_corners.run,
         submit=dict(jobtime=200, name=job_name(T1_QC_CNR_corners.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(T1_QC_CNR_eyes.run,
         submit=dict(jobtime=200, name=job_name(T1_QC_CNR_eyes.run, subj)),
         kwargs={'ctx' : ctx})
