#!/usr/bin/env python
#
# T1_sienax.py - Sub-pipeline with the SienaX processing of the T1w.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=R1704
#

import os
import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from fsl.transform import affine, flirt
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1:                                  In,
        T1_brain:                            In,
        T1_fast_pve_0:                       In,
        T1_fast_pve_1:                       In,
        T1_fast_pve_2:                       In,
        T1_to_MNI_linear_mat:                In,
        T1_to_MNI_warp_coef_inv:             In,
        T1_sienax_brain_skull:               Out,
        T1_sienax_to_MNI_linear:             Out,
        T1_sienax_to_MNI_linear_mat:         Out,
        T1_sienax_brain_skull_to_MNI_linear: Out,
        T1_sienax_segperiph:                 Out,
        T1_sienax_segvent:                   Out,
        T1_sienax_pve_0_segperiph:           Out,
        T1_sienax_pve_1_segperiph:           Out,
        T1_sienax_report:                    Out,
        T1_sienax_brain:                     Ref,
        logs_dir:                            Ref,
        tmp_dir:                             Ref):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        T1_tmp_mat_1 = op.join(tmp_dir, 'tmp_mat_1.mat')
        T1_tmp_mat_2 = op.join(tmp_dir, 'tmp_mat_2.mat')
        T1_tmp_mat_3 = op.join(tmp_dir, 'tmp_mat_3.mat')

        MNI               = ctx.get_standard('MNI152_T1_1mm.nii.gz')
        MNI_2mm_brain     = ctx.get_standard('MNI152_T1_2mm_brain.nii.gz')
        MNI_2mm_skull     = ctx.get_standard('MNI152_T1_2mm_skull.nii.gz')
        MNI_2mm_structseg = ctx.get_standard('MNI152_T1_2mm_strucseg.nii.gz')
        MNI_2mm_segperiph = ctx.get_standard('MNI152_T1_2mm_strucseg_periph.nii.gz')

        report = []

        wrappers.bet(T1, T1_sienax_brain, s=True)
        os.remove(T1_sienax_brain)

        # These 4 lines are equivalent to sienax's pairreg command:
        #     pairreg ${FSLDIR}/data/standard/MNI152_T1_2mm_brain T1_brain
        #             ${FSLDIR}/data/standard/MNI152_T1_2mm_skull T1_brain_skull
        #             T1_to_MNI_linear.mat
        wrappers.flirt(src=T1_brain, ref=MNI_2mm_brain, omat=T1_tmp_mat_1,
                       schedule=op.join(ctx.FSLDIR, 'etc', 'flirtsch',
                                        'pairreg1.sch'),
                       interp="trilinear")
        wrappers.flirt(src=T1_sienax_brain_skull, ref=MNI_2mm_skull,
                       omat=T1_tmp_mat_2, init=T1_tmp_mat_1, interp="trilinear",
                       schedule=op.join(ctx.FSLDIR, 'etc', 'flirtsch',
                                        'pairreg2.sch'))
        wrappers.fixscaleskew(inmat1=T1_tmp_mat_1, inmat2=T1_tmp_mat_2,
                              omat=T1_tmp_mat_3)
        wrappers.flirt(src=T1_brain, ref=MNI_2mm_brain,
                       omat=T1_sienax_to_MNI_linear_mat, init=T1_tmp_mat_3,
                       schedule=op.join(ctx.FSLDIR, 'etc', 'flirtsch',
                                        'pairreg3.sch'),
                       interp="trilinear")

        matrix = flirt.readFlirt(T1_sienax_to_MNI_linear_mat)
        scales = affine.decompose(matrix)[0]

        vscale = float(scales[0]) * float(scales[1]) * float(scales[2])

        wrappers.applyxfm(src=T1, ref=MNI, out=T1_sienax_to_MNI_linear,
                          mat=T1_sienax_to_MNI_linear_mat, interp="spline")
        wrappers.applyxfm(src=T1_sienax_brain_skull, ref=MNI,
                          out=T1_sienax_brain_skull_to_MNI_linear,
                          mat=T1_sienax_to_MNI_linear_mat, interp="trilinear")

        wrappers.applywarp(src=MNI_2mm_segperiph, ref=T1,
                           w=T1_to_MNI_warp_coef_inv, out=T1_sienax_segperiph,
                           rel=True, interp='trilinear')
        wrappers.fslmaths(T1_sienax_segperiph).thr(0.5).bin().\
            run(T1_sienax_segperiph)

        wrappers.fslmaths(MNI_2mm_structseg).thr(4.5).bin().\
            run(T1_sienax_segvent)
        wrappers.applywarp(src=T1_sienax_segvent, ref=T1,
                           w=T1_to_MNI_warp_coef_inv, out=T1_sienax_segvent,
                           rel=True, interp='nn')

        wrappers.fslmaths(T1_fast_pve_1).mas(T1_sienax_segperiph).\
            run(T1_sienax_pve_1_segperiph, odt="float")
        V   = wrappers.fslstats(T1_sienax_pve_1_segperiph).m.v.run()
        xa  = float(V[0])
        xb  = float(V[2])
        uxg = xa * xb
        xg  = xa * xb * vscale

        report.append(f'VSCALING {vscale}')
        report.append('tissue             volume    unnormalised-volume')
        report.append(f'pgrey              {xg} {uxg} (peripheral grey)')

        wrappers.fslmaths(T1_fast_pve_0).mas(T1_sienax_segvent).\
            run(T1_sienax_pve_0_segperiph, odt="float")
        V   = wrappers.fslstats(T1_sienax_pve_0_segperiph).m.v.run()
        xa  = float(V[0])
        xb  = float(V[2])
        uxg = xa * xb
        xg  = xa * xb * vscale
        report.append(f'vcsf               {xg} {uxg} (ventricular CSF)')

        V     = wrappers.fslstats(T1_fast_pve_1).m.v.run()
        xa    = float(V[0])
        xb    = float(V[2])
        ugrey = xa * xb
        ngrey = xa * xb * vscale
        report.append(f'GREY               {ngrey} {ugrey}')

        V      = wrappers.fslstats(T1_fast_pve_2).m.v.run()
        xa     = float(V[0])
        xb     = float(V[2])
        uwhite = xa * xb
        nwhite = xa * xb * vscale
        report.append(f'WHITE              {nwhite} {uwhite}')

        ubrain = ugrey + uwhite
        nbrain = ngrey + nwhite
        report.append(f'BRAIN              {nbrain} {ubrain}')

        with open(T1_sienax_report, 'wt', encoding="utf-8") as f:
            for line in report:
                f.write(f'{line}\n')
