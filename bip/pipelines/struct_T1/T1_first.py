#!/usr/bin/env python
#
# T1_first.py - Sub-pipeline with FSL's FIRST processing of T1w.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os
import os.path as op
import glob
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_unbiased_brain:          In,
        T1_first_unbiased_brain:    Ref,
        T1_first_prefix:            Ref,
        logs_dir:                   Ref,
        T1_first_all_fast_firstseg: Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        # Creates a link inside T1_first to T1_unbiased_brain.nii.gz
        if T1_first_unbiased_brain is not None and \
                not op.exists(T1_first_unbiased_brain):
            rel_path = op.relpath(T1_unbiased_brain,
                                  op.dirname(T1_first_unbiased_brain))
            os.symlink(src=rel_path, dst=T1_first_unbiased_brain)
        wrappers.run_first_all(input=T1_first_unbiased_brain,
                               output=T1_first_prefix, b=True)

        for f in glob.glob(T1_first_prefix + "-*_first*.nii.gz"):
            if op.exists(f):
                os.remove(f)
        for f in glob.glob(T1_first_prefix + "-*_corr*.nii.gz"):
            if op.exists(f):
                os.remove(f)
