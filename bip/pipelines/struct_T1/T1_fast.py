#!/usr/bin/env python
#
# T1_fast.py -  Sub-pipeline with FSL's FAST processing of T1w.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1:                 In,
        T1_brain:           In,
        logs_dir:           Ref,
        T1_fast_dir:        Ref,
        T1_unbiased:        Out,
        T1_unbiased_brain:  Out,
        T1_fast_pve_0:      Out,
        T1_fast_pve_1:      Out,
        T1_fast_pve_2:      Out,
        T1_fast_pveseg:     Out,
        T1_fast_CSF_mask:   Out,
        T1_fast_GM_mask:    Out,
        T1_fast_WM_mask:    Out,
        T1_fast_brain_bias: Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        # Run fast
        wrappers.fast(T1_brain, out=op.join(T1_fast_dir, 'T1_brain'), b=True)

        # Binarize PVE masks
        wrappers.fslmaths(T1_fast_pve_0).thr(0.5).bin().run(T1_fast_CSF_mask)
        wrappers.fslmaths(T1_fast_pve_1).thr(0.5).bin().run(T1_fast_GM_mask)
        wrappers.fslmaths(T1_fast_pve_2).thr(0.5).bin().run(T1_fast_WM_mask)

        # Apply bias field correction to T1
        wrappers.fslmaths(T1).div(T1_fast_brain_bias).run(T1_unbiased)
        wrappers.fslmaths(T1_brain).div(T1_fast_brain_bias).run(T1_unbiased_brain)
