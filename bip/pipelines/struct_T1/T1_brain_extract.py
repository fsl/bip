#!/usr/bin/env python
#
# T1_brain_extract.py - Sub-pipeline with the brain extraction of T1w.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1704,E1101
#

import os.path as op
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_orig:                 In,
        T1_orig_ud:              In,
        T1_orig_ud_warp:         In,
        T1_brain:                Out,
        T1_brain_mask:           Out,
        T1_brain_to_MNI:         Out,
        T1_to_MNI_linear_mat:    Out,
        T1_orig_ud_to_T1_mat:    Out,
        T1_to_MNI_warp:          Out,
        T1_to_MNI_warp_coef_inv: Out,
        T1_to_T1_orig_ud_mat:    Out,
        logs_dir:                Ref,
        T1:                      Ref,
        tmp_dir:                 Ref,
        T1_orig_to_MNI_warp:     Out,
        T1_orig_ud_to_std_mat:   Out,
        T1_to_MNI_warp_coef:     Out,
        T1_to_MNI_warp_jac:      Out):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        T1_tmp_1                  = op.join(tmp_dir, 'T1_tmp_1.nii.gz')
        T1_tmp_2                  = op.join(tmp_dir, 'T1_tmp_2.nii.gz')
        T1_tmp_1_brain            = op.join(tmp_dir, 'T1_tmp_1_brain.nii.gz')
        T1_tmp_orig_ud_to_std_mat = op.join(tmp_dir, 'T1_tmp_to_std.mat')
        T1_tmp                    = op.join(tmp_dir, 'T1.nii.gz')
        T1_tmp_prefix             = op.join(tmp_dir, 'T1')

        # Calculate where does the brain start in the z dimension and
        # extract roi
        head_top = int(round(float(wrappers.robustfov(T1_orig_ud).stdout[0].
                                   split()[7])))
        wrappers.fslmaths(T1_orig_ud).roi(0, -1, 0, -1, head_top, 170, 0, 1, ).\
            run(T1_tmp_1)

        # Run a (Recursive) brain extraction on the roi
        wrappers.bet(T1_tmp_1, T1_tmp_1_brain, robust=True)

        # Reduce the FOV of T1_orig_ud by calculating a registration
        # from T1_tmp_brain to ssref and apply it to T1_orig_ud
        wrappers.standard_space_roi(T1_tmp_1_brain, T1_tmp_prefix,
                                    maskNONE=True,
                                    ssref=ctx.MNI + '_brain',
                                    altinput=T1_orig_ud, d=True)
        copyfile(src=T1_tmp, dst=T1)
        copyfile(src=T1_tmp_orig_ud_to_std_mat, dst=T1_orig_ud_to_std_mat)

        # Generate the actual affine from the orig_ud volume to the cut version
        # we haveand combine it to have an affine matrix from orig_ud to MNI
        wrappers.flirt(src=T1, ref=T1_orig_ud, omat=T1_to_T1_orig_ud_mat,
                       schedule=op.join(ctx.FSLDIR, 'etc', 'flirtsch',
                                        'xyztrans.sch'))
        wrappers.invxfm(inmat=T1_to_T1_orig_ud_mat, omat=T1_orig_ud_to_T1_mat)
        wrappers.concatxfm(atob=T1_to_T1_orig_ud_mat,
                           btoc=T1_orig_ud_to_std_mat,
                           atoc=T1_to_MNI_linear_mat)

        # Nonlinear registration to MNI using
        # the previously calculated alignment
        wrappers.fnirt(src=T1, ref=ctx.MNI, aff=T1_to_MNI_linear_mat,
                       config=ctx.get_data('fnirt/bb_fnirt.cnf'),
                       refmask=ctx.get_data('MNI/MNI152_T1_1mm_brain_mask_dil_GD7.nii.gz'),
                       logout=op.join(logs_dir, 'bb_T1_to_MNI_fnirt.log'),
                       cout=T1_to_MNI_warp_coef,
                       fout=T1_to_MNI_warp,
                       jout=T1_to_MNI_warp_jac,
                       iout=T1_tmp_2,
                       interp='spline')

        # Combine all transforms (Gradient Distortion Unwarp and T1 to ctx.MNI)
        if ctx.gdc != '' :
            wrappers.convertwarp(ref=ctx.MNI, warp1=T1_orig_ud_warp,
                                 midmat=T1_orig_ud_to_T1_mat,
                                 warp2=T1_to_MNI_warp,
                                 out=T1_orig_to_MNI_warp)
        else:
            wrappers.convertwarp(ref=ctx.MNI, premat=T1_orig_ud_to_T1_mat,
                                 warp1=T1_orig_ud_warp, out=T1_orig_to_MNI_warp)

        # Apply the previously generated transformation
        wrappers.applywarp(src=T1_orig, ref=ctx.MNI, w=T1_orig_to_MNI_warp,
                           out=T1_brain_to_MNI, rel=True, interp='spline')

        # Create brain mask
        wrappers.invwarp(ref=T1, warp=T1_to_MNI_warp_coef,
                         out=T1_to_MNI_warp_coef_inv)
        MNI_var_name = 'MNI/MNI152_T1_1mm_brain_mask.nii.gz'
        wrappers.applywarp(src=ctx.get_data(MNI_var_name), ref=T1,
                           w=T1_to_MNI_warp_coef_inv, out=T1_brain_mask,
                           rel=True, interp='trilinear')
        wrappers.fslmaths(T1).mul(T1_brain_mask).run(T1_brain)
        wrappers.fslmaths(T1_brain_to_MNI).mul(ctx.get_data(MNI_var_name)).\
            run(T1_brain_to_MNI)
