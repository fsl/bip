#!/usr/bin/env python
#
# T1_QC_CNR_eyes.py - Sub-pipeline with the QC (CNR in front of eyes) of T1w.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1704
#

import os.path as op
import logging
import nibabel as nib
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_orig:               In,
        T1_notNorm:            In,
        T1_fast_GM_mask:       In,
        T1_fast_WM_mask:       In,
        T1_orig_ud_warp:       In,
        T1_orig_ud_to_T1_mat:  In,
        T1_to_MNI_linear_mat:  In,
        T1_to_T1_orig_ud_mat:  In,
        T1_eyes_cuboid_mask:   Out,
        MNI_to_T1_orig_ud_mat: Out,
        T1_orig_ud_to_MNI_mat: Out,
        left_eye_flirted:      Out,
        right_eye_flirted:     Out,
        T1_QC_CNR_eyes:        Out,
        T1_orig_ud_warp_inv:   Ref,
        logs_dir:              Ref,
        tmp_dir:               Ref):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        T1_brain_GM_mask_orig = op.join(tmp_dir, 'T1_brain_GM_mask_orig.nii.gz')
        T1_brain_WM_mask_orig = op.join(tmp_dir, 'T1_brain_WM_mask_orig.nii.gz')
        lmask                 = op.join(tmp_dir, 'lmask.nii.gz')
        rmask                 = op.join(tmp_dir, 'rmask.nii.gz')
        T1_tmp_7              = op.join(tmp_dir, 'T1_tmp_7.nii.gz')

        if T1_orig_ud_warp_inv is not None and not op.exists(T1_orig_ud_warp_inv):
            wrappers.invwarp(ref=T1_orig, warp=T1_orig_ud_warp,
                             out=T1_orig_ud_warp_inv)

        wrappers.applywarp(src=T1_fast_GM_mask, ref=T1_orig,
                           w=T1_orig_ud_warp_inv, premat=T1_to_T1_orig_ud_mat,
                           out=T1_brain_GM_mask_orig, rel=True, interp='nn')
        wrappers.applywarp(src=T1_fast_WM_mask, ref=T1_orig,
                           w=T1_orig_ud_warp_inv, premat=T1_to_T1_orig_ud_mat,
                           out=T1_brain_WM_mask_orig, rel=True, interp='nn')

        wrappers.concatxfm(atob=T1_orig_ud_to_T1_mat, btoc=T1_to_MNI_linear_mat,
                           atoc=T1_orig_ud_to_MNI_mat)
        wrappers.invxfm(inmat=T1_orig_ud_to_MNI_mat, omat=MNI_to_T1_orig_ud_mat)
        wrappers.applyxfm(src=ctx.get_data('MNI/MNI152_T1_1mm_left_eye_dil.nii.gz'),
                          ref=T1_orig, mat=MNI_to_T1_orig_ud_mat,
                          out=left_eye_flirted, interp="nearestneighbour")
        wrappers.applyxfm(src=ctx.get_data('MNI/MNI152_T1_1mm_right_eye_dil.nii.gz'),
                          ref=T1_orig, mat=MNI_to_T1_orig_ud_mat,
                          out=right_eye_flirted, interp="nearestneighbour")

        lx, _, lz  = wrappers.fslstats(left_eye_flirted).C.run()
        rx, _, rz = wrappers.fslstats(right_eye_flirted).C.run()

        size = 40
        half_size = size / 2

        T1_orig_img = nib.load(T1_orig)
        dimY = int(round(T1_orig_img.header['dim'][2] - size))

        lDimX = int(round(lx - half_size))
        lDimZ = int(round(lz - half_size))
        rDimX = int(round(rx - half_size))
        rDimZ = int(round(rz - half_size))

        wrappers.fslmaths(T1_orig).mul(0).add(1).roi(lDimX, size, dimY, size,
                                                     lDimZ, size, 0, 1).run(lmask)
        wrappers.fslmaths(T1_orig).mul(0).add(1).roi(rDimX, size, dimY, size,
                                                     rDimZ, size, 0, 1).run(rmask)
        wrappers.fslmaths(rmask).add(lmask).run(T1_eyes_cuboid_mask)
        wrappers.fslmaths(T1_eyes_cuboid_mask).mul(T1_notNorm).run(T1_tmp_7)

        grey   = wrappers.fslstats(T1_notNorm).k(T1_brain_GM_mask_orig).m.run()
        white  = wrappers.fslstats(T1_notNorm).k(T1_brain_WM_mask_orig).m.run()
        threshold = wrappers.fslstats(T1_notNorm).k(T1_brain_GM_mask_orig).P(3).run()

        wrappers.fslmaths(T1_tmp_7).uthr(threshold).bin().run(T1_tmp_7)
        wrappers.fslmaths(T1_tmp_7).dilM(2).ero(4).mul(T1_notNorm).run(T1_tmp_7)

        noise = wrappers.fslstats(T1_tmp_7).S.run()
        brain = int(round((grey + white) / 2))
        # TODO: The roundings are only there to mimic the behaviour of
        #      previous version. We do not have to do it in next version

        result = noise / brain

        with open(T1_QC_CNR_eyes, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
