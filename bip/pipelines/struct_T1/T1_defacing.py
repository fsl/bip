#!/usr/bin/env python
#
# T1_defacing.py - Sub-pipeline with the defacing processing of the T1w.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=R1704
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_orig:                           In,
        T1_brain_mask:                     In,
        T1_to_MNI_linear_mat:              In,
        T1_orig_ud_to_T1_mat:              In,
        T1:                                Out,
        T1_orig_defaced:                   Out,
        T1_defacing_mask:                  Out,
        T1_QC_face_mask_inside_brain_mask: Out,
        logs_dir:                          Ref,
        tmp_dir:                           Ref):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        T1_tmp_4                  = op.join(tmp_dir, 'T1_tmp_4.nii.gz')
        T1_tmp_mat                = op.join(tmp_dir, 'T1_tmp.mat')

        # TODO: Replace this part with proper call to fsl_deface
        # Defacing T1_orig
        wrappers.concatxfm(atob=T1_orig_ud_to_T1_mat, btoc=T1_to_MNI_linear_mat,
                           atoc=T1_tmp_mat)
        wrappers.concatxfm(atob=T1_tmp_mat,
                           btoc=ctx.get_data('MNI/MNI_to_MNI_BigFoV_facemask.mat'),
                           atoc=T1_tmp_mat)
        wrappers.invxfm(inmat=T1_tmp_mat, omat=T1_tmp_mat)
        wrappers.applyxfm(src=ctx.get_data('MNI/MNI152_T1_1mm_BigFoV_facemask'),
                          ref=T1_orig, mat=T1_tmp_mat, out=T1_defacing_mask,
                          interp="trilinear")
        wrappers.fslmaths(T1_defacing_mask).binv().mul(T1_orig).\
            run(T1_orig_defaced)

        # Defacing T1
        wrappers.concatxfm(atob=T1_to_MNI_linear_mat,
                           btoc=ctx.get_data('MNI/MNI_to_MNI_BigFoV_facemask.mat'),
                           atoc=T1_tmp_mat)
        wrappers.invxfm(inmat=T1_tmp_mat, omat=T1_tmp_mat)
        wrappers.applyxfm(src=ctx.get_data('MNI/MNI152_T1_1mm_BigFoV_facemask'),
                          ref=T1, mat=T1_tmp_mat, out=T1_defacing_mask,
                          interp="trilinear")
        wrappers.fslmaths(T1_defacing_mask).binv().mul(T1).run(T1)

        # Generation of QC value: Number of voxels in which
        # the defacing mask goes into the brain mask
        wrappers.fslmaths(T1_brain_mask).thr(0.5).bin().run(T1_tmp_4)
        wrappers.fslmaths(T1_defacing_mask).thr(0.5).bin().add(T1_tmp_4).\
            run(T1_tmp_4)
        vals = wrappers.fslstats(T1_tmp_4).V.run()[0]
        with open(T1_QC_face_mask_inside_brain_mask, 'wt', encoding="utf-8") as f:
            f.write(str(vals))
