#!/usr/bin/env python
#
# swMRI_proc_fnc.py - Functions used by the main processing of swMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
#

import os.path as op
import glob
import nibabel as nib
import numpy as np
import numpy.fft as ft
from fsl import wrappers
from bip.utils.read_json_field import read_json_field

def combine_magnitude_coils(MAG_TE1, MAG_TE2, MAG_TE1_dir, MAG_TE2_dir,
                            num_coils, SOS_TE1, SOS_TE2, SOS_ratio, R2star,
                            T2star):

    MAG_TE1_J = MAG_TE1.replace('.nii.gz', '.json')
    MAG_TE2_J = MAG_TE2.replace('.nii.gz', '.json')

    TE1 = read_json_field(fileName=MAG_TE1_J,
                          fieldName="EchoTime",
                          rounding=3, multFactor=1000)
    TE2 = read_json_field(fileName=MAG_TE2_J,
                          fieldName="EchoTime",
                          rounding=3, multFactor=1000)

    TE_DIFF = TE2 - TE1

    if not num_coils == 0:

        files_TE1 = glob.glob(op.join(MAG_TE1_dir, '*.nii.gz'))
        wrappers.fslmaths(files_TE1[0]).mul(0).run(SOS_TE1)
        for file_TE1 in files_TE1:
            wrappers.fslmaths(file_TE1).sqr().add(SOS_TE1).run(SOS_TE1)
        wrappers.fslmaths(SOS_TE1).sqrt().run(SOS_TE1)

        files_TE2 = glob.glob(op.join(MAG_TE2_dir, '*.nii.gz'))
        wrappers.fslmaths(files_TE2[0]).mul(0).run(SOS_TE2)
        for file_TE2 in files_TE2:
            wrappers.fslmaths(file_TE2).sqr().add(SOS_TE2).run(SOS_TE2)
        wrappers.fslmaths(SOS_TE2).sqrt().run(SOS_TE2)

        wrappers.fslmaths(SOS_TE1).div(SOS_TE2).run(SOS_ratio)

    else:
        wrappers.fslmaths(MAG_TE1).div(MAG_TE2).run(SOS_ratio)

    wrappers.fslmaths(SOS_ratio).log().div(TE_DIFF).run(R2star)
    wrappers.fslmaths(R2star).recip().uthr(80).thr(5).kernel("2D").fmedian().\
        kernel("sphere", "3").dilM().run(T2star)


def gen_filtered_phase(ctx, magImgDir, phaImgDir, magImgFileName, fp_fn,
                       SWI_fn):

    magImgFiles = glob.glob(op.join(magImgDir, '*.nii.gz'))
    phaImgFiles = glob.glob(op.join(phaImgDir, '*.nii.gz'))
    magImgFiles.sort()
    phaImgFiles.sort()

    chaDim = len(magImgFiles)

    # read one data set for getting the header
    magImg = nib.load(magImgFileName)
    magImgNOBIAS = magImg
    [xDim, yDim, zDim] = magImg.header['dim'][1:4]

    # Loading Hanning filter of 96 dim  (actually, 97 dim because of symmetry)
    w = np.loadtxt(ctx.get_data('swMRI/hann_filter.txt'), delimiter=",")
    filterLP = np.zeros((xDim, yDim))
    indX = slice(int(xDim/2+1-96/2)-1, int(xDim/2+1+96/2), 1)
    indY = slice(int(yDim/2+1-96/2)-1, int(yDim/2+1+96/2), 1)
    filterLP[indX, indY] = w

    SOSImg     = np.zeros((xDim, yDim, zDim))
    complexAvg = np.zeros((xDim, yDim, zDim))

    # Loop over channels
    for chaInd in range(chaDim):
        magImgFileName = magImgFiles[chaInd]
        phaImgFileName = phaImgFiles[chaInd]

        magImg_nib = nib.load(magImgFileName)
        phaImg_nib = nib.load(phaImgFileName)

        magImg = magImg_nib.get_fdata()
        phaImg = phaImg_nib.get_fdata()

        phaImg = -np.pi * (phaImg - 2048) / 2048

        complexImg = magImg * np.exp(1j * phaImg)
        phaseHP = np.zeros((xDim, yDim, zDim))

        for zInd in range(zDim):
            complexFFT = ft.fftshift(ft.fft2(ft.
                                             ifftshift(complexImg[:, :, zInd])))
            complexFFT = complexFFT * filterLP
            complexImgLP = ft.fftshift(ft.ifft2(ft.ifftshift(complexFFT)))
            phaseHP[:, :, zInd] = np.angle(complexImg[:, :, zInd] * np.
                                           conj(complexImgLP))

        complexAvg = complexAvg + magImg * np.exp(1j * phaseHP)
        SOSImg = SOSImg + (magImg * magImg)

    SOSImg = np.power(SOSImg, 0.5)
    fp = np.angle(complexAvg)

    maskSWI = np.zeros((xDim, yDim, zDim))
    maskSWI[np.where(fp > 0)] = 1
    maskSWI[np.where(fp <= 0)] = 1 + fp[np.where(fp <= 0)] / np.pi
    maskSWI = maskSWI * maskSWI * maskSWI * maskSWI

    # Using prescan-normalised mag image instead of bias-fielded SOS
    SWI = magImgNOBIAS.get_fdata() * maskSWI

    fp_tmp  = nib.Nifti1Image(fp,  magImgNOBIAS.affine, magImgNOBIAS.header)
    SWI_tmp = nib.Nifti1Image(SWI, magImgNOBIAS.affine, magImgNOBIAS.header)
    nib.save(fp_tmp,  fp_fn)
    nib.save(SWI_tmp, SWI_fn)
