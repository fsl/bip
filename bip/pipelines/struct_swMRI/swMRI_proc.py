#!/usr/bin/env python
#
# swMRI_proc.py - Sub-pipeline with the main processing of swMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R0912
#

import os.path as op
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from gradunwarp.core.gradient_unwarp_apply import gradient_unwarp_apply
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name
from bip.pipelines.struct_swMRI.swMRI_proc_fnc import combine_magnitude_coils
from bip.pipelines.struct_swMRI.swMRI_proc_fnc import gen_filtered_phase

log = logging.getLogger(__name__)

def run(ctx,
        T1:                         In,
        T1_brain_mask:              In,
        SWI_MAG_TE1_orig:           In,
        SWI_MAG_TE2_orig:           In,
        logs_dir:                   Ref,
        SWI_MAG_TE1_C01:            Ref,
        SOS:                        Ref,
        MAG_TE1_dir:                Ref,
        MAG_TE2_dir:                Ref,
        PHA_TE2_dir:                Ref,
        R2star:                     Out,
        T2star:                     Out,
        T2star_to_T1:               Out,
        SOS_TE1:                    Out,
        SOS_TE2:                    Out,
        SOS_ratio:                  Out,
        SWI:                        Out,
        SWI_brain_mask:             Out,
        SWI_GDC:                    Out,
        SWI_MAG_TE1:                Out,
        SWI_TOTAL_MAG_orig_ud_warp: Out,
        SWI_to_T1:                  Out,
        SWI_to_T1_mat:              Out,
        T1_to_SWI_mat:              Out,
        filtered_phase:             Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        num_coils     = ctx.get('SWI_coils', 32)
        # num_echoes    = ctx.get('SWI_ecoes', 2)
        # complex_phase = ctx.get('SWI_complex_phase', False)

        MAG_TE1 = SWI_MAG_TE1_orig
        if SWI_MAG_TE2_orig is not None and op.exists(SWI_MAG_TE2_orig):
            MAG_TE2 = SWI_MAG_TE2_orig
        else:
            MAG_TE2 = ""

        if num_coils > 0:
            if SWI_MAG_TE1_C01 is not None and op.exists(SWI_MAG_TE1_C01):
                combine_magnitude_coils(MAG_TE1, MAG_TE2, MAG_TE1_dir,
                                        MAG_TE2_dir, num_coils, SOS_TE1,
                                        SOS_TE2, SOS_ratio, R2star, T2star)

            # Generate filtered_phase and SWI
            gen_filtered_phase(ctx, MAG_TE2_dir, PHA_TE2_dir, SWI_MAG_TE2_orig,
                               filtered_phase, SWI)

        gdc = ctx.get('gdc', None)
        if gdc not in (None, "", "none"):
            # Calculate and apply the Gradient Distortion Unwarp
            # TODO: Review the "half=True" in next version
            gradient_unwarp_apply(WD=SWI_GDC, infile=SWI_MAG_TE1_orig,
                                  outfile=SWI_MAG_TE1,
                                  owarp=SWI_TOTAL_MAG_orig_ud_warp,
                                  gradcoeff=gdc,
                                  vendor='siemens', nojac=True, half=False)
        else:
            copyfile(src=SWI_MAG_TE1_orig, dst=SWI_MAG_TE1)

        wrappers.flirt(src=SWI_MAG_TE1, ref=T1, omat=SWI_to_T1_mat, dof=6,
                       out=SWI_to_T1, interp="trilinear")

        wrappers.invxfm(inmat=SWI_to_T1_mat, omat=T1_to_SWI_mat)

        # TODO: BINARISE BRAIN MASK?
        wrappers.applyxfm(src=T1_brain_mask, ref=SWI_MAG_TE1,
                          mat=T1_to_SWI_mat, out=SWI_brain_mask,
                          interp="trilinear")

        if ctx.gdc != '':
            wrappers.applywarp(src=T2star, ref=T1, w=SWI_TOTAL_MAG_orig_ud_warp,
                               out=T2star_to_T1, postmat=SWI_to_T1_mat,
                               rel=True, interp='trilinear')
        else:
            wrappers.applyxfm(src=T2star, ref=T1, mat=SWI_to_T1_mat,
                              out=T2star_to_T1, interp="trilinear")

        wrappers.fslmaths(T2star_to_T1).mul(T1_brain_mask).run(T2star_to_T1)

        # TODO: Change name of SWI to venogram?
        for fil in [R2star, T2star, SOS_TE1, SOS_TE2, SOS_ratio, filtered_phase,
                    SWI]:
            if fil is not None and op.exists(fil):
                if ctx.gdc != '':
                    wrappers.applywarp(src=fil, ref=fil, out=fil, rel=True,
                                       w=SWI_TOTAL_MAG_orig_ud_warp,
                                       interp='trilinear')

                wrappers.fslmaths(fil).mul(SWI_brain_mask).run(fil)
