#!/usr/bin/env python
#
# struct_T1.py - Pipeline with the T1w processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1735
#

import logging
from bip.utils.log_utils    import job_name
from bip.pipelines.QC_gen.T1 import QC_T1_align_to_std
from bip.pipelines.QC_gen.T1 import QC_T1_noise_ratio
from bip.pipelines.QC_gen.T1 import QC_T1_SIENAX
from bip.pipelines.QC_gen.T1 import QC_T1_FIRST_vols
from bip.pipelines.QC_gen.T1 import QC_T1_generator

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree):

    subj = ctx.subject

    pipe(QC_T1_align_to_std.run,
         submit=dict(jobtime=200, name=job_name(QC_T1_align_to_std.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(QC_T1_noise_ratio.run,
         submit=dict(jobtime=200, name=job_name(QC_T1_noise_ratio.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(QC_T1_SIENAX.run,
         submit=dict(jobtime=200, name=job_name(QC_T1_SIENAX.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(QC_T1_FIRST_vols.run,
         submit=dict(jobtime=200, name=job_name(QC_T1_FIRST_vols.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(QC_T1_generator.run,
         submit=dict(jobtime=200, name=job_name(QC_T1_generator.run, subj)),
         kwargs={'ctx' : ctx})
