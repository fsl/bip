#!/usr/bin/env python
#
# QC_T1_align_to_std.py - Copying QC file with alignment to std metrics.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

from shutil import copyfile
from fsl_pipe import In, Out

def run(ctx,
        IDP_T1_align_to_std: In,
        QC_T1_align_to_std:  Out):

    copyfile(src=IDP_T1_align_to_std, dst=QC_T1_align_to_std)
