#!/usr/bin/env python
#
# QC_diff_autoPtx.py - Generating QC file with autoPtx metrics
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
#

import os.path as op
import json
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        QC_T1_align_to_std: In(optional=True),
        QC_T1_noise_ratio:  In(optional=True),
        QC_T1_SIENAX:       In(optional=True),
        QC_T1_FIRST_vols:   In(optional=True),
        logs_dir:           Ref,
        QC_T1:              Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        result = ""
        QC_json_file = ctx.get_data("QC/QC_T1.json")

        with open(QC_json_file, "r", encoding="utf-8") as f:
            QC_dict = json.load(f)

        for QC_file in [QC_T1_align_to_std, QC_T1_noise_ratio, QC_T1_SIENAX,
                        QC_T1_FIRST_vols]:

            plain_name = op.basename(QC_file).replace(".txt", "")
            num_QC = len(QC_dict[plain_name])
            result_nans = ("NaN " * num_QC).strip()

            if QC_file is not None and op.exists(QC_file):
                with open(QC_file, "r", encoding="utf-8") as f:
                    QC_l = f.read().strip().split()
                if len(QC_l) != num_QC:
                    result += " " + result_nans
                else:
                    result += " " + " ".join(QC_l)

            else:
                result += " " + result_nans

        result = result.replace("  ", " ").strip()

        print(result)

        with open(QC_T1, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
