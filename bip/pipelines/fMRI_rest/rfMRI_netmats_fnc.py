#!/usr/bin/env python
#
# rfMRI_netmats_fnc.py - Functions for the netmats processing of the rest fMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=E1130
#

import numpy as np
import nibabel as nib

def nets_load(timeseries_name, tr, varnorm, numvols=0):

    timeseries = np.loadtxt(timeseries_name)
    gn = timeseries.shape[0]
    gd = timeseries.shape[1]

    ts = {'NtimepointsPerSubject': numvols}

    TS = timeseries

    if ts['NtimepointsPerSubject'] == 0:
        ts["NtimepointsPerSubject"] = gn
        TS = np.full(timeseries.shape, np.nan)

    TS = TS - np.nanmean(TS)

    if varnorm == 1:
        TS = TS / np.nanstd(TS)

    ts["ts"]          = TS
    ts["tr"]          = tr
    ts["Nsubjects"]   = 1
    ts["Nnodes"]      = gd
    ts["NnodesOrig"]  = ts["Nnodes"]
    ts["Ntimepoints"] = gn
    ts["DD"]          = [*range(ts["Nnodes"])]
    ts["UNK"]         = []

    return ts

def nets_tsclean(ts, aggressive):
    ts["NnodesOrig"] = ts["Nnodes"]

    nongood = [x for x in range(ts["Nnodes"]) if x not in ts["DD"]]
    bad = [x for x in nongood if x not in ts["UNK"]]  # Only bad components

    # Good components
    goodTS = ts["ts"][:, ts["DD"]]

    # Bad components
    badTS = ts["ts"][:, bad]

    if aggressive == 1:
        ts["ts"] = goodTS - np.dot(badTS, np.dot(np.linalg.pinv(badTS), goodTS))
    else:
        ts["ts"] = goodTS

    ts["Nnodes"] = ts["ts"].shape[1]

    return ts

# netmats2 = nets_netmats(ts, -r2zPARTIAL, 'ridgep', 0.5)
def nets_netmats(ts, do_rtoz, method, arg_method=None):

    N       = ts["Nnodes"]
    grot    = ts["ts"]

    # We are keeping just the amplitudes
    if method.lower() in ['corr', 'correlation']:
        grot = np.corrcoef(grot, rowvar=False)
        np.fill_diagonal(grot, 0)

    elif method.lower() in ['ridgep']:
        grot = np.cov(grot, rowvar=False)
        grot = grot / np.sqrt(np.nanmean(np.diag(grot) * np.diag(grot)))
        if not arg_method:
            rho = 0.1
        else:
            rho = arg_method
            if rho < 0:
                rho = -rho

        grot = -(np.linalg.inv(grot+rho*np.eye(N)))
        val0 = np.sqrt(np.abs(np.diag(grot)))
        val2 = np.tile(val0, (N, 1))
        val1 = np.transpose(val2)
        grot = (grot / val1) / val2

        np.fill_diagonal(grot, 0)

    netmats = grot.reshape(1, N*N, order='F').copy()
    netmats = 0.5*np.log((1+netmats)/(1-netmats)) * (-do_rtoz)

    return netmats

def ICA_dr(func, timeseries, D, full_corr, partial_corr, node_amplitudes):
    im_func = nib.load(func)
    tr      = im_func.header['pixdim'][4]
    numvols = im_func.header['dim'][4]

    ts = nets_load(timeseries, tr, 0, numvols)

    if D == "25":
        good_components = [3, 22, 23, 24]
        ts["DD"] = [x for x in ts["DD"] if x not in good_components]
        r2zFULL    = 10.6484
        r2zPARTIAL = 10.6707
    else:
        good_components =  [0, 43, 46, 50, 53, 54, 55, 58, 60, 61]
        good_components += [*range(64, 92)] + [*range(93, 100)]
        ts["DD"] = [x for x in ts["DD"] if x not in good_components]
        r2zFULL    = 19.7177
        r2zPARTIAL = 18.8310

    ts = nets_tsclean(ts, 1)

    netmats1 = nets_netmats(ts, -r2zFULL,    'corr')
    netmats2 = nets_netmats(ts, -r2zPARTIAL, 'ridgep', 0.5)

    tmp = np.reshape(netmats1, (ts["Nnodes"], ts["Nnodes"]), order="F")
    data = tmp[np.where(np.triu(np.ones([ts["Nnodes"], ts["Nnodes"]]),
                                k=1) == 1)]
    with open(full_corr, 'wt', encoding="utf-8") as f:
        np.savetxt(f, data, fmt="%14.8f", newline=" ")

    tmp = np.reshape(netmats2, (ts["Nnodes"], ts["Nnodes"]), order="F")
    data = tmp[np.where(np.triu(np.ones([ts["Nnodes"], ts["Nnodes"]]),
                                k=1) == 1)]
    with open(partial_corr, 'wt', encoding="utf-8") as f:
        np.savetxt(f, data, fmt="%14.8f", newline=" ")

    data = np.nanstd(ts["ts"], axis=0)
    with open(node_amplitudes, 'wt', encoding="utf-8") as f:
        np.savetxt(f, data, fmt="%14.8f", newline=" ")
