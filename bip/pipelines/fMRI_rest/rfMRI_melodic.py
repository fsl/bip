#!/usr/bin/env python
#
# rfMRI_melodic.py - Sub-pipeline with FSL's MELODIC tool
#                    applied to the rest fMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import shutil
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        rfMRI_fsf:                        In,
        logs_dir:                         Ref,
        rfMRI_ica:                        Out,
        rfMRI_example_func2highres:       Out,
        rfMRI_example_func2standard_warp: Out,
        rfMRI_example_func:               Out,
        rfMRI_standard:                   Out,
        filtered_func_data:               Out,
        melodic_mix:                      Out,
        rfMRI_mc_parameters:              Out,
        rfMRI_mc_rel_mean:                Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        if rfMRI_ica is not None and op.exists(rfMRI_ica):
            shutil.rmtree(rfMRI_ica)

        wrappers.feat(fsf=rfMRI_fsf)
