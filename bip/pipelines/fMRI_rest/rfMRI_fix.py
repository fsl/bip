#!/usr/bin/env python
#
# rfMRI_fix.py - Sub-pipeline with the FIX processing of the rest fMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
#

import os
import os.path as op
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from fsl import wrappers

from bip.utils.log_utils import redirect_logging, run_command, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_fast_pveseg:                   In,
        rfMRI_ica:                        In,
        rfMRI_example_func2standard_warp: In,
        rfMRI_example_func:               In,
        rfMRI_standard:                   In,
        logs_dir:                         Ref,
        rfMRI_reg_standard_dir:           Ref,
        rfMRI_highres_pveseg:             Out,
        rfMRI_standard2example_func_warp: Out,
        filtered_func_data_clean:         Out,
        filtered_func_data_clean_std:     Out,
        filtered_func_data_clean_stdmask: Out,
        fix_logs_dir:                     Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        # Generate files needed to extract features
        if rfMRI_highres_pveseg is not None and \
                not op.exists(rfMRI_highres_pveseg):
            copyfile(T1_fast_pveseg, rfMRI_highres_pveseg)
        if not op.exists(rfMRI_standard2example_func_warp):
            wrappers.invwarp(ref=rfMRI_example_func,
                             warp=rfMRI_example_func2standard_warp,
                             out=rfMRI_standard2example_func_warp)

        # TODO: Include the threshold and hp filter in the configuration file
        threshold = 20
        hp_filter = 100

        fix_cmd = ['fix',
                   rfMRI_ica,
                   'UKBiobank',
                   str(threshold),
                   '-h',
                   str(hp_filter),
                   '-m',
                   '-lf',
                   fix_logs_dir]

        try:
            log.info(run_command(log, " ".join(fix_cmd)))

        except:
            log.error("Problem running FIX")

        # Create this directory in case it does not exist. This may
        # happen due to the overwrite function issue in Melodic
        if rfMRI_reg_standard_dir is not None and \
                not op.exists(rfMRI_reg_standard_dir):
            os.mkdir(rfMRI_reg_standard_dir)

        # Generate the data in standard space
        wrappers.applywarp(ref=rfMRI_standard, src=filtered_func_data_clean,
                           out=filtered_func_data_clean_std,
                           warp=rfMRI_example_func2standard_warp,
                           interp="spline")
        mask = ctx.get_data('MNI/MNI152_T1_2mm_brain_mask_bin.nii.gz')
        wrappers.fslmaths(filtered_func_data_clean_std).mas(mask).\
            run(filtered_func_data_clean_std)
        wrappers.fslmaths(filtered_func_data_clean_std).Tstd().bin().\
            run(filtered_func_data_clean_stdmask)
