#!/usr/bin/env python
#
# fMRI_rest.py - Pipeline with the resting state fMRI processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1735
#

import logging
from bip.utils.log_utils     import job_name
from bip.pipelines.fMRI_rest import rfMRI_prepare, rfMRI_melodic
from bip.pipelines.fMRI_rest import rfMRI_fix, rfMRI_netmats

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree):

    subj = ctx.subject

    pipe(rfMRI_prepare.run,
         submit=dict(jobtime=200, name=job_name(rfMRI_prepare.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(rfMRI_melodic.run,
         submit=dict(jobtime=200, name=job_name(rfMRI_melodic.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(rfMRI_fix.run,
         submit=dict(jobtime=200, name=job_name(rfMRI_fix.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(rfMRI_netmats.run,
         submit=dict(jobtime=200, name=job_name(rfMRI_netmats.run, subj)),
         kwargs={'ctx' : ctx})
