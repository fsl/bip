#!/usr/bin/env python
#
# rfMRI_netmats.py - Sub-pipeline with the netmats processing of the rest fMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
#

import os
import os.path as op
import logging
from fsl_pipe import In, Out, Ref, Var
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name
from bip.pipelines.fMRI_rest.rfMRI_netmats_fnc import ICA_dr

log = logging.getLogger(__name__)

def run(ctx,
        netmats_dim:                  Var,
        filtered_func_data:           In,
        filtered_func_data_clean_std: In,
        logs_dir:                     Ref,
        netmats_dir:                  Out,
        dr_stage1:                    Out,
        full_corr:                    Out,
        partial_corr:                 Out,
        node_amplitudes:              Out):

    D = netmats_dim.value

    with redirect_logging(job_name(run, D), outdir=logs_dir):

        MNI_2mm_mask = ctx.get_standard("MNI152_T1_2mm_brain_mask.nii.gz")
        group_IC = ctx.get_data("netmats/melodic_IC_" + D + ".nii.gz")

        # Create this directory in case it does not exist. This may
        # happen due to the overwrite function issue in Melodic
        if netmats_dir is not None and not op.exists(netmats_dir):
            os.mkdir(netmats_dir)

        wrappers.fsl_glm(input=filtered_func_data_clean_std,
                         design=group_IC, out=dr_stage1, demean=True,
                         mask=MNI_2mm_mask)

        # Implement in python
        ICA_dr(filtered_func_data, dr_stage1, D, full_corr, partial_corr,
               node_amplitudes)
