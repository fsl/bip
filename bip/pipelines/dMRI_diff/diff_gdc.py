#!/usr/bin/env python
#
# diff_gdc.py - Sub-pipeline with the Gradient Distortion Correction of the dMRI
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from gradunwarp.core.gradient_unwarp_apply import gradient_unwarp_apply
from bip.utils.log_utils import redirect_logging, job_name


log = logging.getLogger(__name__)

def run(ctx,
        eddy_data:                 In,
        logs_dir:                  Ref,
        eddy_data_GDC:             Ref,
        eddy_data_ud:              Out,
        eddy_data_ud_warp:         Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        gdc = ctx.get('gdc', None)

        if gdc not in (None, "", "none"):
            # Calculate and apply the Gradient Distortion Unwarp
            # TODO: Review the "half=True" in next version
            gradient_unwarp_apply(WD=eddy_data_GDC,
                                  infile=eddy_data,
                                  outfile=eddy_data_ud,
                                  owarp=eddy_data_ud_warp,
                                  gradcoeff=gdc,
                                  vendor='siemens', nojac=True, half=True)
        else:
            copyfile(src=eddy_data, dst=eddy_data_ud)
