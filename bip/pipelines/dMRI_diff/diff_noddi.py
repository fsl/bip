#!/usr/bin/env python
#
# diff_noddi.py - Sub-pipeline with AMICO processing of the dMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=R1704
#

import os
import os.path as op
import logging
from shutil import move, rmtree
import zipfile
import gzip
import shutil
import amico
from fsl_pipe import In, Out, Ref
from bip.utils import lockdir
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        eddy_data_ud:              In,
        eddy_nodif_brain_mask_ud:  In,
        eddy_bvals:                In,
        eddy_bvecs:                In,
        logs_dir:                  Ref,
        tmp_dir:                   Ref,
        NODDI_dir:                 Ref,
        AMICO_dir:                 Ref,
        NODDI_scheme:              Ref,
        ICVF:                      Out,
        ISOVF:                     Out,
        OD:                        Out,
        NODDI_dir_file:            Out):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        amtmp      = op.join(tmp_dir, 'amtmp.nii')
        amtmp_mask = op.join(tmp_dir, 'amtmp_mask.nii')

        # In case the kernels were already calculated for this study:
        # TODO: Verify that this is regular UKB data.
        # Otherwise, this should not be done
        if not op.exists(op.join(os.getcwd(), 'kernels')):
            with lockdir(dirname=os.getcwd(), delay=10):
                # 2 checks will be needed
                if not op.exists(op.join(os.getcwd(), 'kernels')):
                    if op.exists(ctx.get_data('dMRI/kernels.zip')):
                        with zipfile.ZipFile(ctx.get_data('dMRI/kernels.zip'),
                                             'r') as zip_ref:
                            zip_ref.extractall(os.getcwd())

        with gzip.open(eddy_data_ud, 'rb') as f_in:
            with open(amtmp, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

        with gzip.open(eddy_nodif_brain_mask_ud, 'rb') as f_in:
            with open(amtmp_mask , 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

        amico.setup()
        amico.util.fsl2scheme(bvalsFilename=eddy_bvals,
                              bvecsFilename=eddy_bvecs,
                              schemeFilename=NODDI_scheme,
                              bStep=50)

        ae = amico.Evaluation("./", "./", output_path=NODDI_dir)
        ae.load_data(amtmp, NODDI_scheme, mask_filename=amtmp_mask,
                     b0_min_signal=1e-4)
        ae.set_model("NODDI")
        ae.generate_kernels(ndirs=500, regenerate=True)
        ae.load_kernels()
        ae.fit()
        ae.save_results()

        files = [ICVF, ISOVF, OD, NODDI_dir_file]

        # Needed due to a modification in the name of AMICO outputs
        if op.exists(NODDI_dir + os.sep + "fit_NDI.nii.gz"):
            move(NODDI_dir + os.sep + "fit_NDI.nii.gz", ICVF)
            move(NODDI_dir + os.sep + "fit_ODI.nii.gz", OD)
            move(NODDI_dir + os.sep + "fit_FWF.nii.gz", ISOVF)
            move(NODDI_dir + os.sep + "fit_dir.nii.gz", NODDI_dir_file)

        else:
            # Move files to proper place
            for file_n in files:
                old_name = file_n.replace(os.sep + "NODDI_", os.sep + "FIT_")
                move(old_name, file_n)

        # Delete created folders
        if AMICO_dir is not None and op.exists(AMICO_dir):
            rmtree(AMICO_dir)
