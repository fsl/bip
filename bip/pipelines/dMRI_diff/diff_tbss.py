#!/usr/bin/env python
#
# diff_tbss.py - Sub-pipeline with FSL's TBSS processing of the dMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,C0206,R1704
#

import os
import os.path as op
import logging
from shutil import copyfile
import nibabel as nib
import numpy as np
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        FA:                          In,
        ISOVF:                       In(optional=True),
        OD:                          In(optional=True),
        ICVF:                        In(optional=True),
        logs_dir:                    Ref,
        tmp_dir:                     Ref,
        TBSS_MNI_to_dti_FA_warp_msf: Ref,
        dtifit_prefix:               Ref,
        NODDI_prefix:                Ref,
        TBSS_prefix:                 Ref,
        JHUrois_prefix:              Ref,
        TBSS_FA_to_MNI_int:          Ref,
        TBSS_FA_dir_FA:              Out,
        TBSS_FA_dir_FA_mask:         Out,
        TBSS_FA:                     Out,
        TBSS_MNI:                    Out,
        TBSS_FA_to_MNI_affine_mat:   Out,
        TBSS_FA_to_MNI_warp:         Out,
        TBSS_FA_to_MNI_int_txt:      Out,
        TBSS_log_1:                  Out,
        TBSS_log_2:                  Out,
        TBSS_log_3:                  Out,
        TBSS_dti_FA_to_MNI:          Out,
        TBSS_MNI_to_dti_FA_warp:     Out,
        TBSS_all_FA:                 Out,
        TBSS_mean_FA_mask:           Out,
        TBSS_mean_FA:                Out,
        TBSS_mean_FA_skeleton:       Out,
        TBSS_mean_FA_skeleton_mask:  Out,
        TBSS_all_FA_skeletonised:    Out,
        JHUrois_FA:                  Out,
        JHUrois_MD:                  Out,
        JHUrois_MO:                  Out,
        JHUrois_L1:                  Out,
        JHUrois_L2:                  Out,
        JHUrois_L3:                  Out,
        JHUrois_ICVF:                Out,
        JHUrois_OD:                  Out,
        JHUrois_ISOVF:               Out,):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        TBSS_FA_tmp            = op.join(tmp_dir, 'TBSS_FA_tmp.nii.gz')
        TBSS_FA_to_MNI_warp_s1 = op.join(tmp_dir, 'FA_to_MNI_warp_s1.nii.gz')
        TBSS_FA_to_MNI_warp_s2 = op.join(tmp_dir, 'FA_to_MNI_warp_s2.nii.gz')

        # Creates links
        if TBSS_FA is not None and not op.exists(TBSS_FA):
            rel_path = op.relpath(FA, op.dirname(TBSS_FA))
            os.symlink(src=rel_path, dst=TBSS_FA)
        if TBSS_MNI is not None and not op.exists(TBSS_MNI):
            os.symlink(src=ctx.get_standard('FMRIB58_FA_1mm.nii.gz'),
                       dst=TBSS_MNI)
        FA_img     = nib.load(TBSS_FA)
        x, y, z = FA_img.header['dim'][1:4]

        # erode a little and zero end slices
        wrappers.fslmaths(FA).min(1).ero().roi(1, x, 1, y, 1, z, 0, 1).\
            run(TBSS_FA_dir_FA)

        # create mask (for use in FLIRT & FNIRT)
        wrappers.fslmaths(TBSS_FA_dir_FA).bin().run(TBSS_FA_dir_FA_mask)
        wrappers.fslmaths(TBSS_FA_dir_FA_mask).dilD(2).sub(1).abs().\
            add(TBSS_FA_dir_FA_mask).run(TBSS_FA_dir_FA_mask, odt="char")

        if TBSS_MNI_to_dti_FA_warp_msf is not None and \
                not op.exists(TBSS_MNI_to_dti_FA_warp_msf):
            # New Optimal Registration
            wrappers.flirt(ref=TBSS_MNI,
                           src=TBSS_FA_dir_FA,
                           inweight=TBSS_FA_dir_FA_mask,
                           omat=TBSS_FA_to_MNI_affine_mat)

            # perform FNIRT cascade of registrations
            wc1 = ctx.get_data('dMRI/dMRI_reg_optimal_parameters/oxford_s1.cnf')
            wc2 = ctx.get_data('dMRI/dMRI_reg_optimal_parameters/oxford_s2.cnf')
            wc3 = ctx.get_data('dMRI/dMRI_reg_optimal_parameters/oxford_s3.cnf')

            wrappers.fnirt(ref=TBSS_MNI, src=TBSS_FA_dir_FA,
                           cout=TBSS_FA_to_MNI_warp_s1, config=wc1,
                           aff=TBSS_FA_to_MNI_affine_mat,
                           intout=TBSS_FA_to_MNI_int, logout=TBSS_log_1)
            wrappers.fnirt(ref=TBSS_MNI, src=TBSS_FA_dir_FA,
                           cout=TBSS_FA_to_MNI_warp_s2, config=wc2,
                           inwarp=TBSS_FA_to_MNI_warp_s1,
                           intin=TBSS_FA_to_MNI_int_txt, logout=TBSS_log_2)
            wrappers.fnirt(ref=TBSS_MNI, src=TBSS_FA_dir_FA,
                           cout=TBSS_FA_to_MNI_warp, config=wc3,
                           iout=TBSS_dti_FA_to_MNI,
                           inwarp=TBSS_FA_to_MNI_warp_s2,
                           intin=TBSS_FA_to_MNI_int_txt, logout=TBSS_log_3)
            wrappers.invwarp(ref=TBSS_FA_dir_FA, warp=TBSS_FA_to_MNI_warp,
                             out=TBSS_MNI_to_dti_FA_warp)

            # now estimate the mean deformation
            wrappers.fslmaths(TBSS_FA_to_MNI_warp).sqr().Tmean().\
                run(TBSS_FA_tmp)
            mean    = wrappers.fslstats(TBSS_FA_tmp).M.run()
            median  = wrappers.fslstats(TBSS_FA_tmp).P(50).run()

            with open(TBSS_MNI_to_dti_FA_warp_msf, 'wt', encoding="utf-8") as f:
                f.write(f'{mean} {median}')

        wrappers.applywarp(src=TBSS_FA_dir_FA, out=TBSS_dti_FA_to_MNI,
                           ref=TBSS_MNI, warp=TBSS_FA_to_MNI_warp,
                           rel=True, interp='trilinear')

        copyfile(src=TBSS_dti_FA_to_MNI, dst=TBSS_all_FA)

        # Create mean FA
        skel = ctx.get_standard('FMRIB58_FA-skeleton_1mm.nii.gz')
        wrappers.fslmaths(TBSS_all_FA).bin().mul(TBSS_MNI).bin().\
            run(TBSS_mean_FA_mask)
        wrappers.fslmaths(TBSS_all_FA).mas(TBSS_mean_FA_mask).run(TBSS_all_FA)
        wrappers.fslmaths(TBSS_MNI).mas(TBSS_mean_FA_mask).run(TBSS_mean_FA)
        wrappers.fslmaths(skel).mas(TBSS_mean_FA_mask).\
            run(TBSS_mean_FA_skeleton)

        # Create Skeleton mask
        thresh = ctx.get('dMRI_TBSS_skeleton_threshold', 2000)
        wrappers.fslmaths(TBSS_mean_FA_skeleton).thr(thresh).bin().\
            run(TBSS_mean_FA_skeleton_mask)
        wrappers.fslmaths(TBSS_all_FA).mas(TBSS_mean_FA_skeleton_mask).\
            run(TBSS_all_FA_skeletonised)

        atlas = ctx.get_data('dMRI/TBSS/JHU-ICBM-labels-1mm.nii.gz')
        mean = wrappers.fslstats(TBSS_all_FA_skeletonised, K=atlas).M.run()

        # TODO: Funnily enough, this atlas has changed over time, so for v2
        #       of the pipeline, we will need to change the IDPs
        with open(JHUrois_FA, 'wt', encoding="utf-8") as f:
            f.write(f'{mean}')

        # Applying to the outputs of both dtifit and noddi
        dict_suffix = {dtifit_prefix: ['MO', 'MD', 'L1', 'L2', 'L3'],
                       NODDI_prefix: ['ICVF', 'OD', 'ISOVF']}

        for prefix in dict_suffix:

            for suffix in dict_suffix[prefix]:

                d_input = prefix + suffix + ".nii.gz"

                if op.exists(d_input):
                    d_output      = TBSS_prefix + suffix + ".nii.gz"
                    d_output_skel = TBSS_prefix + suffix + "_skeletonised.nii.gz"
                    d_output_txt  = JHUrois_prefix + suffix + '.txt'
                    wrappers.applywarp(src=d_input, out=d_output, ref=TBSS_MNI,
                                       warp=TBSS_FA_to_MNI_warp, rel=True,
                                       interp='trilinear')
                    wrappers.fslmaths(d_output).\
                        mas(TBSS_mean_FA_skeleton_mask).run(d_output_skel)
                    mean = wrappers.fslstats(d_output_skel, K=atlas).M.run()

                    np.savetxt(d_output_txt, mean, fmt="%0.8f")
                else:
                    num_tracts = ctx.get('dMRI_TBSS_num_tracts', 48)
                    str_NaNs = ["NaN" for _ in range(num_tracts)]
                    print("[" + " ".join(str_NaNs) + "]\n")
