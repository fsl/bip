#!/usr/bin/env python
#
# dMRI_diff.py - Pipeline with the dfMRI processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1735
#

import logging
from bip.utils.log_utils     import job_name
from bip.pipelines.dMRI_diff import diff_eddy, diff_dtifit, diff_noddi, diff_gdc
from bip.pipelines.dMRI_diff import diff_tbss, diff_bedpostx, diff_autoptx

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree):

    subj = ctx.subject

    cuda_eddy_dict = dict(queue=ctx.get("queue_cuda"),
                          coprocessor=ctx.get("sge_coprocessor"),
                          coprocessor_class=ctx.get("sge_coprocessor_class"),
                          coprocessor_toolkit=ctx.get("sge_cuda_toolkit_eddy"),
                          name=job_name(diff_eddy.run, subj))

    cuda_bedpostx_dict = dict(queue=ctx.get("queue_cuda"),
                              coprocessor=ctx.get("sge_coprocessor"),
                              coprocessor_class=ctx.get("sge_coprocessor_class"),
                              coprocessor_toolkit=ctx.get("sge_cuda_toolkit_bedpostx"),
                              name=job_name(diff_bedpostx.run, subj))
    pipe(diff_eddy.run,
         submit=cuda_eddy_dict,
         kwargs={'ctx' : ctx})
    pipe(diff_gdc.run,
         submit=dict(jobtime=200, name=job_name(diff_gdc.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(diff_dtifit.run,
         submit=dict(jobtime=200, name=job_name(diff_dtifit.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(diff_noddi.run,
         submit=dict(jobtime=200, name=job_name(diff_noddi.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(diff_tbss.run,
         submit=dict(jobtime=200, name=job_name(diff_tbss.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(diff_bedpostx.run,
         submit=cuda_bedpostx_dict,
         kwargs={'ctx' : ctx})
    pipe(diff_autoptx.run,
         submit=dict(jobtime=200, name=job_name(diff_autoptx.run, subj)),
         kwargs={'ctx' : ctx})
