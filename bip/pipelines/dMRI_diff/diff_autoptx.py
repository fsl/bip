#!/usr/bin/env python
#
# diff_autoptx.py - Sub-pipeline with FSL's autoPtx processing of the dMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os
import os.path as op
import random
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref, Var
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name, query_cuda_version

log = logging.getLogger(__name__)

def run(ctx,
        autoptx_tract:             Var,
        bedpostx_eye:              In,
        TBSS_FA_to_MNI_warp:       In,
        TBSS_MNI_to_dti_FA_warp:   In,
        bedpostx_nodif_brain_mask: In,
        bedpostx_merged:           In,
        logs_dir:                  Ref,
        aptx_stop:                 Ref,
        bedpostx_merged_prefix:    Ref,
        aptx_tracts_sub_dir:       Ref,
        aptx_tractsinv_sub_dir:    Ref,
        density_inv:               Ref,
        waytotal_inv:              Ref,
        fwdensity:                 Ref,
        aptx_seed:                 Out,
        aptx_target:               Out,
        aptx_exclude:              Out,
        density:                   Out,
        waytotal:                  Out,
        tractsNorm:                Out,
        aptx_tract_tmp:            Out):

    with redirect_logging(job_name(run, autoptx_tract.value),
                          outdir=logs_dir):

        t_name  = autoptx_tract.value

        seed_mult = ctx.get('dMRI_multiplier_autoPtx_seeds', 300)
        t_seeds = int(ctx.tract_struct[t_name]["num_seeds"] * seed_mult)

        orig_tract_dir = ctx.get_data('dMRI/autoptx/protocols/' + t_name) + \
                         op.sep
        FMRIB58_FA_1mm = ctx.get_standard('FMRIB58_FA_1mm.nii.gz')

        # Does the protocol defines a second
        # run with inverted seed / target masks?
        if orig_tract_dir is not None and op.exists(orig_tract_dir + 'invert'):
            symtrack = True
            if waytotal is not None and op.exists(waytotal):
                os.remove(waytotal)
        else:
            symtrack = False

        # Copy the seed, target and exclude files for the tract
        copyfile(orig_tract_dir + 'seed.nii.gz',    aptx_seed)
        copyfile(orig_tract_dir + 'target.nii.gz',  aptx_target)
        copyfile(orig_tract_dir + 'exclude.nii.gz', aptx_exclude)

        if waytotal is not None and op.exists(waytotal):
            os.remove(waytotal)

        # Building arguments dictionary for probtrackx
        kwargs = {
            'samples' :   bedpostx_merged_prefix,
            'mask' :      bedpostx_nodif_brain_mask,
            'out' :       'density',
            'nsamples' :  t_seeds,
            'avoid' :     aptx_exclude,
            'opd' :       True,
            'loopcheck' : True,
            'forcedir' :  True,
            'sampvox' :   True,
            'xfm' :       TBSS_MNI_to_dti_FA_warp,
            'invxfm' :    TBSS_FA_to_MNI_warp,
            'rseed' :     int(random.randrange(32767))
        }

        # Is there a stop criterion defined
        # in the protocol for this struct?
        if orig_tract_dir is not None and op.exists(orig_tract_dir +
                                                    'stop.nii.gz'):
            kwargs['stop'] = aptx_stop
            copyfile(orig_tract_dir + 'stop.nii.gz', aptx_stop)

        # Process structure
        wrappers.probtrackx(seed=aptx_seed,
                            waypoints=aptx_target,
                            dir=aptx_tracts_sub_dir,
                            **kwargs)

        with open(waytotal, 'r', encoding="utf-8") as f:
            way = int(f.read())

        # Calculate inverse tractography for this tract and
        # merge runs for forward and inverted tractographies
        if symtrack:
            wrappers.probtrackx(seed=aptx_target,
                                waypoints=aptx_seed,
                                dir=aptx_tractsinv_sub_dir,
                                **kwargs)

            copyfile(density, fwdensity)
            wrappers.fslmaths(density).add(density_inv).run(density)

            with open(waytotal, 'r', encoding="utf-8") as f:
                way1 = int(f.read())
            with open(waytotal_inv, 'r', encoding="utf-8") as f:
                way2 = int(f.read())
            if waytotal is not None and op.exists(waytotal):
                os.remove(waytotal)
            with open(waytotal, 'wt', encoding="utf-8") as f:
                way = way1 + way2
                f.write(f'{way}\n')

        wrappers.fslmaths(density).div(way).range().run(tractsNorm)

        # Hack to ameliorate partial misalignment of mcp
        if t_name == "mcp":
            wrappers.fslmaths(FMRIB58_FA_1mm).thr(1000).bin().ero(2).\
                mul(tractsNorm).run(tractsNorm)

        # This will be needed for IDP generation
        val = wrappers.fslstats(tractsNorm).P(99).run()
        wrappers.fslmaths(tractsNorm).min(val).mul(1e6).div(val).thr(1e5).\
            run(aptx_tract_tmp)
