#!/usr/bin/env python
#
# diff_dtifit.py - Sub-pipeline with FSL's dtifit processing of the dMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import logging
import numpy as np
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        eddy_data:                 In,
        eddy_bvals:                In,
        eddy_bvecs:                In,
        eddy_nodif_brain_mask_ud:  In,
        eddy_data_ud:              In,
        logs_dir:                  Ref,
        dtifit_output_prefix:      Ref,
        eddy_data_ud_1_shell:      Out,
        eddy_data_ud_1_shell_bval: Out,
        eddy_data_ud_1_shell_bvec: Out,
        FA:                        Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        # Correct input for dtifit using one shell
        list_vals = []
        b_vals = np.loadtxt(eddy_bvals)
        b_vecs = np.loadtxt(eddy_bvecs)
        lim = ctx.get('dMRI_b0_threshold', 100)

        b_value_shell_to_keep = ctx.get('dMRI_b_value_shell', 1000)

        for count, b_val in enumerate(b_vals):
            # These are the b0 images: They are automatically included
            if b_val < lim:
                list_vals.append(count)
            else:
                if abs(b_val - b_value_shell_to_keep) < lim:
                    list_vals.append(count)

        new_b_vals = b_vals[list_vals]
        new_b_vecs = b_vecs[:, list_vals]

        np.savetxt(eddy_data_ud_1_shell_bval, new_b_vals, newline=" ",
                   fmt='%.1f')
        np.savetxt(eddy_data_ud_1_shell_bvec, new_b_vecs, fmt='%.6f')

        wrappers.fslselectvols(src=eddy_data_ud, out=eddy_data_ud_1_shell,
                               vols=list_vals)

        wrappers.dtifit(data  = eddy_data_ud_1_shell,
                        bvecs = eddy_data_ud_1_shell_bvec,
                        bvals = eddy_data_ud_1_shell_bval,
                        mask  = eddy_nodif_brain_mask_ud,
                        out   = dtifit_output_prefix,
                        save_tensor = True)
