#!/usr/bin/env python
#
# diff_bedpostx.py - Sub-pipeline with FSL's bedpostx processing of the dMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os
import os.path as op
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name, query_cuda_version

log = logging.getLogger(__name__)

def run(ctx,
        AP_bval:                   In,
        AP_bvec:                   In,
        eddy_data_ud:              In,
        eddy_nodif:                In,
        eddy_nodif_brain_mask:     In,
        eddy_dir:                  In,
        logs_dir:                  Ref,
        bedpostx_logs_dir:         Ref,
        bedpostx_input_dir:        Out,
        bedpostx_dir:              Out,
        bedpostx_eye:              Out,
        bedpostx_merged:           Out,
        bedpostx_data:             Out,
        bedpostx_bvals:            Out,
        bedpostx_bvecs:            Out,
        bedpostx_nodif:            Out,
        bedpostx_nodif_brain:      Out,
        bedpostx_nodif_brain_mask: Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        w = os.getcwd() + os.sep

        if not op.exists(bedpostx_data):

            # Copy of bvals and bvecs
            copyfile(src=AP_bval, dst=bedpostx_bvals)
            copyfile(src=AP_bvec, dst=bedpostx_bvecs)
            copyfile(src=eddy_nodif, dst=bedpostx_nodif)
            copyfile(src=eddy_nodif_brain_mask, dst=bedpostx_nodif_brain_mask)

            wrappers.fslmaths(bedpostx_nodif).mas(bedpostx__nodif_brain_mask).\
                run(bedpostx_nodif_brain)

            rel_path = op.relpath(eddy_data_ud,
                                  op.dirname(bedpostx_data))
            os.symlink(src=rel_path, dst=bedpostx_data)

        qv = query_cuda_version()

        if qv:
            wrappers.bedpostx_gpu(data_dir= w + bedpostx_input_dir,
                                  forcedir=True,
                                  nf="3",
                                  fudge="1",
                                  bi="3000",
                                  nj="1250",
                                  se="25",
                                  model="2",
                                  cnonlinear=True)
                                  #TotalNumParts="1",
                                  #bindir=ctx.FSLDIR)

        else:
            wrappers.bedpostx(data_dir= w + bedpostx_input_dir,
                              forcedir=True,
                              nf="3",
                              fudge="1",
                              bi="3000",
                              nj="1250",
                              se="25",
                              model="2",
                              cnonlinear=True)
