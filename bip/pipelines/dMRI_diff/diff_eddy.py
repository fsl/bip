#!/usr/bin/env python
#
# diff_eddy.py - Sub-pipeline with FSL's eddy processing of the dMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,W0719,R0912
#

import os
import os.path as op
import logging
from shutil import copyfile
import nibabel as nib
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        AP:                             In,
        AP_bval:                        In(optional=True),
        AP_bvec:                        In(optional=True),
        PA_bval:                        In(optional=True),
        PA_bvec:                        In(optional=True),
        AP_best_index:                  In,
        fieldmap_iout_mean:             In,
        fieldmap_mask:                  In,
        fieldmap_mask_ud:               In,
        acqparams:                      In,
        logs_dir:                       Ref,
        eddy_data_prefix:               Ref,
        fieldmap_out_prefix:            Ref,
        eddy_AP:                        Out,
        eddy_nodif:                     Out,
        eddy_nodif_brain_mask:          Out,
        eddy_nodif_brain_mask_ud:       Out,
        eddy_bvals:                     Out,
        eddy_bvecs:                     Out,
        eddy_index:                     Out,
        eddy_data:                      Out,
        eddy_outlier_report:            Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        # Adapting the code to let the user use commin BVAL and BVEC files
        folder_b_files = ctx.get("dMRI_b_files", "")

        if op.exists(folder_b_files):
            new_AP_bval = op.join(folder_b_files, "AP.bval")
            new_PA_bval = op.join(folder_b_files, "PA.bval")
            new_AP_bvec = op.join(folder_b_files, "AP.bvec")
            new_PA_bvec = op.join(folder_b_files, "PA.bvec")

            if op.exists(new_AP_bval):
                copyfile(src=new_AP_bval, dst=AP_bval)
            else:
                raise Exception("Missing AP.bval in " + folder_b_files)
            if op.exists(new_PA_bval):
                copyfile(src=new_PA_bval, dst=PA_bval)
            else:
                raise Exception("Missing PA.bval in " + folder_b_files)
            if op.exists(new_AP_bvec):
                copyfile(src=new_AP_bvec, dst=AP_bvec)
            else:
                raise Exception("Missing AP.bvec in " + folder_b_files)
            if op.exists(new_PA_bvec):
                copyfile(src=new_PA_bvec, dst=PA_bvec)
            else:
                raise Exception("Missing PA.bvec in " + folder_b_files)

        # Creates links
        if eddy_AP is not None and not op.exists(eddy_AP):
            rel_path = op.relpath(AP, op.dirname(eddy_AP))
            os.symlink(src=rel_path, dst=eddy_AP)
        if eddy_nodif is not None and not op.exists(eddy_nodif):
            rel_path = op.relpath(fieldmap_iout_mean, op.dirname(eddy_nodif))
            os.symlink(src=rel_path, dst=eddy_nodif)
        if eddy_nodif_brain_mask is not None and \
                not op.exists(eddy_nodif_brain_mask):
            rel_path = op.relpath(fieldmap_mask,
                                  op.dirname(eddy_nodif_brain_mask))
            os.symlink(src=rel_path, dst=eddy_nodif_brain_mask)
        if eddy_nodif_brain_mask_ud is not None and \
                not op.exists(eddy_nodif_brain_mask_ud):
            rel_path = op.relpath(fieldmap_mask_ud,
                                  op.dirname(eddy_nodif_brain_mask_ud))
            os.symlink(src=rel_path, dst=eddy_nodif_brain_mask_ud)

        copyfile(src=AP_bval, dst=eddy_bvals)
        copyfile(src=AP_bvec, dst=eddy_bvecs)

        AP_img    = nib.load(eddy_AP)
        AP_numvol = AP_img.header['dim'][4]

        indices = ("1 " * AP_numvol).strip() + "\n"

        with open(AP_best_index, 'r', encoding="utf-8") as f:
            AP_index = int(f.read())

        with open(eddy_index, 'wt', encoding="utf-8") as f:
            f.write(indices)

        wrappers.eddy(imain=eddy_AP,
                      mask=eddy_nodif_brain_mask,
                      topup=fieldmap_out_prefix,
                      acqp=acqparams,
                      index=eddy_index,
                      bvecs=eddy_bvecs,
                      bvals=eddy_bvals,
                      out=eddy_data_prefix,
                      ref_scan_no=AP_index,
                      flm="quadratic",
                      resamp="jac",
                      slm="linear",
                      niter=8,
                      fwhm=[10, 8, 4, 2, 0, 0, 0, 0],
                      ff=10,
                      sep_offs_move=True,
                      nvoxhp=1000,
                      very_verbose=True,
                      repol=True,
                      rms=True)
