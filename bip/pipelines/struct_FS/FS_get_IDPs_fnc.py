#!/usr/bin/env python
#
# asl_get_IDPs.py - Sub-pipeline generating the IDPs of the ASL pipeline.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1718,C0201,C0206,R0912,W0702,C0301,W0718,W0719
#

import os
import os.path as op
import logging
from bip.utils.log_utils import run_command

log = logging.getLogger(__name__)

def check_and_create_dir(dirName):
    try:
        os.stat(dirName)
    except Exception as e:
        print(str(e))
        os.mkdir(dirName)

def read_file(fileName):
    result = []
    with open(fileName, 'r', encoding="utf-8") as f:
        for line in f.readlines():
            line = line.replace('\t', ' ')
            result.append([x.replace("\n", '') for x in line.split(' ')])
    return result

def generate_FS_IDP_files(subject_ID, FS_stats_dir, data_dir, ctx, env):

    # ODO: Include a pre-requisite that python2.7 must be availble in the system
    if op.isfile(op.join(FS_stats_dir, 'aseg.stats')):
        run_command(log, 'asegstats2table  ' +
                         ' -m volume --all-segs --tablefile ' +
                         op.join(data_dir, 'aseg_1.txt') +
                         ' --subjects ' + subject_ID +
                         ' --skip', env=env)
        run_command(log, 'asegstats2table  ' +
                         ' -m mean --all-segs --tablefile ' +
                         op.join(data_dir, 'aseg_intensity.txt') +
                         ' --subjects ' + subject_ID +
                         ' --skip', env=env)

    if op.isfile(op.join(FS_stats_dir, 'lh.w-g.pct.stats')):
        run_command(log, 'asegstats2table ' +
                         ' -m mean --all-segs --stats=lh.w-g.pct.stats ' +
                         ' --tablefile ' +
                         op.join(data_dir, 'wg_lh_mean.txt ') +
                         ' --subjects ' + subject_ID +
                         ' --skip', env=env)

    if op.isfile(op.join(FS_stats_dir, 'rh.w-g.pct.stats')):
        run_command(log, 'asegstats2table ' +
                         ' -m mean --all-segs --stats=rh.w-g.pct.stats ' +
                         ' --tablefile ' +
                         op.join(data_dir, 'wg_rh_mean.txt ') +
                         ' --subjects ' + subject_ID +
                         ' --skip', env=env)

    for hemi in ["lh", "rh"]:
        for value in ["volume", "area", "thickness"]:
            for atlas in ["BA_exvivo", "aparc.DKTatlas", "aparc.a2009s",
                          "aparc"]:
                file_name = atlas + '_' + hemi + '_' + value + '.txt'
                out_file_name = op.join(data_dir, file_name)
                if op.isfile(op.join(FS_stats_dir,
                                     hemi + "." + atlas + '.stats')):
                    run_command(log, 'aparcstats2table ' +
                                     ' -m ' + value + ' --hemi=' + hemi +
                                     ' --tablefile ' + out_file_name +
                                     ' --subjects ' + subject_ID +
                                     ' --skip -p ' + atlas, env=env)

    atlas = "aparc.pial"
    value = "area"
    for hemi in ["lh", "rh"]:
        file_name = atlas + '_' + hemi + '_' + value + '.txt'
        out_file_name = op.join(data_dir, file_name)
        if op.isfile(op.join(FS_stats_dir, hemi + '.aparc.pial.stats')):
            run_command(log, 'aparcstats2table ' +
                             ' -m ' + value + ' --hemi=' + hemi +
                             ' --tablefile ' + out_file_name +
                             ' --subjects ' + subject_ID +
                             ' --skip -p ' + atlas, env=env)

    with open(ctx.get_data('FS/FS_initial_files.txt'), encoding="utf-8") as f:
        files_generated = [x.replace('\n', '').split(" ")
                           for x in f.readlines()]

    data_dict = {}

    for file_generated in files_generated:
        file_name = op.join(data_dir, file_generated[0] + '.txt')
        if op.isfile(file_name):
            data = read_file(file_name)
        else:
            data = read_file(ctx.get_data('FS/FS_data_ex/' +
                                          file_generated[0] + '.txt'))
        data_dict[file_generated[0]] = data

    data_dict['ID'] = [['ID'], [ctx.subject]]

    return data_dict


# Quick consistency check
def check_consistency(data_dict, subjects_dir, FS_IDPs, ctx):

    for file_generated in data_dict.keys():
        if len(data_dict[file_generated]) > 2:
            save_data_NaNs(subjects_dir, FS_IDPs, ctx)
            raise Exception("Error in " + file_generated +
                            ': File has more than 2 lines')

        len0 = len(data_dict[file_generated][0])
        len1 = len(data_dict[file_generated][1])

        if len0 != len1:
            save_data_NaNs(subjects_dir, FS_IDPs, ctx)
            raise Exception("Error in " + file_generated +
                            ': Inconsistent # of features')


def fix_aseg_data(data_dict, subject_dir):

    s_1 = 'aseg_1'
    s_global = 'aseg_global'
    s_volume = 'aseg_volume'

    # Split aseg_1 into aseg_global and aseg_volume
    data_dict[s_global] = [[], []]
    data_dict[s_global][0] = [data_dict[s_1][0][0]] + data_dict[s_1][0][46:]
    data_dict[s_global][1] = [data_dict[s_1][1][0]] + data_dict[s_1][1][46:]

    # Variables not needed
    vars_to_delete = ['CortexVol', 'CerebralWhiteMatterVol',
                      'SupraTentorialVolNotVentVox', 'MaskVol', 'SurfaceHoles']
    ind_to_delete = []
    for i in range(len(data_dict[s_global][0])):
        if not data_dict[s_global][0][i] in vars_to_delete:
            ind_to_delete.append(i)

    data_dict[s_global][0] = [data_dict[s_global][0][x] for x in ind_to_delete]
    data_dict[s_global][1] = [data_dict[s_global][1][x] for x in ind_to_delete]

    # For some reason, the VentricleChoroidVol is not caught by asegstats2table
    try:
        file_name = op.join(subject_dir, 'stats' , 'aseg.stats')
        with open(file_name, 'r', encoding="utf-8") as f:
            val = [x.split(',')[3].strip() for x in f.readlines()
                   if 'VentricleChoroidVol' in x]
    except Exception:
        val = ["NaN"]

    data_dict[s_global][0].append('VentricleChoroidVol')
    data_dict[s_global][1].append(val[0])

    data_dict[s_volume] = [[], []]
    data_dict[s_volume][0] = data_dict[s_1][0][0:46]
    data_dict[s_volume][1] = data_dict[s_1][1][0:46]

    del data_dict[s_1]

    # Remove the WM-hypointensities. No value in any subject
    cols_to_remove = ['Left-WM-hypointensities',
                      'Right-WM-hypointensities',
                      'Left-non-WM-hypointensities',
                      'Right-non-WM-hypointensities']

    for key in list(data_dict.keys()):
        if key.startswith('aseg'):
            sub_keys_to_remove = []
            for sub_key in data_dict[key][0]:
                for col in cols_to_remove:
                    if col in sub_key:
                        sub_keys_to_remove.append(sub_key)

            for sub_key in sub_keys_to_remove:
                ind = data_dict[key][0].index(sub_key)
                del data_dict[key][0][ind]
                del data_dict[key][1][ind]

    return data_dict


def gen_aparc_special(data_dict, subject_dir):

    struct_data = [['aparc.pial_lh_area', 'TotalSurface',
                    'lh.aparc.pial.stats', 'PialSurfArea'],
                   ['aparc.pial_rh_area', 'TotalSurface',
                    'rh.aparc.pial.stats', 'PialSurfArea'],
                   ['aparc_lh_area', 'TotalSurface',
                    'lh.aparc.stats', 'WhiteSurfArea'],
                   ['aparc_rh_area', 'TotalSurface',
                    'rh.aparc.stats', 'WhiteSurfArea'],
                   ['aparc_lh_thickness', 'GlobalMeanThickness',
                    'lh.aparc.stats', 'MeanThickness'],
                   ['aparc_rh_thickness', 'GlobalMeanThickness',
                    'rh.aparc.stats', 'MeanThickness']]

    for elem in struct_data:
        data_dict[elem[0]][0].append(elem[1])
        try:
            with open(op.join(subject_dir, 'stats', elem[2]), 'r',
                      encoding="utf-8") as f:
                v = [x.split(',')[3].strip() for x in f.readlines()
                     if elem[3] in x]
                data_dict[elem[0]][1].append(v[0])
        except Exception as e:
            print(str(e))
            data_dict[elem[0]][1].append('NaN')

    return data_dict


def bool_FLAIR(data_dict, subject_dir):
    if op.isfile(op.join(subject_dir, 'mri', 'FLAIR.mgz')):
        data_dict['FLAIR'] = [['Use-T2-FLAIR-for-FreeSurfer'], ['1']]
    else:
        data_dict['FLAIR'] = [['Use-T2-FLAIR-for-FreeSurfer'], ['0']]

    return data_dict


def gen_subsegmentation(data_dict, subject_dir, subject, ctx):
    struct_data = {'Brainstem_global': [['brainstemSsVolumes.v13.txt'], 5],
                   'ThalamNuclei': [['ThalamicNuclei.v13.T1.volumes.txt'], 52],
                   'AmygNuclei_lh': [['lh.amygNucVolumes-T1-AN.v22.txt',
                                      'lh.amygNucVolumes-T1.v22.txt'], 10],
                   'AmygNuclei_rh': [['rh.amygNucVolumes-T1-AN.v22.txt',
                                      'rh.amygNucVolumes-T1.v22.txt'], 10],
                   'HippSubfield_lh': [['lh.hippoSfVolumes-T1-AN.v22.txt',
                                        'lh.hippoSfVolumes-T1.v22.txt'], 22],
                   'HippSubfield_rh': [['rh.hippoSfVolumes-T1-AN.v22.txt',
                                        'rh.hippoSfVolumes-T1.v22.txt'], 22]}

    for struct in struct_data.keys():
        found = False
        data_dict[struct] = [[], []]
        for fil in struct_data[struct][0]:
            final_fil = op.join(subject_dir, 'FreeSurfer', 'mri', fil)
            if op.isfile(final_fil):
                with open(final_fil, 'r', encoding="utf-8") as f:
                    for lin in f.readlines():
                        lin = lin.replace('\n', '').split(' ')
                        data_dict[struct][0].append(lin[0])
                        data_dict[struct][1].append(lin[1])
                found = True
                break

        if not found:
            with open(ctx.get_data('FS/FS_sub_headers/' +
                      struct + '.txt'), encoding="utf-8") as f:
                data_dict[struct][0] = [x.replace('\n', '') for x in f.
                                        readlines()]
                data_dict[struct][0] = ['NaN'] * len(data_dict[struct][0])
        data_dict[struct][0] = ['ID'] + data_dict[struct][0]
        data_dict[struct][1] = [subject] + data_dict[struct][1]
    return data_dict

def fix_aparc_data(data_dict):
    # Remove the column "temporalpole" in aparc files.
    # Unreliable measure: Very few subjects have that measure.
    for key in list(data_dict.keys()):
        if key.startswith('aparc'):
            sub_keys_to_remove = []
            for sub_key in data_dict[key][0]:
                if 'temporalpole' in sub_key:
                    sub_keys_to_remove.append(sub_key)
            for sub_key in sub_keys_to_remove:
                ind = data_dict[key][0].index(sub_key)

                if ind != -1:
                    del data_dict[key][0][ind]
                    del data_dict[key][1][ind]

    # Remove ETIV and BrainSegVolNotVent (They are global)
    for key in list(data_dict.keys()):
        if key.startswith('aparc') or key.startswith('BA_exvivo'):
            sub_keys_to_remove = []
            for sub_key in  data_dict[key][0]:
                if sub_key in ['BrainSegVolNotVent' , 'eTIV']:
                    sub_keys_to_remove.append(sub_key)
            for sub_key in sub_keys_to_remove:
                ind = data_dict[key][0].index(sub_key)
                del data_dict[key][0][ind]
                del data_dict[key][1][ind]

    # Removing last colum for thickness in aparc
    for key in list(data_dict.keys()):
        if key.startswith('aparc') and key.endswith('thickness'):
            sub_keys_to_remove = []
            for sub_key in data_dict[key][0]:
                if sub_key.endswith('MeanThickness_thickness'):
                    sub_keys_to_remove.append(sub_key)
            for sub_key in sub_keys_to_remove:
                ind = data_dict[key][0].index(sub_key)
                del data_dict[key][0][ind]
                del data_dict[key][1][ind]

    # Removing the last column for areas, also in BA
    for key in list(data_dict.keys()):
        if key.endswith('area'):
            sub_keys_to_remove = []
            for sub_key in data_dict[key][0]:
                if sub_key.endswith('WhiteSurfArea_area'):
                    sub_keys_to_remove.append(sub_key)
            for sub_key in sub_keys_to_remove:
                ind = data_dict[key][0].index(sub_key)
                del data_dict[key][0][ind]
                del data_dict[key][1][ind]

    return data_dict

# Remove first feature in case it is the subject ID (except for ID itself)
def remove_first_feature(data_dict, subject):
    for key in list(data_dict.keys()):
        if key != "ID":
            if (data_dict[key][1][0] == subject) or \
               (data_dict[key][1][0] == ('FS_' + subject)) or \
               (data_dict[key][1][0] == ''):
                del data_dict[key][0][0]
                del data_dict[key][1][0]
    return data_dict

def fix_headers(data_dict):
    # Applying some general replacing rules for the categories
    replace_rules = [['.', '-'],
                     ['BA_exvivo',        'BA-exvivo'],
                     ['AmygNuclei_lh',    'AmygNuclei_lh_volume'],
                     ['AmygNuclei_rh',    'AmygNuclei_rh_volume'],
                     ['Brainstem_global', 'Brainstem_global_volume'],
                     ['HippSubfield_lh',  'HippSubfield_lh_volume'],
                     ['HippSubfield_rh',  'HippSubfield_rh_volume'],
                     ['wg_lh_mean',       'wg_lh_intensity-contrast'],
                     ['wg_rh_mean',       'wg_rh_intensity-contrast'],
                     ['aparc_lh',         'aparc-Desikan_lh'],
                     ['aparc_rh',         'aparc-Desikan_rh'],
                     ['FLAIR',            'Use-T2-FLAIR-for-FreeSurfer']]

    for key in list(data_dict.keys()):
        new_key = key
        for rule in replace_rules:
            if rule[0] in new_key:
                new_key = new_key.replace(rule[0], rule[1])
                data_dict[new_key] = data_dict.pop(key)

    # Renaming some special cases. How is it done? For each of these labels, if
    # in aseg_global (struct[0], there is a field called struct[1], then

    # For each of these labels, if in aseg_global
    s_global = 'aseg_global'
    structs = [[s_global, 'lhSurfaceHoles',
                'aseg_lh_number', 'HolesBeforeFixing'],
               [s_global, 'rhSurfaceHoles',
                'aseg_rh_number', 'HolesBeforeFixing'],
               [s_global, 'BrainSegVol-to-eTIV',
                'aseg_global_volume-ratio', 'BrainSegVol-to-eTIV'],
               [s_global, 'MaskVol-to-eTIV',
                'aseg_global_volume-ratio', 'MaskVol-to-eTIV']]

    for struct in structs:
        if struct[1] in data_dict[struct[0]][0]:
            index = data_dict[struct[0]][0].index(struct[1])
            if struct[2] not in data_dict.keys():
                data_dict[struct[2]] = [struct[3]], [data_dict[struct[0]][1][index]]
            else:
                data_dict[struct[2]][0].append(struct[3])
                data_dict[struct[2]][1].append(data_dict[struct[0]][1][index])
            del data_dict[struct[0]][0][index]
            del data_dict[struct[0]][1][index]

    for metric in ['volume', 'intensity']:
        old_key = 'aseg_' + metric
        for i in range(len(data_dict[old_key][0])):
            if 'Left-' in data_dict[old_key][0][i]:
                new_name = data_dict[old_key][0][i].replace('Left-', '')
                new_key = 'aseg_lh_' + metric

                if new_key not in data_dict.keys():
                    data_dict[new_key] = [[], []]
                data_dict[new_key][0].append(new_name)
                data_dict[new_key][1].append(data_dict[old_key][1][i])
            elif 'Right-' in data_dict[old_key][0][i]:
                new_name = data_dict[old_key][0][i].replace('Right-', '')
                new_key = 'aseg_rh_' + metric

                if new_key not in data_dict.keys():
                    data_dict[new_key] = [[], []]
                data_dict[new_key][0].append(new_name)
                data_dict[new_key][1].append(data_dict[old_key][1][i])
            else:
                new_name = data_dict[old_key][0][i]
                new_key = 'aseg_global_' + metric
                if new_key not in data_dict.keys():
                    data_dict[new_key] = [[], []]
                data_dict[new_key][0].append(new_name)
                data_dict[new_key][1].append(data_dict[old_key][1][i])

        del data_dict[old_key]

    for i in range(len(data_dict[s_global][0])):
        if data_dict[s_global][0][i].startswith('lh'):
            new_name = data_dict[s_global][0][i].replace('lh', '').\
                replace('Vol', '')
            data_dict['aseg_lh_volume'][0].append(new_name)
            data_dict['aseg_lh_volume'][1].append(data_dict[s_global][1][i])
        elif data_dict[s_global][0][i].startswith('rh'):
            new_name = data_dict[s_global][0][i].replace('rh', '').\
                replace('Vol', '')
            data_dict['aseg_rh_volume'][0].append(new_name)
            data_dict['aseg_rh_volume'][1].append(data_dict[s_global][1][i])
        else:
            new_name = data_dict[s_global][0][i].replace('Vol', '')
            data_dict['aseg_global_volume'][0].append(new_name)
            data_dict['aseg_global_volume'][1].append(data_dict[s_global][1][i])

    del data_dict[s_global]

    # Split ThalamNuclei into Left and Right
    data_dict['ThalamNuclei_lh_volume'] = [[], []]
    data_dict['ThalamNuclei_rh_volume'] = [[], []]
    for i in range(len(data_dict['ThalamNuclei'][0])):
        if "Left" in data_dict['ThalamNuclei'][0][i]:
            new_name = data_dict['ThalamNuclei'][0][i].replace('Left-', '')
            data_dict['ThalamNuclei_lh_volume'][0].append(new_name)
            data_dict['ThalamNuclei_lh_volume'][1].\
                append(data_dict['ThalamNuclei'][1][i])

        elif "Right" in data_dict['ThalamNuclei'][0][i]:
            new_name = data_dict['ThalamNuclei'][0][i].replace('Right-', '')
            data_dict['ThalamNuclei_rh_volume'][0].append(new_name)
            data_dict['ThalamNuclei_rh_volume'][1].\
                append(data_dict['ThalamNuclei'][1][i])
    del data_dict['ThalamNuclei']

    # Removing redundant prefix and sufix in BA_excvivo
    BAs = ['_exvivo_area', '_exvivo_thickness', '_exvivo_volume']
    BA_keys = [key for key in list(data_dict.keys()) if key.
               startswith('BA-exvivo')]
    for key in BA_keys:
        for i in range(len(data_dict[key][0])):
            for BA in BAs:
                data_dict[key][0][i] = data_dict[key][0][i].replace(BA, '').\
                    replace('rh_', '').replace('lh_', '')

    # Removing redundant prefix and sufix in aparc
    aparcs = ['_area', '_thickness', '_volume', '-area', '-thickness',
              '-volume']
    aparc_keys = [key for key in list(data_dict.keys()) if key.
                  startswith('aparc')]
    for key in aparc_keys:
        for i in range(len(data_dict[key][0])):
            for aparc in aparcs:
                data_dict[key][0][i] = data_dict[key][0][i].\
                    replace(aparc, '').replace('rh_', '').replace('lh_', '')

    # Changing weird and inconsistent characters
    for key in list(data_dict.keys()):
        for i in range(len(data_dict[key][0])):
            data_dict[key][0][i] = data_dict[key][0][i].replace('_', '-').\
                replace('&', '+')

    return data_dict


def save_data_NaNs(subjects_dir, FS_IDPs, ctx):

    with open(ctx.get_data('FS/FS_headers.txt'), encoding="utf-8") as f:
        final_headers = [x.replace('\n', '') for x in f.readlines()]

    num_NaNs = len(final_headers) - 1

    with open(FS_IDPs, 'w', encoding="utf-8") as f:
        values = ['NaN'] * num_NaNs
        values_str = subjects_dir + " " + " ".join(values)
        f.write(f"{values_str}\n")
        f.close()


def save_data(data_dict, FS_IDPs, ctx):

    with open(ctx.get_data('FS/FS_headers.txt'), encoding="utf-8") as f:
        final_headers = [x.replace('\n', '') for x in f.readlines()]

    temp_headers = {}

    for key in list(data_dict.keys()):
        if key in ['ID', 'Use-T2-FLAIR-for-FreeSurfer']:
            temp_headers[key] = data_dict[key][1][0]
        else:
            for i in range(len(data_dict[key][0])):
                temp_headers[key+"_"+data_dict[key][0][i]] = data_dict[key][1][i]

    for x in final_headers:
        if x not in temp_headers.keys():
            temp_headers[x] = "NaN"

    with open(FS_IDPs, 'w', encoding="utf-8") as f:
        values = [temp_headers[x] for x in final_headers]
        values_str = " ".join(values)
        f.write(f"{values_str}\n")
        f.close()


def save_headers_info(data_dict, FS_headers_info, ctx):

    with open(ctx.get_data('FS/FS_final_headers.txt'), encoding="utf-8") as f:
        final_headers = [x.replace('\n', '') for x in f.readlines()]

    temp_headers = {}

    for key in list(data_dict.keys()):
        if key in ['ID', 'Use-T2-FLAIR-for-FreeSurfer']:
            temp_headers[key] = data_dict[key][1][0]
        else:
            for i in range(len(data_dict[key][0])):
                temp_headers[key+"_"+data_dict[key][0][i]] = data_dict[key][1][i]

    for x in final_headers:
        if x not in temp_headers.keys():
            temp_headers[x] = "NaN"

    with open(FS_headers_info, 'w', encoding="utf-8") as f:
        values = [temp_headers[x] for x in final_headers]
        values_str = " ".join(values)
        f.write(f"{values_str}\n")

        f.close()

def bb_FS_get_IDPs(ctx, env, FreeSurfer_dir, FS_IDPs, FS_headers_info,
                   FS_stats_dir):

    subject      = ctx.subject
    subject_ID   = FreeSurfer_dir.split(op.sep)[-1]
    subjects_dir = op.join(os.getcwd(), ctx.subject)
    data_dir     = op.join(os.getcwd(), FreeSurfer_dir, 'data') + op.sep

    if not op.isdir(subjects_dir):
        raise Exception("Error: FreeSurfer has not been run on this subject")

    check_and_create_dir(data_dir)

    data_dict = generate_FS_IDP_files(subject_ID, FS_stats_dir, data_dir, ctx,
                                      env)
    data_dict = fix_aseg_data(data_dict, FreeSurfer_dir)
    data_dict = gen_aparc_special(data_dict, FreeSurfer_dir)
    data_dict = gen_subsegmentation(data_dict, subjects_dir, subject, ctx)
    data_dict = bool_FLAIR(data_dict, subjects_dir)
    data_dict = fix_aparc_data(data_dict)
    data_dict = remove_first_feature(data_dict, subject)
    data_dict = fix_headers(data_dict)

    check_consistency(data_dict, subjects_dir, FS_IDPs, ctx)
    save_data(data_dict, FS_IDPs, ctx)
    # save_headers_info(data_dict, FS_headers_info, ctx)
