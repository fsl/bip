#!/usr/bin/env python
#
# fsl_get_IDPs.py - Sub-pipeline generating the IDPs of the ASL pipeline.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1718,C0201,C0206,R0912,W0702,C0301
#

import os
import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from bip.pipelines.struct_FS.FS_get_IDPs_fnc import bb_FS_get_IDPs
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        ThalamicNuclei:             In,
        BrainStemSubfields:         In,
        LHippoAmyg:                 In,
        RHippoAmyg:                 In,
        rh_entorhinal_exvivo_label: In,
        FS_stats_dir:               In,
        FreeSurfer_dir:             In,
        logs_dir:                   Ref,
        FS_IDPs:                    Out,
        FS_headers_info:            Ref):

    with redirect_logging(job_name(run), outdir=logs_dir):

        env = dict(os.environ, SUBJECTS_DIR=op.join(os.getcwd(), ctx.subject))

        bb_FS_get_IDPs(ctx, env, FreeSurfer_dir, FS_IDPs, FS_headers_info,
                       FS_stats_dir)
