#!/usr/bin/env python
#
# FS_proc.py - Sub-pipeline with the FreeSurfer main processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os
import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, run_command, job_name

log = logging.getLogger(__name__)

def run(ctx,
        rh_entorhinal_exvivo_label: In,
        T2_FLAIR_unbiased:          Ref,
        FreeSurfer_FLAIR:           Ref,
        logs_dir:                   Ref,
        FreeSurfer_dir:             Ref,
        ThalamicNuclei:             Out,
        BrainStemSubfields:         Out,
        LHippoAmyg:                 Out,
        RHippoAmyg:                 Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        subjects_dir = op.join(os.getcwd(), ctx.subject)

        log.info(run_command(log, 'rm -f ' + subjects_dir + \
                             '/FreeSurfer/scripts/IsRunning*'))

        if not op.exists(ThalamicNuclei):
            log.info(run_command(log, 'segmentThalamicNuclei.sh FreeSurfer ' +
                                 subjects_dir))
        if not op.exists(BrainStemSubfields):
            log.info(run_command(log, 'segmentBS.sh FreeSurfer ' + subjects_dir))

        if not op.exists(RHippoAmyg):
            if T2_FLAIR_unbiased is not None and op.exists(T2_FLAIR_unbiased):
                log.info(run_command(log, 'segmentHA_T2.sh FreeSurfer ' +
                                     FreeSurfer_FLAIR + ' AN 1 ' + subjects_dir))

            else:
                log.info(run_command(log, 'segmentHA_T1.sh FreeSurfer ' +
                                     subjects_dir))
