#!/usr/bin/env python
#
# struct_FS.py - Pipeline with the FreeSurfer processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1735
#

import logging
from bip.utils.log_utils     import job_name
from bip.pipelines.struct_FS import FS_proc, FS_segm, FS_get_IDPs

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree):

    subj = ctx.subject

    pipe(FS_proc.run,
         submit=dict(jobtime=200, name=job_name(FS_proc.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(FS_segm.run,
         submit=dict(jobtime=200, name=job_name(FS_segm.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(FS_get_IDPs.run,
         submit=dict(jobtime=200, name=job_name(FS_get_IDPs.run, subj)),
         kwargs={'ctx' : ctx})
