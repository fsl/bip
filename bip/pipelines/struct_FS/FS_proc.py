#!/usr/bin/env python
#
# FS_proc.py - Sub-pipeline with the FreeSurfer main processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os
import os.path as op
import shutil
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, run_command, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_unbiased:                In,
        T2_FLAIR_unbiased:          In(optional=True),
        logs_dir:                   Ref,
        fsaverage:                  Ref,
        rh_entorhinal_exvivo_label: Out,
        FreeSurfer_dir:             Out,
        FS_stats_dir:               Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        # We need to delete the folder because otherwise, FreeSurfer complains
        if FreeSurfer_dir is not None and op.exists(FreeSurfer_dir):
            shutil.rmtree(FreeSurfer_dir)

        subjects_dir = op.join(os.getcwd(), ctx.subject)
        T1_path      = op.join(os.getcwd(), T1_unbiased)
        T2_path      = op.join(os.getcwd(), T2_FLAIR_unbiased)

        cmd = 'rm -rf ' + subjects_dir + \
              '/FreeSurfer ; recon-all -all -s FreeSurfer -i ' + \
              T1_path + ' -sd ' + subjects_dir

        if T2_FLAIR_unbiased is not None and op.exists(T2_FLAIR_unbiased):
            cmd += " -FLAIR " + T2_path + " -FLAIRpial"

        try:
            log.info(run_command(log, cmd))
        finally:
            if fsaverage is not None and op.exists(fsaverage):
                os.unlink(fsaverage)
