#!/usr/bin/env python
#
# tfMRI_feat.py - Sub-pipeline with FSL's FEAT tool applied to the task fMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import shutil
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        tfMRI_fsf:                  In,
        logs_dir:                   Ref,
        tfMRI_feat:                 Out,
        tfMRI_mc_rel_mean:          Out,
        tfMRI_filtered_func_data:   Out,
        tfMRI_example_func2highres: Out,
        tfMRI_cope1:                Out,
        tfMRI_cope2:                Out,
        tfMRI_cope3:                Out,
        tfMRI_cope4:                Out,
        tfMRI_cope5:                Out,
        tfMRI_zstat1:               Out,
        tfMRI_zstat2:               Out,
        tfMRI_zstat3:               Out,
        tfMRI_zstat4:               Out,
        tfMRI_zstat5:               Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        if tfMRI_feat is not None and op.exists(tfMRI_feat):
            shutil.rmtree(tfMRI_feat)

        wrappers.feat(fsf=tfMRI_fsf)
