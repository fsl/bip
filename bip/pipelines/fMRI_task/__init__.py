#!/usr/bin/env python
#
# fMRI_task.py - Pipeline with the task fMRI processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1735
#

import logging
from bip.utils.log_utils     import job_name
from bip.pipelines.fMRI_task import tfMRI_prepare, tfMRI_feat

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree):

    subj = ctx.subject

    pipe(tfMRI_prepare.run,
         submit=dict(jobtime=200, name=job_name(tfMRI_prepare.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(tfMRI_feat.run,
         submit=dict(jobtime=200, name=job_name(tfMRI_feat.run, subj)),
         kwargs={'ctx' : ctx})
