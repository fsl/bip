#!/usr/bin/env python
#
# tfMRI_prepare.py - Sub-pipeline with the processing previous to feat tfMRI.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os
import os.path as op
import logging
from shutil import copyfile
import nibabel as nib
from fsl_pipe import In, Out, Ref
from gradunwarp.core.gradient_unwarp_apply import gradient_unwarp_apply
from fsl import wrappers
from bip.utils.get_dwell_time import get_dwell_time
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1:                                      In,
        T1_brain:                                In,
        T1_to_MNI_warp:                          In,
        T1_to_MNI_linear_mat:                    In,
        T1_fast_pve_2:                           In,
        tfMRI:                                   In,
        tfMRI_SBREF:                             In,
        tfMRI_json:                              In,
        fieldmap_fout_to_T1_brain_rad:           In,
        logs_dir:                                Ref,
        tfMRI_feat:                              Ref,
        tfMRI_SBREF_GDC:                         Out,
        tfMRI_T1:                                Out,
        tfMRI_T1_brain:                          Out,
        tfMRI_T1_brain2MNI152_T1_2mm_brain_warp: Out,
        tfMRI_T1_brain2MNI152_T1_2mm_brain_mat:  Out,
        tfMRI_T1_brain_wmseg:                    Out,
        tfMRI_SBREF_ud:                          Out,
        tfMRI_SBREF_ud_warp:                     Out,
        tfMRI_fsf:                               Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        # Creates links
        if tfMRI_T1 is not None and not op.exists(tfMRI_T1):
            rel_path = op.relpath(T1, op.dirname(tfMRI_T1))
            os.symlink(src=rel_path, dst=tfMRI_T1)
        if tfMRI_T1_brain is not None and not op.exists(tfMRI_T1_brain):
            rel_path = op.relpath(T1_brain, op.dirname(tfMRI_T1_brain))
            os.symlink(src=rel_path, dst=tfMRI_T1_brain)
        if tfMRI_T1_brain2MNI152_T1_2mm_brain_warp is not None and \
                not op.exists(tfMRI_T1_brain2MNI152_T1_2mm_brain_warp):
            rel_path = op.relpath(T1_to_MNI_warp,
                            op.dirname(tfMRI_T1_brain2MNI152_T1_2mm_brain_warp))
            os.symlink(src=rel_path,
                       dst=tfMRI_T1_brain2MNI152_T1_2mm_brain_warp)
        if tfMRI_T1_brain2MNI152_T1_2mm_brain_mat is not None and \
                not op.exists(tfMRI_T1_brain2MNI152_T1_2mm_brain_mat):
            rel_path = op.relpath(T1_to_MNI_linear_mat,
                             op.dirname(tfMRI_T1_brain2MNI152_T1_2mm_brain_mat))
            os.symlink(src=rel_path, dst=tfMRI_T1_brain2MNI152_T1_2mm_brain_mat)

        wrappers.fslmaths(T1_fast_pve_2).thr(0.5).bin().\
            run(tfMRI_T1_brain_wmseg)

        # Generation of FSF file
        copyfile(src=ctx.get_data('fMRI_fsf/design_TASK.fsf'), dst=tfMRI_fsf)

        fMRI_img   = nib.load(tfMRI)
        fmriNumVol = fMRI_img.header['dim'][4]
        fmriTR     = fMRI_img.header['pixdim'][4]
        fmriDwell  = get_dwell_time(tfMRI, tfMRI_json)
        fmriTE     = 39  # Default value for task fMRI TE
        descrip    = str(fMRI_img.header['descrip'])

        fields = descrip.split(";")

        for field in fields:
            t = field.split("=")

            fieldName  = t[0]
            fieldValue = t[1]

            if fieldName == "TE":
                fmriTE = fieldValue

        gdc = ctx.get('gdc', None)

        if gdc not in (None, "", "none"):
            # Calculate and apply the Gradient Distortion Unwarp
            # TODO: Review the "half=True" in next version
            gradient_unwarp_apply(WD=tfMRI_SBREF_GDC,
                                  infile=tfMRI_SBREF,
                                  outfile=tfMRI_SBREF_ud,
                                  owarp=tfMRI_SBREF_ud_warp,
                                  gradcoeff=gdc,
                                  vendor='siemens', nojac=True, half=False)
        else:
            copyfile(src=tfMRI_SBREF, dst=tfMRI_SBREF_ud)

        with open(tfMRI_fsf, 'a', encoding="utf-8") as f:
            file_name = ctx.get_standard("MNI152_T1_2mm_brain")
            f.write('set fmri(regstandard) "' + file_name + '"\n')

            file_name = ctx.get_data('fMRI_fsf/designS.txt')
            f.write('set fmri(custom1) "' + file_name + '"\n')

            file_name = ctx.get_data('fMRI_fsf/designF.txt')
            f.write('set fmri(custom2) "' + file_name + '"\n')

            file_name = op.join(os.getcwd(), tfMRI_feat)
            f.write('set fmri(outputdir) "' + file_name + '"\n')

            file_name = op.join(os.getcwd(), tfMRI)
            f.write('set feat_files(1) "' + file_name + '"\n')

            file_name = op.join(os.getcwd(), tfMRI_SBREF)
            f.write('set alt_ex_func(1) "' + file_name + '"\n')

            file_name = op.join(os.getcwd(), fieldmap_fout_to_T1_brain_rad)
            f.write('set unwarp_files(1) "' + file_name + '"\n')

            file_name = op.join(os.getcwd(), tfMRI_T1_brain)
            f.write('set unwarp_files_mag(1) "' + file_name + '"\n')

            file_name = op.join(os.getcwd(), tfMRI_T1_brain)
            f.write('set highres_files(1) "' + file_name + '"\n')

            if ctx.gdc != '':
                file_name = op.join(os.getcwd(), tfMRI_SBREF_ud_warp)
                f.write('set fmri(gdc) "' + file_name + '"\n')

            f.write('set fmri(tr) ' + str(fmriTR) + '\n')
            f.write('set fmri(npts) ' + str(fmriNumVol) + '\n')
            f.write('set fmri(dwell) ' + str(fmriDwell) + '\n')
            f.write('set fmri(te) ' + str(fmriTE) + '\n')
