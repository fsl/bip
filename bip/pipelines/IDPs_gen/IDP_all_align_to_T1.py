#!/usr/bin/env python
#
# IDP_all_align_to_T1.py - Generating IDP file with alignment to T1 metric.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=R1704,E1101
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_brain:                   In,
        T1_brain_mask:              In,
        T2_FLAIR_brain:             In(optional=True),
        fieldmap_iout_to_T1:        In(optional=True),
        SWI_to_T1:                  In(optional=True),
        rfMRI_example_func2highres: In(optional=True),
        tfMRI_example_func2highres: In(optional=True),
        tmp_dir:                    Ref,
        logs_dir:                   Ref,
        IDP_all_align_to_T1:        Out):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        tmp_mat = op.join(tmp_dir, 'tmp_mat.mat')

        result=""

        MC = op.join(ctx.FSLDIR, 'etc', 'flirtsch', 'measurecost1.sch')

        for file_name in [T2_FLAIR_brain, fieldmap_iout_to_T1, SWI_to_T1,
                         rfMRI_example_func2highres,tfMRI_example_func2highres]:

            if file_name is not None and op.exists(file_name):
                costs1 = wrappers.flirt(src=file_name, ref=T1_brain,
                                        refweight=T1_brain_mask, schedule=MC,
                                        omat=tmp_mat).stdout[0].strip()
                result += " " + str(costs1.split()[0])
            else:
                result += " NaN"

        result = result.strip()

        with open(IDP_all_align_to_T1, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
