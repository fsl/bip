#!/usr/bin/env python
#
# IDP_T2_FLAIR_WMH.py - Generating IDP file with WMH segmentation metrics
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T2_FLAIR_bianca_volume: In,
        logs_dir:               Ref,
        IDP_T2_FLAIR_WMH:       Out):

    with redirect_logging(job_name(run), outdir=logs_dir):
        copyfile(src=T2_FLAIR_bianca_volume, dst=IDP_T2_FLAIR_WMH)
