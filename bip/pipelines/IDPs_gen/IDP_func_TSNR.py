#!/usr/bin/env python
#
# IDP_func_TSNR.py - Generating IDP file with measure of TSNR of fMRI
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1704
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        filtered_func_data:       In(optional=True),
        filtered_func_data_clean: In(optional=True),
        tfMRI_filtered_func_data: In(optional=True),
        logs_dir:                 Ref,
        tmp_dir:                  Ref,
        IDP_func_TSNR:            Out):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        fMRI_SNR = op.join(tmp_dir, 'fMRI_SNR.nii.gz')

        result =""

        for file_name in [filtered_func_data, filtered_func_data_clean,
                          tfMRI_filtered_func_data]:
            if file_name is not None and op.exists(file_name):
                wrappers.fslmaths(file_name).Tstd().run(fMRI_SNR)
                wrappers.fslmaths(file_name).Tmean().div(fMRI_SNR).run(fMRI_SNR)
                v = 1 / wrappers.fslstats(fMRI_SNR).l(0.1).p(50).run()
                result += f"{v} "
            else:
                result += "NaN "

        result = result.strip()

        with open(IDP_func_TSNR, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
