#!/usr/bin/env python
#
# IDP_T1_GM_parcellation.py - Generating IDP file with GM parcellation metrics
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=R1704
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1:                      In,
        T1_to_MNI_warp_coef_inv: In,
        T1_fast_pve_1:           In,
        tmp_dir:                 Ref,
        logs_dir:                Ref,
        IDP_T1_GM_parcellation:  Out):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        GMatlas_to_T1 = op.join(tmp_dir, 'GMatlas_to_T1.nii.gz')
        GMatlas = ctx.get_data("IDPs/GMatlas/GMatlas.nii.gz")

        wrappers.applywarp(src=GMatlas, ref=T1, w=T1_to_MNI_warp_coef_inv,
                           out=GMatlas_to_T1, interp='nn')

        vals = wrappers.fslstats(T1_fast_pve_1, K=GMatlas_to_T1).m.v.run()
        result = " ".join([str(x) for x in vals[:, 0] * vals[:, 1]])

        with open(IDP_T1_GM_parcellation, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
