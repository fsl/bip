#!/usr/bin/env python
#
# IDP_T1_SIENAX.py - Generating IDP file with SIENAX metrics of T1.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import logging
from shutil import copyfile
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_sienax_report: In,
        T1_sienax_txt:    Ref,
        logs_dir:         Ref,
        IDP_T1_SIENAX:    Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        result = ("NaN " * 11).strip()

        if T1_sienax_report is not None and op.exists(T1_sienax_report):
            with open(T1_sienax_report, "r",  encoding="utf-8") as f:
                text = f.readlines()
                # indices that we are interested in
                result  = text[0].split()[1] + " "
                result += text[2].split()[1] + " " + text[2].split()[2] + " "
                result += text[3].split()[1] + " " + text[3].split()[2] + " "
                result += text[4].split()[1] + " " + text[4].split()[2] + " "
                result += text[5].split()[1] + " " + text[5].split()[2] + " "
                result += text[6].split()[1] + " " + text[6].split()[2]

        with open(IDP_T1_SIENAX, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')

        copyfile(src=IDP_T1_SIENAX, dst=T1_sienax_txt)
