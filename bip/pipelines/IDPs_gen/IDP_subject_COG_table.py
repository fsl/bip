#!/usr/bin/env python
#
# IDP_subject_centre.py - Generating IDP file with the acquisition centre.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_QC_COG:          In,
        T1_dcm_txt:         In,
        dMRI_dcm_txt:       Ref,
        rfMRI_dcm_txt:      Ref,
        SWI_dcm_txt:        Ref,
        T2_dcm_txt:         Ref,
        tfMRI_dcm_txt:      Ref,
        logs_dir:           Ref,
        IDP_subject_COG_table: Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        with open(T1_QC_COG, "r", encoding="utf-8") as f:
            COG  = f.read().strip()

        table = "NaN"
        table_text = "(0019, 1012) [TablePositionOrigin]"

        for file_name in [T1_dcm_txt, T2_dcm_txt, rfMRI_dcm_txt, SWI_dcm_txt,
                          dMRI_dcm_txt, tfMRI_dcm_txt]:

            if file_name is not None and op.exists(file_name):
                with open(file_name, 'rt', encoding="utf-8") as f:
                    cont = f.readlines()
                    for x in cont:
                        if x.startswith(table_text):
                            table = x.replace(table_text, "")
                            table = table.strip().replace("SL: ", "").strip()
                            table = table.split()[2].replace("]", "")
                            break
            if table != "NaN":
                break

        with open(IDP_subject_COG_table, 'wt', encoding="utf-8") as f:
            f.write(f'{COG} {table}\n')
