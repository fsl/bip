#!/usr/bin/env python
#
# IDP_diff_autoPtx.py - Generating IDP file with autoPtx metrics
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
#

import os.path as op
import json
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        IDP_subject_ID:           In(optional=True),
        IDP_T1_align_to_std:      In(optional=True),
        IDP_T1_noise_ratio:       In(optional=True),
        IDP_all_align_to_T1:      In(optional=True),
        IDP_func_head_motion:     In(optional=True),
        IDP_func_TSNR:            In(optional=True),
        IDP_diff_eddy_outliers:   In(optional=True),
        IDP_T1_SIENAX:            In(optional=True),
        IDP_T1_FIRST_vols:        In(optional=True),
        IDP_T1_GM_parcellation:   In(optional=True),
        IDP_T2_FLAIR_WMH:         In(optional=True),
        IDP_SWI_T2star:           In(optional=True),
        IDP_func_task_activation: In(optional=True),
        IDP_diff_TBSS:            In(optional=True),
        IDP_diff_autoPtx:         In(optional=True),
        IDP_subject_COG_table:    In(optional=True),
        IDP_subject_centre:       In(optional=True),
        logs_dir:                 Ref,
        IDPs:                     Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        result = ""
        IDPs_json_file = ctx.get_data("IDPs/IDPs.json")

        with open(IDPs_json_file, "r", encoding="utf-8") as f:
            IDPs_dict = json.load(f)

        for IDP_file in [IDP_subject_ID, IDP_T1_align_to_std,
                         IDP_T1_noise_ratio, IDP_all_align_to_T1,
                         IDP_func_head_motion, IDP_func_TSNR,
                         IDP_diff_eddy_outliers, IDP_T1_SIENAX,
                         IDP_T1_FIRST_vols, IDP_T1_GM_parcellation,
                         IDP_T2_FLAIR_WMH, IDP_SWI_T2star,
                         IDP_func_task_activation, IDP_diff_TBSS,
                         IDP_diff_autoPtx, IDP_subject_COG_table,
                         IDP_subject_centre]:

            plain_name = op.basename(IDP_file).replace(".txt", "")
            num_IDPs = len(IDPs_dict[plain_name])
            result_nans = ("NaN " * num_IDPs).strip()

            if IDP_file is not None and op.exists(IDP_file):
                with open(IDP_file, "r", encoding="utf-8") as f:
                    IDPs_l = f.read().strip().split()
                if len(IDPs_l) != num_IDPs:
                    result += " " + result_nans
                else:
                    result += " " + " ".join(IDPs_l)

            else:
                result += " " + result_nans

        result = result.replace("  ", " ").strip()

        print(result)

        with open(IDPs, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
