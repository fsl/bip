#!/usr/bin/env python
#
# IDP_subject_ID.py - Generating IDP file with the subject ID.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_orig:        In,
        IDP_subject_ID: Out,
        logs_dir:       Ref):

    with redirect_logging(job_name(run), outdir=logs_dir):
        with open(IDP_subject_ID, 'wt', encoding="utf-8") as f:
            f.write(f'{ctx.subject}\n')
