#!/usr/bin/env python
#
# IDP_func_task_activation.py - Generating IDP file with tfMRI activation
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
#

import os
import os.path as op
import shutil
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_to_MNI_warp:            In,
        T1_to_MNI_warp_coef_inv:   In,
        tfMRI_feat:                In,
        tfMRI_cope1:               In,
        tfMRI_cope2:               In,
        tfMRI_cope5:               In,
        tfMRI_zstat1:              In,
        tfMRI_zstat2:              In,
        tfMRI_zstat5:              In,
        highres2standard_warp:     Ref,
        highres2standard_warp_inv: Ref,
        logs_dir:                  Ref,
        tfMRI_featquery_1_dir:     Out,
        tfMRI_featquery_2_dir:     Out,
        tfMRI_featquery_5_dir:     Out,
        tfMRI_featquery_5a_dir:    Out,
        tfMRI_featquery_1_report:  Out,
        tfMRI_featquery_2_report:  Out,
        tfMRI_featquery_5_report:  Out,
        tfMRI_featquery_5a_report: Out,
        IDP_func_task_activation:  Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        if highres2standard_warp is not None and \
            not op.exists(highres2standard_warp):
            rel_path = op.relpath(T1_to_MNI_warp,
                                  op.dirname(highres2standard_warp))
            os.symlink(src=rel_path, dst=highres2standard_warp)

        if highres2standard_warp_inv is not None and \
            not op.exists(highres2standard_warp_inv):
            rel_path = op.relpath(T1_to_MNI_warp_coef_inv,
                                  op.dirname(highres2standard_warp_inv))
            os.symlink(src=rel_path, dst=highres2standard_warp_inv)

        for dir_name in [tfMRI_featquery_1_dir, tfMRI_featquery_2_dir,
                         tfMRI_featquery_5_dir, tfMRI_featquery_5a_dir]:
            if dir_name is not None and op.exists(dir_name):
                shutil.rmtree(dir_name)

        group_mask_1  = ctx.get_data("tfMRI_group/groupMask1.nii.gz")
        group_mask_2  = ctx.get_data("tfMRI_group/groupMask2.nii.gz")
        group_mask_5  = ctx.get_data("tfMRI_group/groupMask5.nii.gz")
        group_mask_5a = ctx.get_data("tfMRI_group/groupMask5a.nii.gz")

        tfMRI_feat += os.sep

        N_tfMRI_cope1 = tfMRI_cope1.replace(tfMRI_feat, "")
        N_tfMRI_cope2 = tfMRI_cope2.replace(tfMRI_feat, "")
        N_tfMRI_cope5 = tfMRI_cope5.replace(tfMRI_feat, "")

        N_tfMRI_zstat1 = tfMRI_zstat1.replace(tfMRI_feat, "")
        N_tfMRI_zstat2 = tfMRI_zstat2.replace(tfMRI_feat, "")
        N_tfMRI_zstat5 = tfMRI_zstat5.replace(tfMRI_feat, "")

        N_tfMRI_featquery_1_dir  = tfMRI_featquery_1_dir.replace(tfMRI_feat, "")
        N_tfMRI_featquery_2_dir  = tfMRI_featquery_2_dir.replace(tfMRI_feat, "")
        N_tfMRI_featquery_5_dir  = tfMRI_featquery_5_dir.replace(tfMRI_feat, "")
        N_tfMRI_featquery_5a_dir = tfMRI_featquery_5a_dir.replace(tfMRI_feat,"")


        wrappers.featquery(featdirs=[tfMRI_feat],
                           stats=[N_tfMRI_cope1, N_tfMRI_zstat1],
                           outputRootName=N_tfMRI_featquery_1_dir,
                           mask=group_mask_1)
        wrappers.featquery(featdirs=[tfMRI_feat],
                           stats=[N_tfMRI_cope2, N_tfMRI_zstat2],
                           outputRootName=N_tfMRI_featquery_2_dir,
                           mask=group_mask_2)
        wrappers.featquery(featdirs=[tfMRI_feat],
                           stats=[N_tfMRI_cope5, N_tfMRI_zstat5],
                           outputRootName=N_tfMRI_featquery_5_dir,
                           mask=group_mask_5)
        wrappers.featquery(featdirs=[tfMRI_feat],
                           stats=[N_tfMRI_cope5, N_tfMRI_zstat5],
                           outputRootName=N_tfMRI_featquery_5a_dir,
                           mask=group_mask_5a)

        result=""

        for file_name in [tfMRI_featquery_1_report, tfMRI_featquery_2_report,
                          tfMRI_featquery_5_report, tfMRI_featquery_5a_report]:
            with open(file_name, "r", encoding="utf-8") as f:
                report = f.readlines()

            a = round(float(report[0].split()[6]) / 100, 6)
            b = round(float(report[0].split()[7]) / 100, 6)
            c = round(float(report[-1].split()[6]), 6)
            d = round(float(report[-1].split()[7]), 6)

            result += " " + str(a) + " " + str(b) + " " + str(c) + " " + str(d)

        result = result.strip()

        with open(IDP_func_task_activation, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
