#!/usr/bin/env python
#
# IDP_T1_noise_ratio.py - Generating IDP file with measure of noise ratio of T1.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1704
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_fast_pveseg:    In,
        T1:                 In,
        tmp_dir:            Ref,
        logs_dir:           Ref,
        IDP_T1_noise_ratio: Out):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        tmp_SNR = op.join(tmp_dir, 'tmp_SNR.nii.gz')

        wrappers.fslmaths(T1_fast_pveseg).thr(2).uthr(2).ero().run(tmp_SNR)
        grey  = wrappers.fslstats(T1).k(tmp_SNR).m.run()
        wrappers.fslmaths(T1_fast_pveseg).thr(3).uthr(3).ero().run(tmp_SNR)
        white = wrappers.fslstats(T1).k(tmp_SNR).m.run()

        # TODO: The roundings are only there to mimic the behaviour of
        #       previous version. We do not have to do it in next version
        brain    = int(round((grey + white) / 2))
        contrast = int(round(white - grey))
        thresh   = int(round(brain / 10))

        noise = wrappers.fslstats(T1).l(0.001).u(thresh).s.run()

        SNR = noise / brain
        CNR = noise / contrast

        with open(IDP_T1_noise_ratio, 'wt', encoding="utf-8") as f:
            f.write(f'{SNR} {CNR}\n')
