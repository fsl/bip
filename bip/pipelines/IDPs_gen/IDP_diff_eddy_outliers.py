#!/usr/bin/env python
#
# IDP_diff_eddy_outliers.py - Generating IDP file with eddy outliers metrics
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        eddy_outlier_report:    In(optional=True),
        logs_dir:               Ref,
        IDP_diff_eddy_outliers: Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        num_outliers = 0

        if eddy_outlier_report is not None and op.exists(eddy_outlier_report):
            with open(eddy_outlier_report, "r", encoding="utf-8") as f:
                num_outliers = str(len(f.readlines()))
        else:
            num_outliers = "NaN"

        with open(IDP_diff_eddy_outliers, 'wt', encoding="utf-8") as f:
            f.write(f'{num_outliers}\n')
