#!/usr/bin/env python
#
# IDP_func_head_motion.py - Generating IDP file with measure of fMRI head motion
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import json
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        rfMRI_mc_rel_mean:    In(optional=True),
        tfMRI_mc_rel_mean:    In(optional=True),
        logs_dir:             Ref,
        IDP_func_head_motion: Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        if rfMRI_mc_rel_mean is not None and op.exists(rfMRI_mc_rel_mean):
            with open(rfMRI_mc_rel_mean, "r", encoding="utf-8") as f:
                val_1 = json.load(f)
        else:
            val_1 = "NaN"

        if tfMRI_mc_rel_mean is not None and op.exists(tfMRI_mc_rel_mean):
            with open(tfMRI_mc_rel_mean, "r", encoding="utf-8") as f:
                val_2 = json.load(f)
        else:
            val_2 = "NaN"

        with open(IDP_func_head_motion, 'wt', encoding="utf-8") as f:
            f.write(f'{val_1} {val_2}\n')
