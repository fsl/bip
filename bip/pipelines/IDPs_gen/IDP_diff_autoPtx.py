#!/usr/bin/env python
#
# IDP_diff_autoPtx.py - Generating IDP file with autoPtx metrics
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1704
#

import os.path as op
import logging
import nibabel as nib
from fsl_pipe import In, Out, Ref, Var
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        logs_dir:           Ref,
        TBSS_prefix:        Ref,
        aptx_txt_prefix:    Ref,
        tmp_dir:            Ref,
        aptx_tract_tmp:     In,
        autoptx_tract:      Var(no_iter=True),
        IDP_diff_autoPtx:   Out):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        autoPtx_all = op.join(tmp_dir, 'autoPtx_all.nii.gz')
        autoPtx_tmp = op.join(tmp_dir, 'autoPtx_tmp.nii.gz')

        result_NaN = ("NaN " * 27).strip()
        result = ""

        # TODO: See if it is possible to generate the aptx_tract_tmp files here
        wrappers.fslmerge("t", autoPtx_all, *list(aptx_tract_tmp.data))
        tractnorm = wrappers.fslstats(autoPtx_all, t=True).m.run()

        for suffix in ['FA','MD','MO','L1','L2','L3','ICVF','OD','ISOVF']:
            all_file = TBSS_prefix + suffix + ".nii.gz"
            APTX_file = aptx_txt_prefix + suffix + ".txt"

            if op.exists(all_file):
                wrappers.fslmaths(autoPtx_all).mul(all_file).run(autoPtx_tmp)
                tractmeas = wrappers.fslstats(autoPtx_tmp, t=True).m.run()

                img = nib.load(autoPtx_all)
                jmax = int(round(img.header['dim'][4]))

                cad = ""
                for i in range(jmax):
                    cad += " " + str(tractmeas[i] / tractnorm[i])
                cad = cad.strip()

                with open(APTX_file, 'wt', encoding="utf-8") as f:
                    f.write(f'{cad}\n')

                result += " " + cad
            else:
                result += " " + result_NaN

        result = result.strip()

        with open(IDP_diff_autoPtx, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
