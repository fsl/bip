#!/usr/bin/env python
#
# IDP_SWI_T2star.py - Generating IDP file with metrics of T2star
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T2star_to_T1:               In,
        T1_first_all_fast_firstseg: In(optional=True),
        logs_dir:                   Ref,
        IDP_SWI_T2star:             Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        result = ("NaN " * 14).strip()

        if T1_first_all_fast_firstseg is not None and \
                op.exists(T1_first_all_fast_firstseg):
            v = wrappers.fslstats(T2star_to_T1, K=T1_first_all_fast_firstseg).\
                p(50).run()

            # Check that the outputs are OK
            if len(v) == 58:
                # indices that we are interested in
                ind = [9, 48, 10, 49, 11, 50, 12, 51, 16, 52, 17, 53, 25, 57]
                result = [str(int(v[x])) for x in ind]
                result = " ".join(result)

        with open(IDP_SWI_T2star, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
