#!/usr/bin/env python
#
# IDP_subject_centre.py - Generating IDP file with the acquisition centre.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
#

import os.path as op
import json
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_dcm_txt:         In,
        dMRI_dcm_txt:       Ref,
        rfMRI_dcm_txt:      Ref,
        SWI_dcm_txt:        Ref,
        T2_dcm_txt:         Ref,
        tfMRI_dcm_txt:      Ref,
        logs_dir:           Ref,
        IDP_subject_centre: Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        line = ""
        address_text = "(0008, 0081) Institution Address"

        for file_name in [T1_dcm_txt, T2_dcm_txt, rfMRI_dcm_txt, SWI_dcm_txt,
                          dMRI_dcm_txt, tfMRI_dcm_txt]:

            if file_name is not None and op.exists(file_name):
                with open(file_name, 'rt', encoding="utf-8") as f:
                    cont = f.readlines()

                    for x in cont:
                        if x.startswith(address_text):
                            line = x.replace(address_text, "")
                            line = line.strip().replace("ST: ", "").strip()
                            line = line.replace("'","").replace(",","__")
                            line = line.replace(" ","_")
                            break
            if line != "":
                break

        if line != "":
            # TODO: Allow users to change the file with the sites
            json_name = "IDPs/sites_UKBB.json"
            with open(ctx.get_data(json_name), "r", encoding="utf-8") as f:
                sites = json.load(f)

        centre = sites[line]

        with open(IDP_subject_centre, 'wt', encoding="utf-8") as f:
            f.write(f'{centre}\n')
