#!/usr/bin/env python
#
# IDP_T1_align_to_std.py - Generating IDP file with alignment to std metrics.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=E1101,R1704
#

import os
import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from fsl import wrappers
from bip.utils.log_utils import redirect_logging, tempdir, job_name

log = logging.getLogger(__name__)

def run(ctx,
        T1_brain:             In,
        T1_to_MNI_linear_mat: In,
        T1_brain_to_MNI:      In,
        T1_to_MNI_warp_jac:   In,
        tmp_dir:              Ref,
        logs_dir:             Ref,
        IDP_T1_align_to_std:  Out):

    with redirect_logging(job_name(run), outdir=logs_dir),\
         tempdir(op.join(tmp_dir, job_name(run))) as tmp_dir:

        tmp_jac = op.join(tmp_dir, 'tmpjac.nii.gz')
        tmp_mat = op.join(tmp_dir, 'tmp_mat.mat')

        MC = ctx.FSLDIR + os.sep + op.join('etc', 'flirtsch',
                                           'measurecost1.sch')
        MNI152_T1_1mm_brain = ctx.get_standard("MNI152_T1_1mm_brain.nii.gz")
        MNI152_T1_1mm_brain_mask = ctx.get_standard("MNI152_T1_1mm_brain_mask.nii.gz")

        costs1 = wrappers.flirt(src=T1_brain, ref=MNI152_T1_1mm_brain,
                                refweight=MNI152_T1_1mm_brain_mask,
                                init=T1_to_MNI_linear_mat,
                                schedule=MC, omat=tmp_mat).stdout[0].strip()
        val1 = costs1.split()[0]

        costs2 = wrappers.flirt(src=T1_brain_to_MNI, ref=MNI152_T1_1mm_brain,
                                refweight=MNI152_T1_1mm_brain_mask,
                                schedule=MC, omat=tmp_mat).stdout[0].strip()
        val2 = costs2.split()[0]

        wrappers.fslmaths(T1_to_MNI_warp_jac).sub(1).sqr().run(tmp_jac)
        val3 = wrappers.fslstats(tmp_jac, K=MNI152_T1_1mm_brain_mask).m.run()

        with open(IDP_T1_align_to_std, 'wt', encoding="utf-8") as f:
            f.write(f'{val1} {val2} {val3}\n')
