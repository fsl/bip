#!/usr/bin/env python
#
# struct_T1.py - Pipeline with the T1w processing.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613,R1735
#

import logging
from bip.utils.log_utils    import job_name
from bip.pipelines.IDPs_gen import IDP_subject_ID, IDP_subject_centre
from bip.pipelines.IDPs_gen import IDP_subject_COG_table, IDP_all_align_to_T1
from bip.pipelines.IDPs_gen import IDP_T1_FIRST_vols, IDP_T1_SIENAX
from bip.pipelines.IDPs_gen import IDP_T1_GM_parcellation, IDP_T1_align_to_std
from bip.pipelines.IDPs_gen import IDP_T1_noise_ratio, IDP_T2_FLAIR_WMH
from bip.pipelines.IDPs_gen import IDP_SWI_T2star, IDP_func_head_motion
from bip.pipelines.IDPs_gen import IDP_func_TSNR, IDP_func_task_activation
from bip.pipelines.IDPs_gen import IDP_diff_eddy_outliers, IDP_diff_TBSS
from bip.pipelines.IDPs_gen import IDP_diff_autoPtx, IDPs_generator

log = logging.getLogger(__name__)

def add_to_pipeline(ctx, pipe, tree):

    subj = ctx.subject

    pipe(IDP_subject_ID.run,
         submit=dict(jobtime=200, name=job_name(IDP_subject_ID.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_T1_align_to_std.run,
         submit=dict(jobtime=200, name=job_name(IDP_T1_align_to_std.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_T1_noise_ratio.run,
         submit=dict(jobtime=200, name=job_name(IDP_T1_noise_ratio.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_all_align_to_T1.run,
         submit=dict(jobtime=200, name=job_name(IDP_all_align_to_T1.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_func_head_motion.run,
         submit=dict(jobtime=200, name=job_name(IDP_func_head_motion.run,
                                                subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_func_TSNR.run,
         submit=dict(jobtime=200, name=job_name(IDP_func_TSNR.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_diff_eddy_outliers.run,
         submit=dict(jobtime=200, name=job_name(IDP_diff_eddy_outliers.run,
                                                subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_T1_SIENAX.run,
         submit=dict(jobtime=200, name=job_name(IDP_T1_SIENAX.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_T1_FIRST_vols.run,
         submit=dict(jobtime=200, name=job_name(IDP_T1_FIRST_vols.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_T1_GM_parcellation.run,
         submit=dict(jobtime=200, name=job_name(IDP_T1_GM_parcellation.run,
                                                subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_T2_FLAIR_WMH.run,
         submit=dict(jobtime=200, name=job_name(IDP_T2_FLAIR_WMH.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_SWI_T2star.run,
         submit=dict(jobtime=200, name=job_name(IDP_SWI_T2star.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_func_task_activation.run,
         submit=dict(jobtime=200, name=job_name(IDP_func_task_activation.run,
                                                subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_diff_TBSS.run,
         submit=dict(jobtime=200, name=job_name(IDP_diff_TBSS.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_diff_autoPtx.run,
         submit=dict(jobtime=200, name=job_name(IDP_diff_autoPtx.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_subject_COG_table.run,
         submit=dict(jobtime=200, name=job_name(IDP_subject_COG_table.run,
                                                subj)),
         kwargs={'ctx' : ctx})
    pipe(IDP_subject_centre.run,
         submit=dict(jobtime=200, name=job_name(IDP_subject_centre.run, subj)),
         kwargs={'ctx' : ctx})
    pipe(IDPs_generator.run,
         submit=dict(jobtime=200, name=job_name(IDPs_generator.run, subj)),
         kwargs={'ctx' : ctx})
