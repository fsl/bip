#!/usr/bin/env python
#
# IDP_diff_TBSS.py - Generating IDP file with TBSS metrics
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W0613
#

import os.path as op
import logging
from fsl_pipe import In, Out, Ref
from bip.utils.log_utils import redirect_logging, job_name

log = logging.getLogger(__name__)

def run(ctx,
        JHUrois_FA:     In(optional=True),
        JHUrois_MD:     In(optional=True),
        JHUrois_MO:     In(optional=True),
        JHUrois_L1:     In(optional=True),
        JHUrois_L2:     In(optional=True),
        JHUrois_L3:     In(optional=True),
        JHUrois_ICVF:   In(optional=True),
        JHUrois_OD:     In(optional=True),
        JHUrois_ISOVF:  In(optional=True),
        logs_dir:       Ref,
        IDP_diff_TBSS:  Out):

    with redirect_logging(job_name(run), outdir=logs_dir):

        nan_result = ("NaN " * 48).strip()

        result = ""

        for file_name in [JHUrois_FA, JHUrois_MD, JHUrois_MO, JHUrois_L1,
                          JHUrois_L2, JHUrois_L3, JHUrois_ICVF, JHUrois_OD,
                          JHUrois_ISOVF]:
            print(file_name)
            if op.exists(file_name):
                with open(file_name, "r", encoding="utf-8") as f:
                    mini_result = f.read()
                    mini_result = mini_result.replace("\n", " ")
                    mini_result = mini_result.replace("  ", " ")
                    mini_result = mini_result.replace("   ", " ")
                    mini_result = mini_result.replace("[", "")
                    mini_result = mini_result.replace("]", "").strip()
            else:
                mini_result = nan_result

            result += mini_result + " "

        result = result.strip()

        with open(IDP_diff_TBSS, 'wt', encoding="utf-8") as f:
            f.write(f'{result}\n')
