#!/usr/bin/env python
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
# pylint: disable=W1203
#

"""The bip.utils package contains a range of miscellaneous utilities. """

import contextlib
import logging
import os.path as op
import os
import time
import errno
import bip

log = logging.getLogger(__name__)

@contextlib.contextmanager
def lockdir(dirname, delay=5):
    """Lock a directory for exclusive access.

    Primitive mechanism by which concurrent access to a directory can be
    prevented. Attempts to create a semaphore file in the directory, but waits
    if that file already exists. Removes the file when finished.
    """

    lockfile = op.join(dirname, '.fsl_ci.lockdir')

    while True:
        try:
            log.debug(f'Attempting to lock {dirname} for exclusive access.')
            fd = os.open(lockfile, os.O_CREAT | os.O_EXCL | os.O_RDWR)
            break
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise e
            log.debug('%s is already locked - trying again '
                      'in %s seconds ...', dirname, delay)
        time.sleep(10)

    log.debug('Exclusive access acquired for %s ...', dirname)
    try:
        yield

    finally:
        log.debug('Relinquishing lock on %s', dirname)
        os.close(fd)
        os.unlink(lockfile)

def get_data(file_name):
    """Return the file_name, prefixed with the bip/data/ directory path. """
    file_name = file_name.replace('/', os.sep)
    return op.join(bip.BIPDIR, 'data', file_name)
