#!/usr/bin/env python
#
# read_json_field.py - Utility to read json fields.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
#

import sys
import json
import numbers
import argparse


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write(f'error: {message}\n')
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def read_json_field(fileName, fieldName, rounding=0, multFactor=1):

    result = []
    with open(fileName, 'r', encoding="utf-8") as data_file:
        data = json.load(data_file)

    if fieldName in data.keys():
        value = data[fieldName]
        if isinstance(value, numbers.Number):
            if rounding != 0:
                result = round(data[fieldName]*multFactor, rounding)
            else:
                result = data[fieldName]*multFactor
        else:
            result = str(data[fieldName])

    return result


def main():

    parser = MyParser(description='BioBank Dicom Header Reader')
    parser.add_argument('-F', dest="file", type=str, nargs=1,
                        help='Read json file')
    parser.add_argument('-f', dest="field", type=str, nargs=1, default="NONE",
                        help='Read field')
    parser.add_argument('-r', dest="rounding", type=int, default=0,
                        help='Round the value the selected number of decimals' +
                        ' (Default: No rounding')
    parser.add_argument('-m', dest="multFactor",  type=float, default=1,
                        help='Multiplication factor for the selected value (Default 1)')

    argsa = parser.parse_args()

    if argsa.file is None:
        parser.print_help()
        sys.exit()

    if argsa.field is None:
        parser.print_help()
        sys.exit()

    rounding = argsa.rounding
    multFactor = argsa.multFactor

    fileName = argsa.file[0]
    fieldName = argsa.field[0]

    res = read_json_field(fileName, fieldName, rounding, multFactor)

    print(str(res))

if __name__ == "__main__":
    main()
