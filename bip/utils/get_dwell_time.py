#!/usr/bin/env python
#
# get_dwell_time.py - Utility to get the dwell time of a 4D image.
#
# Author: Fidel Alfaro Almagro <fidel.alfaroalmagro@ndcn.ox.ac.uk>
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
# Author: Michiel Cottaar <michiel.cottaar@ndcn.ox.ac.uk>
#
# pylint: disable=C0103,E0602,C0114,C0115,C0116,R0913,R0914,R0915
#

import os
import nibabel as nib
from bip.utils.read_json_field import read_json_field

def get_dwell_time(img, json):
    if os.path.exists(json):
        t = read_json_field(fileName=json, fieldName="EffectiveEchoSpacing",
                            rounding=4,  multFactor=1000)
        if t in ["[]", ""]:
            t = read_json_field(fileName=json,
                                fieldName="EstimatedEffectiveEchoSpacing",
                                rounding=4,  multFactor=1000)
        return t

    im = nib.load(img)
    descrip = str(im.header['descrip'])

    fields = descrip.split(";")

    for field in fields:
        t = field.split("=")

        fieldName  = t[0]
        fieldValue = t[1]

        if fieldName == "dwell":
            return fieldValue
    return 0
