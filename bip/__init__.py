import os.path as op

BIPDIR      = op.dirname(__file__)
__version__ = open(op.join(BIPDIR, 'VERSION')).read()
