-------------------------------------------
BRAIN IMAGING PIPELINE
Fidel Alfaro Almagro, WIN-FMRIB
June, 2022
-------------------------------------------

Automated tool

Installation
------------

BIP is published as a conda package, and can be installed with a command such as:

```
mamba install                                                 \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ \
  -c conda-forge                                              \
  fsl-bip
```

If you want to install everythong that is needed from scratch, run:

```
cd /path-to-lib/install_dir/
pip install -e .
```
