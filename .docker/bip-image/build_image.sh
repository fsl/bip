#!/usr/bin/env bash

set -e

SHELL_PROFILE="/root/.bashrc" ;

export TAR_OPTIONS="--no-same-owner" ;
export WGET_OPTIONS="--no-check-certificate -q" ;

# OS / build dependencies
yum install -y epel-release ;
yum install -y \
    bzip2 \
    curl \
    file \
    freetype \
    git \
    git-lfs \
    libgomp \
    libXmu-devel \
    libffi \
    mesa-libGL \
    nano \
    openssh-clients \
    parallel \
    python3 \
    tcsh \
    unzip \
    which \
    wget ;

# FSL
export FSL_URL="https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/releases/fslinstaller.py" ;
wget ${WGET_OPTIONS} ${FSL_URL} -O fslinstaller.py ;
python fslinstaller.py -V 6.0.7.10 -d /usr/local/fsl ;
echo -e '\nexport FSLDIR=/usr/local/fsl/'                    >> ${SHELL_PROFILE} ;
echo -e 'source ${FSLDIR}/etc/fslconf/fsl.sh'                >> ${SHELL_PROFILE} ;
echo -e "export FSLOUTPUTTYPE=\"NIFTI_GZ\""                  >> ${SHELL_PROFILE} ;
echo -e "export FSLSUB_CONF=\"\$BIPDIR/data/fsl_sub.yml\"\n" >> ${SHELL_PROFILE} ;

rm -rf fslinstaller.py ;

#Update fsl-pipe
/usr/local/fsl/bin/conda update -y fsl-pipe ;

# BIP
/usr/local/fsl/bin/conda install -y -n base --override-channels    \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/ \
  -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/      \
  -c conda-forge                                                   \
   fsl-bip ;
echo -e "\nexport BIPDIR=\"/usr/local/fsl/lib/python3.11/site-packages/bip\"" >> ${SHELL_PROFILE} ;
echo -e "chmod 755 \$BIPDIR/main.py"                                          >> ${SHELL_PROFILE} ;

# For no-FS image
if [ "$1" == "0" ] ; then
  # ROBEX
  ROBEX_URL=https://www.nitrc.org/frs/download.php/5994/ROBEXv12.linux64.tar.gz//\?i_agree\=1\&download_now\=1 ;
  pushd /usr/local/ ;
  wget ${WGET_OPTIONS} ${ROBEX_URL} -O ROBEX.tar.gz ;
  tar xf ROBEX.tar.gz && rm -rf ROBEX.tar.gz ;
  echo -e '\nexport PATH=/usr/local/ROBEX:${PATH}\n' >> ${SHELL_PROFILE} ;
  popd ;

  # Brainsuite
  BRAINSUITE_URL=http://brainsuite.org/data/BIDS/bse15c.biobank.tgz ;
  pushd /usr/local/ ;
  wget ${WGET_OPTIONS} ${BRAINSUITE_URL} -O brainsuite.tgz ;
  tar -xf brainsuite.tgz && rm -rf brainsuite.tgz ;
  chmod -R ugo+rx BrainSuite15c ;
  echo -e '\nexport PATH=/usr/local/BrainSuite15c/bin:${PATH}\n' >> ${SHELL_PROFILE} ;
  popd ;

  # Workbench
  WORKBENCH_URL=https://humanconnectome.org/storage/app/media/workbench/workbench-rh_linux64-v1.5.0.zip ;
  pushd /usr/local/ ;
  wget ${WGET_OPTIONS} ${WORKBENCH_URL} -O workbench.zip ;
  unzip workbench.zip && rm -rf workbench.zip ;
  echo -e '\nexport PATH=/usr/local/workbench/bin_rh_linux64:${PATH}\n' >> ${SHELL_PROFILE} ;
  popd ;

# For FS image
elif [ ! "$1" == "0" ] ; then
  # Freesurfer
  FREESURFER_URL=https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/7.4.1/freesurfer-linux-centos7_x86_64-7.4.1.tar.gz
  pushd /usr/local/
  wget ${WGET_OPTIONS} ${FREESURFER_URL} -O freesurfer.tgz ;
  tar xf freesurfer.tgz && rm -rf freesurfer.tgz ;
  export FREESURFER_HOME=/usr/local/freesurfer ;
  source ${FREESURFER_HOME}/SetUpFreeSurfer.sh ;
  cp /tmp/.license /usr/local/freesurfer/ ;
  /usr/local/freesurfer/bin/fs_install_mcr R2019b ;
  echo -e '\nexport FREESURFER_HOME=/usr/local/freesurfer' >> ${SHELL_PROFILE} ;
  echo -e 'source ${FREESURFER_HOME}/SetUpFreeSurfer.sh'   >> ${SHELL_PROFILE} ;
  echo -e "export MNI_DIR=\"\${FREESURFER_HOME}/mni\"\n"   >> ${SHELL_PROFILE} ;
  popd
fi

echo -e "\nunset FSFAST_HOME"                                            >> ${SHELL_PROFILE} ;
echo -e "unset MNI_DIR"                                                  >> ${SHELL_PROFILE} ;
echo -e "unset FSL_DIR"                                                  >> ${SHELL_PROFILE} ;
echo -e "unset PYTHONPATH"                                               >> ${SHELL_PROFILE} ;
echo -e "unset LC_ALL"                                                   >> ${SHELL_PROFILE} ;
echo -e "unset LC_CTYPE"                                                 >> ${SHELL_PROFILE} ;
echo -e "unset LD_LIBRARY_PATH"                                          >> ${SHELL_PROFILE} ;
echo -e "unset LD_GOLD"                                                  >> ${SHELL_PROFILE} ;
echo -e "alias runbip=\"\$FSLDIR/bin/python \$BIPDIR/main.py\""          >> ${SHELL_PROFILE} ;
echo -e "alias ll=\"ls -larth\""                                         >> ${SHELL_PROFILE} ;
echo -e "PS1=\"(ukb) \! \"\`whoami\`@\`hostname|cut -d. -f1\`@\$(basename \$(tty))\" | \\D{%Y.%m.%d} - \\\t | \\w \$ \"" >> ${SHELL_PROFILE} ;
echo -e "export PS1"                                                     >> ${SHELL_PROFILE} ;
echo -e "export PATH=\"\$BIPDIR:\$PATH\"\n"                              >> ${SHELL_PROFILE} ;
echo -e "export USER=\"BIPPER\"\n"                                       >> ${SHELL_PROFILE} ;

# Clean up
/usr/local/fsl/bin/conda clean --all --yes --force-pkgs-dirs ;
rm -rf /tmp/* ;
rm -rf ${HOME}/.cache/ ;
yum clean all ;
rm -rf /var/cache/yum/* ;
