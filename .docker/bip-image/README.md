BIP Docker image
----------------


The `Dockerfile` can be used to create a Docker image which contains BIP and
all of its dependencies. The image contains a full BIP runtime environment,
including FSL, FreeSurfer (optional) and Connectome Workbench. Be warned that
the image is quite large, on the order of 30GB with FS and 10 GB without FS.


1. Clone the BIP repository and change into the image directory:
   ```
   git clone https://git.fmrib.ox.ac.uk/fsl/bip.git
   cd bip/.docker/bip-image
   ```

2. Build the image:

   ```
   docker build . -t bip &> build_log.txt
   ```

3. If the image was built on a different machine than the one it is to be run
   on, save it to a `.tar.gz` archive:
   ```
   docker save bip | gzip -c > bip.tar.gz
   ```

   Then download the archive to the target machine, and load it:

   ```
   docker load < bip.tar.gz
   ```

4. Start a container from the image. Replace `<freesurfer-license>` with the
   full path to your FreeSurfer license file.

   ```
   docker run -it                                              \
     -v <freesurfer-license>:/usr/local/freesurfer/license.txt \
     bip /bin/bash
   ```
