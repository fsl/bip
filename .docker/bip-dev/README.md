This docker image is used for running unit tests on the BIP code base. It can
be built via Gitlab CI / CD by running the `build-dev-docker-image` job.

At the moment the image is published to
https://hub.docker.com/u/pauldmccarthy/bip-dev/.