#!/usr/bin/env bash

envdir=${1}

channels=(-c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/
          -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/
          -c conda-forge)

conda create -y ${channels[@]}   \
      -p ./${envdir}             \
      "python=${PYTHON_VERSION}" \
      coverage                   \
      dmri-amico                 \
      file-tree                  \
      file-tree-fsl              \
      fsl-gradunwarp             \
      fsl-avwutils               \
      fsl-cluster                \
      fsl-pyfix                  \
      fsl-pipe                   \
      fsl_sub                    \
      fsl_sub_plugin_slurm       \
      fsl_sub_plugin_sge         \
      jinja2                     \
      pytest                     \
      pytest-cov


echo "channels:"                                                        > ${envdir}/.condarc
echo "  https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/" >> ${envdir}/.condarc
echo "  https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/"      >> ${envdir}/.condarc
echo "  conda-forge"                                                   >> ${envdir}/.condarc

conda clean -y -all
pip cache purge
